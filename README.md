use https://heyapi.dev/ for generating code.

run curl http://127.0.0.1:8000/api/openapi.json > openapi.json to download the latest version from the backend.

run
yarn prettier -w openapi* && yarn openapi-ts
to compile and format it.

Use
yarn run
to run the code. Installation needs to run yarn install beforehand
