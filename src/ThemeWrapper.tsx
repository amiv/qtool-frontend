import { ThemeProvider } from "@emotion/react";
import {
  useMediaQuery,
  createTheme,
  CssBaseline,
  PaletteMode,
} from "@mui/material";
import { indigo, red } from "@mui/material/colors";
import React from "react";

const getDesignTokens = (mode: PaletteMode) => ({
  palette: {
    mode,
    ...(mode === "light"
      ? {
          // light mode palette
          primary: indigo,
          divider: indigo[200],
        }
      : {
          // dark mode palette
          primary: red,
          divider: red[500],
        }),
  },
});

export default function ThemeWrapper({
  children,
}: {
  children: React.ReactElement;
}) {
  const prefersDarkMode = useMediaQuery("(prefers-color-scheme: dark)");

  const theme = React.useMemo(
    () => createTheme(getDesignTokens(prefersDarkMode ? "dark" : "light")),
    [prefersDarkMode],
  );

  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      {children}
    </ThemeProvider>
  );
}
