// main.tsx
import React from "react";
import client from "./apiClientConfig";
import ReactDOM from "react-dom/client";
import { createBrowserRouter, RouterProvider } from "react-router-dom";
import Root from "./pages/root";
import ErrorBoundary from "./ErrorBoundary";
import Index from "./pages/index";
import ThemeWrapper from "./ThemeWrapper";

import Callback from "./Callback";

import CombinedPaymentsPage from "./pages/CombinedList";
import Belegformular, {
  addLoader as addCreditPaymentLoader,
} from "./pages/GenericLoader";
import Reimbursement from "./pages/Reimbursement";
import Bills from "./pages/Bills";
import CreditPayment from "./pages/CreditPayment";
import InternalTransfer from "./pages/InternalTransfer";
import EditCreditPayment from "./pages/EditCreditPayment";
import GenerateItem from "./pages/qtool-admin/GenerateItem";
import GenerateLedger from "./pages/qtool-admin/GenerateLedger";
import Onboarding from "./pages/Onboarding";
import GenerateKst from "./pages/qtool-admin/GenerateKst";
import EditBills from "./pages/EditBills";
import EditInternalTransfer from "./pages/EditInternalTransfer";
import EditReimbursement from "./pages/EditReimbursement";
import UncheckedPayments from "./pages/UncheckedPayments";
import KstEval from "./pages/KstEval";
import OwnList from "./pages/OwnList";
import ResponsibleList from "./pages/ResponsibleList";
import OnboardingQuiz from "./pages/OnboardingQuiz";
import EzagPage from "./pages/Ezag";
import EditLedger from "./pages/qtool-admin/EditLedger";
import ListLedger from "./pages/qtool-admin/ListLedger";
import ListItem from "./pages/qtool-admin/ListItem";
import EditKst from "./pages/qtool-admin/EditKst";
import ListKst from "./pages/qtool-admin/ListKst";
import CreditList from "./pages/CreditList";
import SettingsPage from "./pages/Settings";
import GenerateInvoice from "./pages/Invoices";
import ListInvoice from "./pages/ListInvoices";

const router = createBrowserRouter([
  {
    path: "/",
    element: <Root />,
    errorElement: <ErrorBoundary />,
    children: [
      {
        index: true,
        element: <Index />,
      },
      {
        path: "belegformular",
        element: <Belegformular />,
        loader: addCreditPaymentLoader,
      },
      {
        path: "Reimbursement",
        element: <Reimbursement />,
        loader: addCreditPaymentLoader,
      },
      {
        path: "CreditPayment",
        element: <CreditPayment />,
        loader: addCreditPaymentLoader,
      },
      {
        path: "Bills",
        element: <Bills />,
        loader: addCreditPaymentLoader,
      },
      {
        path: "InternalTransfer",
        element: <InternalTransfer />,
        loader: addCreditPaymentLoader,
      },
      {
        path: "GenerateItem",
        element: <GenerateItem />,
      },
      {
        path: "GenerateLedger",
        element: <GenerateLedger />,
      },
      {
        path: "GenerateKst",
        element: <GenerateKst />,
      },
      {
        path: "EditLedger",
        element: <EditLedger />,
      },
      {
        path: "EditKst",
        element: <EditKst />,
      },
      {
        path: "ListLedger",
        element: <ListLedger />,
      },
      {
        path: "ListKst",
        element: <ListKst />,
      },
      {
        path: "creditlist",
        element: <CreditList />,
        loader: addCreditPaymentLoader,
      },
      {
        path: "combinedList",
        element: <CombinedPaymentsPage />,
        loader: addCreditPaymentLoader,
      },
      {
        path: "ownList",
        element: <OwnList />,
        loader: addCreditPaymentLoader,
      },
      {
        path: "kstResponsibleCreditorList",
        element: <ResponsibleList />,
        loader: addCreditPaymentLoader,
      },
      {
        path: "onboarding",
        element: <Onboarding />,
      },
      {
        path: "settings",
        element: <SettingsPage />,
        loader: addCreditPaymentLoader,
      },
      {
        path: "onboardingQuiz",
        element: <OnboardingQuiz />,
      },
      {
        path: "CreditPayment/:idstring",
        element: <EditCreditPayment />,
        loader: addCreditPaymentLoader,
      },
      {
        path: "Bill/:idstring",
        element: <EditBills />,
        loader: addCreditPaymentLoader,
      },
      {
        path: "InternalTransfer/:idstring",
        element: <EditInternalTransfer />,
        loader: addCreditPaymentLoader,
      },
      {
        path: "Reimbursement/:idstring",
        element: <EditReimbursement />,
        loader: addCreditPaymentLoader,
      },
      {
        path: "UncheckedPayments",
        element: <UncheckedPayments />,
        loader: addCreditPaymentLoader,
      },
      {
        path: "KstEval",
        element: <KstEval />,
        loader: addCreditPaymentLoader,
      },
      {
        path: "Ezag",
        element: <EzagPage />,
      },
      {
        path: "Invoices",
        element: <GenerateInvoice />,
        loader: addCreditPaymentLoader,
      },
      {
        path: "ListInvoices",
        element: <ListInvoice />,
        loader: addCreditPaymentLoader,
      },
      {
        path: "ListItems",
        element: <ListItem />,
        loader: addCreditPaymentLoader,
      },
    ],
  },
  { path: "callback", element: <Callback /> },
]);
ReactDOM.createRoot(document.getElementById("root")!).render(
  <React.StrictMode>
    <ThemeWrapper>
      <RouterProvider router={router} />
    </ThemeWrapper>
  </React.StrictMode>,
);
