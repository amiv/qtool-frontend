import { client } from "./client/services.gen"; // Correct import path

const getToken = () => {
  // Retrieve the token from a secure place, e.g., localStorage or a secure token service
  console.log(sessionStorage.getItem("access_token"));
  return sessionStorage.getItem("access_token");
};

client.setConfig({
  baseUrl: import.meta.env.VITE_API_BASE_URL, // This fetches the base URL from your environment variables
  headers: {
    Authorization: "Bearer " + getToken(),
  },
});

//

// Response interceptor for logging
client.interceptors.response.use((response) => {
  if (response.status === 200) {
    console.log(`Request to ${response.url} was successful`);
  }
  return response;
});

export default client;
