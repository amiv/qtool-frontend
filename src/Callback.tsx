import { useEffect, useRef } from "react";
import { useNavigate } from "react-router-dom";
import { authIsOnboarded } from "./client/services.gen";
import client from "./apiClientConfig";
client.getConfig();
/**
 * Callback component handles OAuth redirection, token storage, and onboarding check.
 *
 * It extracts the access token from the URL, stores it locally, and then checks
 * if the user has completed onboarding. Depending on the result, it navigates to the
 * appropriate route.
 */
const Callback = () => {
  const navigate = useNavigate();
  const isProcessed = useRef(false); // Prevents multiple executions of the effect

  useEffect(() => {
    // Skip effect if already processed
    if (isProcessed.current) return;
    isProcessed.current = true;

    // Extract parameters from URL query string
    const searchParams = new URLSearchParams(window.location.search);
    const accessToken = searchParams.get("access_token");

    // If access token exists, proceed with storage and onboarding check
    if (accessToken) {
      sessionStorage.setItem("access_token", accessToken); // Store token securely

      // IIFE to handle asynchronous onboarding check within useEffect
      (async () => {
        try {
          client.setConfig({
            headers: {
              Authorization: "Bearer " + accessToken,
            },
          });

          const response = await authIsOnboarded({ client: client });
          // Navigate based on onboarding status
          if (response.error) {
            navigate("/onboardingQuiz");
          } else if (response.data) {
            //console.log(response.data);
            if (response.data === true) {
              navigate("/");
            } else {
              navigate("/error");
            }
          } else if (response.data === false) {
            navigate("/onboardingQuiz");
          }
        } catch (err) {
          console.error("Onboarding check failed:", err);
          navigate("/error");
        }
      })();
    } else {
      console.error("No access token found.");
      navigate("/error"); // Navigate to error page if no token is found
    }
  }, [navigate]);

  return <div>Processing login...</div>;
};

export default Callback;
