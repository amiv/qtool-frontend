import { Button, Card, Container, Stack, Typography } from "@mui/material";
import { Link, isRouteErrorResponse, useRouteError } from "react-router-dom";
import AmivWheelSVG from "./assets/amiv-wheel.svg";
import { Home } from "@mui/icons-material";

export default function ErrorBoundary() {
  const error = useRouteError();
  let errorMessage: string;

  if (isRouteErrorResponse(error)) {
    errorMessage = error.data.message || error.statusText;
  } else if (error instanceof Error) {
    errorMessage = error.message;
  } else if (typeof error === "string") {
    errorMessage = error;
  } else {
    errorMessage = "Unknown error";
  }

  return (
    <Container>
      <Stack direction="row">
        <img src={AmivWheelSVG} alt="AMIV-Logo" height={96} />
        <Typography variant="h1" component="h1">
          Error
        </Typography>
      </Stack>
      <Stack spacing={2}>
        <Card variant="outlined" sx={{ padding: 2 }}>
          {errorMessage}
        </Card>
        <Button variant="outlined" component={Link} to="/" startIcon={<Home />}>
          Go home
        </Button>
      </Stack>
    </Container>
  );
}
