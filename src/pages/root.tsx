import React, { useEffect, useState } from "react";
import {
  AppBar,
  Toolbar,
  Typography,
  Button,
  useMediaQuery,
  Box,
  SxProps,
  IconButton,
  useTheme,
  ListItem,
  ListItemButton,
  ListItemIcon,
  ListItemText,
  List,
  Drawer,
  Divider,
  Alert,
  Snackbar,
} from "@mui/material";
import { Outlet, useLocation, useNavigate } from "react-router-dom";
import {
  Home,
  ReceiptLong,
  ViewList,
  PlaylistRemove,
  Reorder,
  Category,
  Extension,
  AccountTree,
  School,
  Settings,
  Login,
  Logout,
  Menu,
  ShowChart,
  ViewListTwoTone,
  CreditCard,
  Description,
  SyncAlt,
} from "@mui/icons-material";
import { Link } from "react-router-dom";
import AmivLogoSVG from "../assets/amiv.svg";
import AmivWheelSVG from "../assets/amiv-wheel-white.svg";
import { authGetBasicUserInfo, client } from "../client/services.gen";
import { SnackbarProvider } from "./SnackbarProvider";

const sections = [
  {
    title: "General",
    items: [
      {
        path: "/",
        name: "Home",
        icon: <Home />,
      },
      {
        path: "/ownList",
        name: "My Requests",
        icon: <ViewList />,
      },
      {
        path: "/KstEval",
        name: "KST | Cost Center Evaluation",
        icon: <ShowChart />,
      },
      {
        path: "/kstResponsibleCreditorList",
        name: "KST Responsible List",
        icon: <ViewListTwoTone />,
      },
    ],
  },
  {
    title: "Requests",
    items: [
      {
        path: "/Reimbursement",
        name: "Reimbursement",
        icon: <ReceiptLong />,
      },
      {
        path: "/CreditPayment",
        name: "Credit Card Approval",
        icon: <CreditCard />,
      },
      {
        path: "/Bills",
        name: "Bill Payment",
        icon: <Description />,
      },
      {
        path: "/InternalTransfer",
        name: "Internal Transfer",
        icon: <SyncAlt />,
      },
    ],
  },
  {
    title: "Receipt Management",
    items: [
      {
        path: "/UncheckedPayments",
        name: "Open Requests",
        icon: <PlaylistRemove />,
      },
      {
        path: "/combinedList",
        name: "All Payments",
        icon: <Reorder />,
      },
      {
        path: "/creditlist",
        name: "Credit List",
        icon: <ViewList />,
      },
      {
        path: "/Ezag",
        name: "Ezag stuff",
        icon: <ReceiptLong />,
      },
    ],
  },
  {
    title: "Edit Qtool",
    items: [
      {
        path: "/ListItems",
        name: "Invoice Item",
        icon: <Category />,
      },
      {
        path: "/ListLedger",
        name: "Ledger",
        icon: <Extension />,
      },
      {
        path: "/ListKst",
        name: "Cost Center | KST",
        icon: <AccountTree />,
      },
      {
        path: "/ListInvoices",
        name: "Invoices",
        icon: <AccountTree />,
      },
    ],
  },
  {
    title: "Other",
    items: [
      {
        path: "/settings",
        name: "My Profile",
        icon: <School />,
      },
    ],
  },
];

function NavigationList({ user }: { user: any }) {
  // Ensure sections are filtered based on user.role
  const filteredSections = sections.filter((section) => {
    if (section.title === "General") {
      return true; // Always include the General section
    }
    if (section.title === "Requests") {
      return true; // Always include the Requests section
    }
    if (section.title === "Receipt Management" && user?.role === "auditor") {
      return true; // Include Receipt Management for auditors
    }
    return user?.role === "quaestor"; // Include other sections only for quaestors
  });

  return (
    <>
      {user ? (
        filteredSections.map((section, sectionIndex) => (
          <Box key={sectionIndex} sx={{ marginBottom: 2 }}>
            <Typography
              variant="subtitle1"
              sx={{ paddingLeft: 2, paddingTop: 1 }}
            >
              {section.title}
            </Typography>
            <List>
              {section.items.map((item, index) => (
                <ListItem key={index}>
                  <ListItemButton component={Link} to={item.path}>
                    <ListItemIcon>{item.icon}</ListItemIcon>
                    <ListItemText primary={item.name} />
                  </ListItemButton>
                </ListItem>
              ))}
            </List>
            {sectionIndex < filteredSections.length - 1 && <Divider />}
          </Box>
        ))
      ) : (
        <Typography></Typography>
      )}
    </>
  );
}

function DesktopAppBar({
  sx,
  toggleDrawer,
  isAuthenticated,
  onLogin,
  onLogout,
}: {
  sx?: SxProps;
  toggleDrawer: () => void;
  isAuthenticated: boolean;
  onLogin: () => void;
  onLogout: () => void;
}) {
  const navigate = useNavigate();
  return (
    <AppBar position="fixed" sx={sx}>
      <Toolbar>
        <IconButton color="inherit" onClick={toggleDrawer}>
          <Menu />
        </IconButton>
        <a href="/" style={{ height: "32px", marginLeft: "16px" }}>
          <img src={AmivLogoSVG} alt="AMIV Logo" style={{ height: "100%" }} />
        </a>
        <div style={{ flex: 1 }} />
        <IconButton color="inherit" onClick={() => navigate("/settings")}>
          <Settings />
        </IconButton>
        {isAuthenticated ? (
          <Button color="inherit" startIcon={<Logout />} onClick={onLogout}>
            Logout
          </Button>
        ) : (
          <Button color="inherit" startIcon={<Login />} onClick={onLogin}>
            Login
          </Button>
        )}
      </Toolbar>
    </AppBar>
  );
}

function MobileAppBar({
  sx,
  toggleDrawer,
  isAuthenticated,
  onLogin,
  onLogout,
}: {
  sx?: SxProps;
  toggleDrawer: () => void;
  isAuthenticated: boolean;
  onLogin: () => void;
  onLogout: () => void;
}) {
  const navigate = useNavigate();
  return (
    <AppBar position="fixed" sx={sx}>
      <Toolbar>
        <IconButton color="inherit" onClick={toggleDrawer}>
          <Menu />
        </IconButton>
        <a
          href="/"
          style={{ height: "32px", marginLeft: "16px" }}
        >
          <img
            src={AmivWheelSVG}
            alt="AMIV Wheel"
            style={{ height: "100%" }}
          />
        </a>
        <div style={{ flex: 1 }} />
        {isAuthenticated ? (
          <Button color="inherit" startIcon={<Logout />} onClick={onLogout}>
            Logout
          </Button>
        ) : (
          <Button color="inherit" startIcon={<Login />} onClick={onLogin}>
            Login
          </Button>
        )}
      </Toolbar>
    </AppBar>
  );
}

function App() {
  const theme = useTheme();
  const bigScreen = useMediaQuery(theme.breakpoints.up("md"));
  const navigate = useNavigate();
  const location = useLocation();

  const [drawerOpen, setDrawerOpen] = useState<boolean>(false);
  const [isAuthenticated, setIsAuthenticated] = useState<boolean>(false);
  const [user, setUser] = useState<any | null>(null);
  const [isFetchingUser, setIsFetchingUser] = useState(true);
  // Function to fetch user info and data
  const fetchUser = async () => {
    try {
      setIsFetchingUser(true);
      // Fetch user info
      const userResponse = await authGetBasicUserInfo();
      if (userResponse.error) {
        console.error("Failed to fetch user info:", userResponse.error);
        setUser(null);
      } else {
        setUser(userResponse.data);
      }
    } catch (error) {
      console.error("Error fetching data:", error);
      setUser(null);
    } finally {
      setIsFetchingUser(false);
    }
  };

  // Fetch data on mount and when user state changes
  useEffect(() => {
    fetchUser();
  }, []);

  const toggleDrawer = () => {
    setDrawerOpen((o) => !o);
  };

  useEffect(() => {
    const token = sessionStorage.getItem("access_token");
    setIsAuthenticated(!!token);
    setDrawerOpen(false);
  }, [location]);

  const handleLogin = () => {
    window.location.href = import.meta.env.VITE_API_BASE_URL + "api/login";
    fetchUser();
  };

  const handleLogout = () => {
    sessionStorage.removeItem("access_token");
    client.setConfig({
      headers: {
        Authorization: "Bearer " + "",
      },
    });
    setIsAuthenticated(false);
    navigate("/");
  };

  return (
    <>
      {bigScreen ? (
        <DesktopAppBar
          sx={{ display: { sx: "none", md: "block" } }}
          toggleDrawer={toggleDrawer}
          isAuthenticated={isAuthenticated}
          onLogin={handleLogin}
          onLogout={handleLogout}
        />
      ) : (
        <MobileAppBar
          sx={{ display: { sx: "block", md: "none" } }}
          toggleDrawer={toggleDrawer}
          isAuthenticated={isAuthenticated}
          onLogin={handleLogin}
          onLogout={handleLogout}
        />
      )}

      <Drawer
        anchor={bigScreen ? "left" : "bottom"}
        open={drawerOpen}
        onClose={toggleDrawer}
      >
        <Box sx={{ width: "100%" }}>
          <NavigationList user={user} />
        </Box>
      </Drawer>

      <Box sx={{ marginTop: "72px" }}>
        <Outlet />
      </Box>
    </>
  );
}
export default function Root() {
  return (
    <SnackbarProvider>
      <App />
    </SnackbarProvider>
  );
}
