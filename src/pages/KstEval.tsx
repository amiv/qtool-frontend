import React from "react";
import { useLoaderData } from "react-router-dom";
import {
  creditPaymentsReadCreditPayments,
  kstsReadKsts,
  ledgersReadLedgers,
  authGetBasicUserInfo,
  evaluationReadKsts,
} from "../client/services.gen";
import { BasicUser, Kst, Ledger } from "../client/types.gen";
import GenericDataTable from "../components/GenericDataTable";
import { Container } from "@mui/material";

export async function addLoader() {
  const kstList = await kstsReadKsts({});
  const kst = kstList.data?.items || [];
  const ledgerList = await ledgersReadLedgers({});
  const ledger = ledgerList.data?.items || [];
  const userRequest = await authGetBasicUserInfo();
  const user = userRequest.data;
  return { kst, ledger, user };
}

const KstEvalList: React.FC = () => {
  const { kst, ledger, user } = useLoaderData() as {
    kst: Kst[];
    ledger: Ledger[];
    user: BasicUser;
  };

  const columns = [
    { name: "kst_number", label: "KST Number" },
    { name: "name_de", label: "Name" },
    { name: "name_en", label: "Name (EN)" },
    { name: "active", label: "Active" },
    { name: "budget_plus", label: "Budget Plus" },
    { name: "budget_minus", label: "Budget Minus" },
    { name: "effective_plus", label: "Effective Plus" },
    { name: "effective_minus", label: "Effective Minus" },
  ];

  /**
   * fetchKstEval is now extended to receive pagination parameters.
   * It builds a query object including search, sort, filters, page, and limit.
   * The API call (evaluationReadKsts) is assumed to return data in the format:
   * { items: any[]; total: number; ... }.
   */
  const fetchKstEval = async ({
    search,
    sort,
    filters,
    page,
    limit,
  }: {
    search: string;
    sort: { column: string; direction: "asc" | "desc" } | null;
    filters: Record<string, any>;
    page: number;
    limit: number;
  }) => {
    const body: any = {
      search: search || null,
      sort: sort ? `${sort.column}:${sort.direction}` : null,
      page, // send current page to the backend
      limit, // send rows per page
      ...filters,
    };

    console.log("Fetching KST Evaluation with:", body);

    try {
      const response = await evaluationReadKsts({ query: body });
      // Assume response.data returns { items: [...], total: number }
      const results = response.data?.items || [];
      const total = response.data?.total || results.length;

      const transformedData = results.map((item: any) => ({
        kst_number: item.kst_number,
        name_de: item.name_de,
        name_en: item.name_en,
        active: item.active,
        budget_plus: item.budget_plus / 100.0,
        budget_minus: item.budget_minus / 100.0,
        effective_plus: item.effective_plus / 100.0,
        effective_minus: item.effective_minus / 100.0,
      }));

      return { data: transformedData, total };
    } catch (error) {
      console.error("Error fetching KST Evaluation:", error);
      return { data: [], total: 0 };
    }
  };

  return (
    <Container>
      <p>
        Here you can see the current status of each cost center in the AMIV
        budget. Please note that, at the moment, only debits and invoices issued
        by AMIV are displayed. Credits in the form of event payments or direct
        transfers to the AMIV account are not shown. The official document
        remains the KST evaluation which can be found in the "KST Auswertung"
        drive.
      </p>
      <GenericDataTable
        title="KST Evaluation"
        columns={columns}
        fetchData={fetchKstEval}
      />
    </Container>
  );
};

export default KstEvalList;
