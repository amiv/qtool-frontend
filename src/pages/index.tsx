import React, { useEffect, useState } from "react";
import {
  Container,
  Box,
  Typography,
  Button,
  Link,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Paper,
  Grid,
} from "@mui/material";
import {
  combinedReadCombinedPayments,
  authGetBasicUserInfo,
  combinedReadOwnCombinedPayments,
} from "../client/services.gen";
import { CombinedCreditor } from "../client";
import GenericEditableTable from "../components/GenericEditableTable";
import {
  Article,
  CreditCard,
  Person,
  Receipt,
  Settings,
} from "@mui/icons-material";
import { Link as RouterLink } from "react-router-dom";

export default function WelcomeToQTool() {
  const [lastFiveEntries, setLastFiveEntries] = useState<CombinedCreditor[]>(
    [],
  );
  const [user, setUser] = useState<any | null>(null);
  const [isFetchingUser, setIsFetchingUser] = useState(true);

  // Function to fetch user info and data
  const fetchUserAndData = async () => {
    try {
      setIsFetchingUser(true);
      // Fetch user info
      const userResponse = await authGetBasicUserInfo();
      if (userResponse.error) {
        console.error("Failed to fetch user info:", userResponse.error);
        setUser(null);
      } else {
        setUser(userResponse.data);
        // Fetch last 5 entries if user is authenticated
        const paymentResponse = await combinedReadOwnCombinedPayments({
          query: {
            search: "",
            sort: "id:desc",
            limit: 5,
          },
        });
        if (paymentResponse.error) {
          console.error(
            "Error fetching combined payments:",
            paymentResponse.error,
          );
        } else {
          console.log("received data");
          console.log(paymentResponse.data.items);
          setLastFiveEntries(paymentResponse.data.items);
          console.log(lastFiveEntries);
        }
      }
    } catch (error) {
      console.error("Error fetching data:", error);
      setUser(null);
    } finally {
      setIsFetchingUser(false);
    }
  };

  // Fetch data on mount and when user state changes
  useEffect(() => {
    fetchUserAndData();
  }, []);

  // Trigger re-fetch when login state changes (e.g., through global state or a login action)
  const handleLogin = () => {
    fetchUserAndData();
  };

  if (isFetchingUser) {
    return (
      <Container sx={{ textAlign: "center", padding: 2 }}>
        <Typography variant="h6">Loading...</Typography>
      </Container>
    );
  }

  return (
    <Container
      sx={{
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center",
        padding: 2,
        height: "100vh",
        textAlign: "center",
      }}
    >
      {user ? (
        <>
          <Box sx={{ marginBottom: 4 }}>
            <Typography variant="h4" sx={{ fontWeight: "bold" }}>
              Welcome to QTool {user.firstname}!
            </Typography>
          </Box>
          <Box sx={{ width: "100%", maxWidth: "600px", marginBottom: 4 }}>
            <Grid container spacing={2} justifyContent="center">
              <Grid item md={6} xs={12}>
                <Button
                  variant="outlined"
                  size="large"
                  startIcon={<Receipt />}
                  component={RouterLink}
                  to="/Reimbursement"
                  sx={{
                    width: "100%",
                    borderColor: "#d8202a", // Kahoot red
                    color: "#d8202a",
                  }}
                >
                  Reimbursement Request
                </Button>
              </Grid>
              <Grid item md={6} xs={12}>
                <Button
                  variant="outlined"
                  size="large"
                  startIcon={<CreditCard />}
                  component={RouterLink}
                  to="/CreditPayment"
                  sx={{
                    width: "100%",
                    borderColor: "#1368ce", // Kahoot blue
                    color: "#1368ce",
                  }}
                >
                  Credit Card Form
                </Button>
              </Grid>
              <Grid item md={6} xs={12}>
                <Button
                  variant="outlined"
                  size="large"
                  startIcon={<Person />}
                  component={RouterLink}
                  to="/OwnList"
                  sx={{
                    width: "100%",
                    borderColor: "#ffc028", // Kahoot yellow
                    color: "#ffc028",
                  }}
                >
                  Requests and Payments
                </Button>
              </Grid>
              <Grid item md={6} xs={12}>
                <Button
                  variant="outlined"
                  size="large"
                  startIcon={<Settings />}
                  component={RouterLink}
                  to="/settings"
                  sx={{
                    width: "100%",
                    borderColor: "#2baa41", // Kahoot green
                    color: "#2baa41",
                  }}
                >
                  Settings
                </Button>
              </Grid>
            </Grid>
          </Box>
          <Box sx={{ maxWidth: "600px", marginBottom: 4 }}>
            <Typography variant="body1">
              For more information, please visit the official QTool
              documentation linked below. If you face a problem of financial
              nature, contact the AMIV Finance Team at{" "}
              <Link
                component={RouterLink}
                to="mailto:quaestor@amiv.ethz.ch"
                underline="hover"
                color="primary"
              >
                quaestor@amiv.ethz.ch
              </Link>
              . For technical issues, reach out to the AMIV IT Team at{" "}
              <Link
                component={RouterLink}
                to="mailto:it@amiv.ethz.ch"
                underline="hover"
                color="primary"
              >
                it@amiv.ethz.ch
              </Link>
              .
            </Typography>
          </Box>
          <Box sx={{ marginTop: 0 }}>
            <Button
              variant="contained"
              size="large"
              startIcon={<Article />}
              component={RouterLink}
              to="https://wiki.vseth.ethz.ch/x/BQBLHQ"
              target="_blank"
              rel="noopener noreferrer"
              sx={{
                width: "300px",
                backgroundColor: "#46178f", // Purple color
                "&:hover": { backgroundColor: "#3d157f" }, // Slightly darker purple on hover
              }}
            >
              QTool Documentation
            </Button>
          </Box>
          <Box sx={{ marginTop: 4, width: "100%", maxWidth: "600px" }}>
            <Typography variant="h6" sx={{ marginBottom: 2 }}>
              Latest Requests:
            </Typography>
            <TableContainer component={Paper}>
              <Table>
                <TableHead>
                  <TableRow>
                    <TableCell>Name</TableCell>
                    <TableCell>Amount</TableCell>
                    <TableCell>Kst</TableCell>
                    <TableCell>Status</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody></TableBody>
              </Table>
            </TableContainer>
          </Box>
        </>
      ) : (
        <>
          <Box>
            <Typography variant="h4" sx={{ fontWeight: "bold" }}>
              Welcome to the new QTool of AMIV!
            </Typography>
          </Box>
          <Box sx={{ textAlign: "center" }}>
            <Typography variant="h6" sx={{ marginBottom: 2 }}>
              Please log in to access your dashboard. If this is your first time
              using QTool, please complete the onboarding process after logging
              in. The onboarding process is mandatory and will unlock the full
              functionality of QTool.
            </Typography>
            <Button
              variant="contained"
              color="primary"
              size="large"
              component={RouterLink}
              to={import.meta.env.VITE_API_BASE_URL + "api/login"}
            >
              Log In
            </Button>
          </Box>
        </>
      )}
    </Container>
  );
}
