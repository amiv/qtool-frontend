import React, { useEffect, useState } from "react";
import { CircularProgress, Container } from "@mui/material";
import { useParams, useLoaderData } from "react-router-dom";

import ObjectEditor, { FieldConfig, FieldType } from "../components/ObjectEditor";
import { Kst, Ledger, BasicUser, InvoiceCreate, Item, ItemstoInvoiceCreate } from "../client/types.gen";
import {
  invoicesReadInvoice,
  invoicesUpdateInvoice,
  invoicesDeleteInvoice,
  itemsReadItems,
} from "../client";
import { Kst_title, Ledger_title } from "../components/Titles";

// Import the field configuration generator from GenerateInvoice.
import { generateFieldConfigs } from "./Invoices";

interface InvoicePublic extends InvoiceCreate {
  id: string;
}

/**
 * EditInvoice component fetches an invoice's data,
 * displays an ObjectEditor for editing, and calls onClose after a successful update or deletion.
 *
 * The component performs the following data transformations:
 *  - Converts debitor.amount from cents to display units.
 *  - Transforms the invoice items array (as returned by the API) into an object mapping for the multi-item field.
 */
export default function EditInvoice({
  propIdString,
  onClose = () => {},
}: {
  propIdString: string;
  onClose?: () => void;
}) {
  // Retrieve the invoice ID from URL params or from the provided prop.
  const { idstring: urlidstring } = useParams<{ idstring: string }>();
  const idstring = urlidstring || propIdString;

  // Retrieve additional data from the loader (e.g. cost centers, ledgers, and the user).
  const { kst, ledger, user } = useLoaderData() as {
    kst: Kst[];
    ledger: Ledger[];
    user: BasicUser;
  };

  const [loading, setLoading] = useState<boolean>(true);
  const [initialElement, setInitialElement] = useState<InvoicePublic | null>(null);
  const [items, setItems] = useState<Item[]>([]);

  // Fetch invoice items for populating multi-item fields.
  useEffect(() => {
    const fetchItems = async () => {
      try {
        const response = await itemsReadItems();
        setItems(response.data?.items || []);
      } catch (error) {
        console.error("Error fetching items:", error);
      }
    };

    fetchItems();
  }, []);

  // Fetch the invoice data to be edited.
  useEffect(() => {
    const fetchInvoice = async () => {
      try {
        console.log("Fetching Invoice data for ID:", idstring);
        const response = await invoicesReadInvoice({ path: { id: idstring } });
        const data = response.data;
        if (!data) {
          console.error("No invoice data found");
          return;
        }
        // Convert debitor.amount from cents to display value.
        const debitorAmount = data.debitor?.amount ? data.debitor.amount / 100 : 0;

        // Convert the items array to an object mapping (for the multi-item field).
        const itemsMapping: Record<string, number> = {};
        if (data.items && Array.isArray(data.items)) {
          data.items.forEach((itm: ItemstoInvoiceCreate) => {
            itemsMapping[itm.item_id] = itm.count;
          });
        }

        // Ensure invoice_date is a Date object.
        const invoiceDate = data.invoice_date ? new Date(data.invoice_date) : new Date();

        const initial: InvoicePublic = {
          id: data.id || "",
          invoice_date: invoiceDate,
          payinterval: data.payinterval || 0,
          text_comment: data.text_comment || "",
          mwst_type: data.mwst_type || "",
          our_reference: data.our_reference || "",
          your_reference: data.your_reference || "",
          items: itemsMapping,
          debitor: {
            comment: data.debitor?.comment || "",
            name: data.debitor?.name || "",
            kst_id: data.debitor?.kst_id || "",
            ledger_id: data.debitor?.ledger_id || "",
            creator_id: data.debitor?.creator_id || user.id,
            accounting_year: data.debitor?.accounting_year || new Date().getFullYear(),
            amount: debitorAmount,
          },
          address: {
            name: data.address?.name || "",
            address1: data.address?.address1 || "",
            address2: data.address?.address2 || "",
            address3: data.address?.address3 || "",
            plz: data.address?.plz || "",
            city: data.address?.city || "",
            country: data.address?.country || "",
          },
        };
        setInitialElement(initial);
      } catch (error) {
        console.error("Failed to load invoice data:", error);
      } finally {
        setLoading(false);
      }
    };

    fetchInvoice();
  }, [idstring, user.id]);

  // Generate the field configuration for the invoice form.
  const fieldConfig: FieldConfig<InvoiceCreate>[] = generateFieldConfigs(
    kst,
    ledger,
    items,
    user.role === "quastor"
  );

  /**
   * Prepares and submits the updated invoice data.
   * Converts form values back to the expected API formats.
   *
   * @param changes The updated Invoice data from the ObjectEditor.
   */
  const submitter = async (changes: InvoicePublic) => {
    // Convert debitor.amount back to cents.
    changes.debitor.amount = changes.debitor.amount * 100;

    // Convert the multi-item mapping to an array.
    const itemsMapping = changes.items as Record<string, number>;
    const itemstoinvoice: ItemstoInvoiceCreate[] = Object.entries(itemsMapping).map(
      ([itemId, value]) => {
        const parsedCount = Number(value);
        return {
          item_id: itemId,
          count: isNaN(parsedCount) ? 1 : parsedCount,
          amount: 10, // Adjust default amount if needed.
          comment: "", // Adjust default comment if needed.
        };
      }
    );
    // Replace the items with the converted array.
    changes.items = itemstoinvoice;

    const response = await invoicesUpdateInvoice({
      body: changes,
      path: { id: idstring },
    });
    if (response.error) {
      // Revert the amount conversion in case of error.
      changes.debitor.amount = changes.debitor.amount / 100;
      throw response.error;
    } else {
      onClose();
      return;
    }
  };

  /**
   * Deletes the current invoice.
   */
  const deleter = async () => {
    const response = await invoicesDeleteInvoice({
      path: { id: idstring },
    });
    if (response.error) {
      throw response.error;
    } else {
      onClose();
      return;
    }
  };

  if (loading || !initialElement) {
    return (
      <Container>
        <CircularProgress />
      </Container>
    );
  }

  return (
    <Container>
      <ObjectEditor
        fieldConfigs={fieldConfig}
        header="Edit Invoice"
        submitter={submitter}
        deleter={deleter}
        initial={initialElement}
        onClose={onClose}
      />
    </Container>
  );
}
