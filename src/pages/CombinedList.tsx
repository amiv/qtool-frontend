import React from "react";
import { useLoaderData } from "react-router-dom";
import { combinedReadCombinedPayments } from "../client/services.gen";
import GenericEditableTable from "../components/GenericEditableTable";
import { CombinedCreditor } from "../client/types.gen";

const CombinedList: React.FC = () => {
  const { kst, ledger, user } = useLoaderData() as {
    kst: any[];
    ledger: any[];
    user: any;
  };

  /**
   * fetchCombinedPayments now supports server-side pagination.
   * It accepts page and limit, along with search, sort, and filters,
   * and returns an object containing:
   *   - data: the transformed list of combined payments for the current page
   *   - total: the total number of records available.
   */
  const fetchCombinedPayments = async ({
    search,
    sort,
    filters,
    page,
    limit,
  }: {
    search: string;
    sort: { column: string; direction: "asc" | "desc" } | null;
    filters: Record<string, any>;
    page: number;
    limit: number;
  }) => {
    const response = await combinedReadCombinedPayments({
      query: {
        search,
        sort: sort ? `${sort.column}:${sort.direction}` : null,
        page,
        limit,
        ...filters,
      },
    });
    if (response.error) {
      console.error("Error fetching combined payments:", response.error);
      return { data: [], total: 0 };
    }
    const results = response.data.items;
    const total = response.data.total || results.length;
    return {
      data: results.map((item: CombinedCreditor) => ({
        name: item.creditor.name,
        creditor__amount: (item.creditor?.amount) / 100.0,
        card: item.card,
        creditor__kst__kst_number:
          kst.find((k) => k.id === item.creditor?.kst_id)?.kst_number ||
          "Unknown",
        creditor__kst__name_de:
          kst.find((k) => k.id === item.creditor?.kst_id)?.name_de || "Unknown",
        creditor__ledger__name_de:
          ledger.find((l) => l.id === item.creditor?.ledger_id)?.name_de ||
          "Unknown",
        creditor__currency: item.creditor?.currency,
        q_check: item.creditor?.q_check,
        creditor__qcomment: item.creditor?.qcomment,
        creator: item.creator,
        type: item.type,
        id: item.id,
        reference: item.reference,
        iban: item.iban,
        comment: item.creditor.comment,
        reimbursement__recipient: item.reimbursement__recipient,
      })),
      total,
    };
  };

  return (
    <GenericEditableTable
      title="Combined Payments List"
      fetchFunction={fetchCombinedPayments}
      kst={kst}
      ledger={ledger}
      editable={user.role === "quaestor"}
    />
  );
};

export default CombinedList;
