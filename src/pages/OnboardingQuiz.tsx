import React, { useState } from "react";
import {
  Container,
  Box,
  Typography,
  Button,
  FormControl,
  FormLabel,
  RadioGroup,
  FormControlLabel,
  Radio,
  Checkbox,
} from "@mui/material";

export default function OnboardingCourse() {
  interface PageContent {
    id: number;
    title: string;
    description: string;
    question?: string;
    options?: string[]; // Multiple-choice options
    correctAnswer?: string; // The correct option
    isFinalPage?: boolean; // Whether this is the last page
  }

  // Sample content with multiple-choice questions
  const data: PageContent[] = [
    {
      id: 1,
      title: "Welcome to QTool! ",
      description:
        "In the next few minutes, you will learn how to use the tool correctly. At the end of this onboarding, you will need to enter your IBAN and address, so please have them ready. You will also need to answer a small quiz on each page of this onboarding. Let's begin!",
      question: "I will pay attention.",
      options: ["Yes", "No", "Maybe"],
      correctAnswer: "Yes",
    },
    {
      id: 2,
      title: "What is Qtool?",
      description:
        "QTool is used by AMIV to manage most financial processes. For you, it provides the ability to request reimbursements, enter any purchases made with an AMIV credit card, submit large bills that need to be paid by AMIV, and check the current status of the AMIV budget. If you are part of the board or a commission co-president, you will have detailed access to any transactions concerning the cost centers you are responsible for.",
      question: "What is not a feature of QTool?",
      options: [
        "Reimbursement Requests",
        "Rights management of AMIV IT services.",
        "AMIV Invoice Management",
        "Budget Overview",
      ],
      correctAnswer: "Rights management of AMIV IT services.",
    },
    {
      id: 3,
      title: "Receipts are important!",
      description:
        "The most common scenario for using QTool is when you spend money for AMIV (e.g., buying snacks for your weekly meeting). When spending money for AMIV, you ALWAYS need to ask for a receipt. No receipt, no reimbursement. The receipt is always necessary, whether you use your own money or an AMIV credit card. We strongly recommend directly taking a picture with your phone when you get the receipt so you do not forget it!",
      question: "Do I need a receipt when spending money for AMIV?",
      options: [
        "Only if I need a reimbursement",
        "Only if I use an AMIV Credit card",
        "I always need a receipt when spending money for AMIV",
        "Never, receipts are optional",
      ],
      correctAnswer: "I always need a receipt when spending money for AMIV",
    },
    {
      id: 4,
      title: "Reimbursement Requests",
      description:
        "When you want to get a reimbursement, choose the appropriate icon in the sidebar of QTool. Enter a short, unique, but precise description of what the money was spent on and what it is. Important things are what, which shop, and what event did you buy if for.\nChoose the correct cost center. If you are part of a commission, select the commission; if you spent the money for an AMIV event, select the event; if you are part of the board, you should know which option to choose. If in doubt, please contact the responsible person of your commission / event or the treasurer directly.\nSelect the most appropriate ledger, based on what you bought. Enter the exact amount stated on the receipt. Upload the receipt, making sure it is clear and that the whole receipt is visible. If you are happy with your choice, submit the form.",
      question:
        "Which of the following is the best description of a reimbursement request?",
      options: [
        "Gimme Money, was for snacks",
        "Snacks",
        "Beer for EESTEC meeting on 20.02.2014.",
        "Beer for EESTEC meeting",
      ],
      correctAnswer: "Beer for EESTEC meeting on 20.02.2014.",
    },
    {
      id: 5,
      title: "AMIV Credit Card",
      description:
        "If you have a longer event or bigger expenses to do for AMIV, you have the option to borrow an AMIV credit card. The president, the treasurer and the Kulturvorstände have one each. Contact us if you need one.\nIf you use an AMIV credit card, the receipt also needs to be entered into QTool. The procedure is very similar to the reimbursement process, but you should choose the credit card form. Please be aware that you are responsible for the credit card when you have it with you, and AMIV could charge you for any usage of the card where you cannot provide the receipt.",
      question: "Who has a credit card at AMIV?",
      options: ["Treasurer", "President", "Kulturvorstände", "all of them"],
      correctAnswer: "all of them",
    },
    {
      id: 6,
      title: "Submitting Requests",
      description:
        "Once you submit a request, the AMIV treasury will check it for correctness and either accept or decline it. You will be notified by email, and in the summary of QTool, you can see the status of all your requests.",
      question: "When can I discard the original receipt?",
      options: [
        "After I submitted the request",
        "After I received the money",
        "Never",
        "After the request was accepted",
      ],
      correctAnswer: "After the request was accepted",
    },
    {
      id: 7,
      title: "Additional Information",
      description:
        "You need to submit one form for EACH receipt. You CANNOT group multiple receipts. Additionally, receipts must be entered into QTool within 7 days of the purchase.",
      question:
        "What is the ideal sequence of actions when you spend money for AMIV? The context is a big event where you use an AMIV credit card to make some purchases.",
      options: [
        "After I make a purchase, I throw the receipt in the bin and have fun at the event.",
        "After each purchase, I take a high-quality picture of the receipt, and within 7 days of the purchase, I fill out a form for each receipt and upload it.",
        "I keep the receipts in my wallet and fill out the forms when I have time.",
        "After I make all purchases, I put the receipts next to each other, take a picture, and fill out one form for all receipts.",
      ],
      correctAnswer:
        "After each purchase, I take a high-quality picture of the receipt, and within 7 days of the purchase, I fill out a form for each receipt and upload it.",
    },

    {
      id: 8,
      title: "Warning!",
      description:
        "Use QTool responsibly and make every effort to ensure accuracy. The AMIV treasury will greatly appreciate your diligence. Remember, we all do this work voluntarily. Lastly, every request will be thoroughly reviewed by the AMIV treasury, and any fraudulent behavior will result in consequences.",
      isFinalPage: true,
    },
  ];

  // State
  const [currentPage, setCurrentPage] = useState(1);
  const [selectedOption, setSelectedOption] = useState("");
  const [answeredPages, setAnsweredPages] = useState<number[]>([]);
  const [termsAccepted, setTermsAccepted] = useState(false);

  // Derived values
  const totalPages = data.length;
  const currentItem = data[currentPage - 1];

  // Handle multiple-choice selection
  const handleOptionChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setSelectedOption(event.target.value);
  };

  // Validate the selected answer
  const checkAnswer = () => {
    if (
      selectedOption.trim().toLowerCase() ===
      (currentItem.correctAnswer?.toLowerCase() ?? "")
    ) {
      // Mark page as answered correctly if not already
      if (!answeredPages.includes(currentItem.id)) {
        setAnsweredPages((prev) => [...prev, currentItem.id]);
      }
      setSelectedOption(""); // Clear the selection
      // Auto-advance if not on the last page
      if (currentPage < totalPages) {
        setCurrentPage((prev) => prev + 1);
      }
    } else {
      alert("Incorrect answer. Please try again!");
    }
  };

  // Navigate to previous page
  const goToPreviousPage = () => {
    if (currentPage > 1) {
      setCurrentPage((prev) => prev - 1);
      setSelectedOption("");
    }
  };

  // Navigate to next page (only if page is answered or is the final page)
  const goToNextPage = () => {
    // If it's a quiz page, must be answered before allowing next
    if (currentItem?.correctAnswer) {
      // If current page is answered, or we are on the final page
      if (answeredPages.includes(currentItem.id) && currentPage < totalPages) {
        setCurrentPage((prev) => prev + 1);
        setSelectedOption("");
      }
    } else if (currentItem.isFinalPage && currentPage < totalPages) {
      // Jump directly to T&C page if user hasn't gone there yet
      setCurrentPage((prev) => prev + 1);
    }
  };

  // Handle T&C acceptance on the final page
  const handleAcceptTerms = () => {
    // Usually you'd call an API or do something else before redirecting
    // Then redirect once accepted:
    window.location.href = "/Onboarding";
  };

  return (
    <Container
      sx={{
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center",
        padding: 2,
        height: "100vh",
      }}
    >
      {/* Title & Description */}
      <Box sx={{ textAlign: "center", marginBottom: 2 }}>
        <Typography variant="h4">{currentItem.title}</Typography>
        <Typography variant="body1">{currentItem.description}</Typography>
      </Box>

      {/* If it's the final page, show Terms & Conditions instead of quiz */}
      {currentItem.isFinalPage ? (
        <Box>
          {/* Example: Simple checkbox for T&C acceptance */}
          <Box sx={{ display: "flex", alignItems: "center", marginBottom: 2 }}>
            <Checkbox
              checked={termsAccepted}
              onChange={() => setTermsAccepted(!termsAccepted)}
            />
            <Typography variant="body2">
              I acknowledge my responsibilities and commit to using QTool
              responsibly.
            </Typography>
          </Box>

          <Button
            variant="contained"
            disabled={!termsAccepted}
            onClick={handleAcceptTerms}
          >
            Submit & Continue to enter IBAN and address
          </Button>
        </Box>
      ) : (
        <>
          {/* Multiple Choice Question */}
          {currentItem.options && currentItem.question && (
            <Box sx={{ marginBottom: 2 }}>
              <FormControl component="fieldset">
                <FormLabel component="legend" sx={{ marginBottom: 1 }}>
                  {currentItem.question}
                </FormLabel>
                <RadioGroup
                  value={selectedOption}
                  onChange={handleOptionChange}
                  sx={{ marginLeft: 2 }}
                >
                  {currentItem.options.map((option) => (
                    <FormControlLabel
                      key={option}
                      value={option}
                      control={<Radio />}
                      label={option}
                    />
                  ))}
                </RadioGroup>
              </FormControl>
            </Box>
          )}

          {/* Submit Button for Quiz Pages */}
          {currentItem.correctAnswer && (
            <Box sx={{ textAlign: "center", marginBottom: 2 }}>
              <Button
                variant="contained"
                color="primary"
                onClick={checkAnswer}
                sx={{ marginRight: 2 }}
              >
                Submit Answer
              </Button>
            </Box>
          )}

          {/* Navigation Buttons */}
          <Box sx={{ display: "flex", justifyContent: "center", gap: 1 }}>
            <Button
              variant="contained"
              onClick={goToPreviousPage}
              disabled={currentPage === 1}
            >
              Previous
            </Button>
            <Button
              variant="contained"
              onClick={goToNextPage}
              disabled={
                // If it's a quiz page, disable Next if it's not answered yet
                (currentItem.correctAnswer &&
                  !answeredPages.includes(currentItem.id)) ||
                currentPage === totalPages
              }
            >
              Next
            </Button>
          </Box>
        </>
      )}
    </Container>
  );
}
