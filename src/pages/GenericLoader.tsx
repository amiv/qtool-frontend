import React, { useEffect, useState } from "react";
import { Container, Typography } from "@mui/material";
import {
  authGetBasicUserInfo,
  kstsReadKsts,
  ledgersReadLedgers,
} from "../client/services.gen";
import { useSnackbar } from "./SnackbarProvider";

import client from "../apiClientConfig";
import { BasicUser, KstPublic, LedgerPublic } from "../client";
client.getConfig();

// Maximum number of retries per page request
const MAX_RETRIES = 3;

/**
 * Generic function to fetch all paginated items with retry logic.
 *
 * @template T - The type of the items being fetched.
 * @param fetchFn - A function that fetches a specific page of data.
 * It should accept a page number and return a promise with an object
 * containing an optional data property (with items, count, total) and/or an error.
 * @param errorMessage - The error message to throw if the fetch fails after retries.
 * @param maxRetries - Maximum number of retries per page before failing.
 * @returns A promise that resolves with an array of all items of type T.
 */
async function fetchAllPages<T>(
  fetchFn: (page: number) => Promise<{
    data?: { items?: T[]; count: number; total: number };
    error?: any;
  }>,
  errorMessage: string,
  maxRetries: number = MAX_RETRIES
): Promise<T[]> {
  let page = 0;
  let allItems: T[] = [];
  let totalPages = 0;

  while (page <= totalPages) {
    let response = null;
    let retries = 0;

    // Try to fetch the current page, retrying if necessary.
    while (retries <= maxRetries) {
      response = await fetchFn(page);
      if (!response.error) {
        break; // Successful fetch.
      }
      retries++;
      if (retries > maxRetries) {
        console.log("failed after ", retries, " retries.")
        throw new Error(errorMessage);
      }
    }
    if (!response || !response.data) {
      console.log("invalid response")
      throw new Error(errorMessage);
    }
    const data = response.data;
    if (!data) {
      break;
    }

    const items = data.items || [];
    allItems = allItems.concat(items);

    // Calculate total pages based on the returned count and total.
    const count = data.count || items.length;
    const total = data.total || items.length;
    if (count === 0) {
      break;
    }
    totalPages = Math.ceil(total / count);
    page++;
  }

  return allItems;
}

/**
 * Fetches all active KSTs across all pages with retry logic.
 *
 * @returns A promise that resolves to an array of KstPublic objects.
 */
async function fetchAllKsts(): Promise<KstPublic[]> {
  return fetchAllPages<KstPublic>(
    (page) => kstsReadKsts({ query: { page, active: true } }),
    "Failed to load KST data, if this error persist contact the amiv IT."
  );
}

/**
 * Fetches all ledgers across all pages with retry logic.
 *
 * @returns A promise that resolves to an array of LedgerPublic objects.
 */
async function fetchAllLedgers(): Promise<LedgerPublic[]> {
  return fetchAllPages<LedgerPublic>(
    (page) => ledgersReadLedgers({ query: { page } }),
    "Failed to load ledger data, if this error persist contact the amiv IT."
  );
}

/**
 * Loader function for fetching application data with pagination and retry.
 *
 * Retrieves:
 * - All active KSTs (paginated)
 * - All ledgers (paginated)
 * - Basic user information
 *
 * Additionally, it produces a filtered ledger list (`limitedLedger`):
 * if the user role is "quaestor", then all ledgers are returned; otherwise,
 * only ledgers with `visibility === 0` are included.
 *
 * @returns A promise that resolves with an object containing:
 *   - `kst`: an array of active KstPublic objects,
 *   - `ledger`: an array of all LedgerPublic objects,
 *   - `limitedLedger`: an array of LedgerPublic objects after filtering,
 *   - `user`: basic user information.
 */
export const addLoader = async (): Promise<{
  kst: KstPublic[];
  ledger: LedgerPublic[];
  limitedLedger: LedgerPublic[];
  user: BasicUser | null;
}> => {
  // Fetch all active KSTs.
  const allKsts = await fetchAllKsts();

  // Fetch all ledgers.
  const allLedgers = await fetchAllLedgers();

  // Fetch basic user information.
  const userrequest = await authGetBasicUserInfo();
  if (userrequest.error) {
    throw new Error(
      "Failed to fetch user information, if this error persist contact the amiv IT."
    );
  }

  // Filter ledger list based on user's role.
  const limitedLedgerList =
    userrequest.data?.role === "quaestor"
      ? allLedgers
      : allLedgers.filter(
          (ledger: LedgerPublic) => ledger.visibility === 0
        );

  return {
    kst: allKsts,
    ledger: allLedgers,
    limitedLedger: limitedLedgerList,
    user: userrequest.data,
  };
};

// React component using the loader
const MainForm: React.FC = () => {
  const [dataLoaded, setDataLoaded] = useState<boolean>(false);
  const [error, setError] = useState<string | null>(null);
  const { showMessage } = useSnackbar();

  useEffect(() => {
    (async () => {
      try {
        const data = await addLoader();
        // Process the loaded data as needed.
        console.log("Loaded data:", data);
        setDataLoaded(true);
      } catch (err: any) {
        console.error("Error loading data:", err);
        setError(err.message);
        showMessage(err.message, "error");
      }
    })();
  }, [showMessage]);

  return (
    <Container>
      <Typography variant="h4" gutterBottom>
        Main Form
      </Typography>
      {error && (
        <Typography variant="body1" color="error">
          {error}
        </Typography>
      )}
      {dataLoaded ? (
        <Typography variant="body1">Data loaded successfully.</Typography>
      ) : (
        <Typography variant="body1">Loading data...</Typography>
      )}
    </Container>
  );
};

export default MainForm;
