import { Container } from "@mui/material";
import {
  authGetBasicUserInfo,
  internalTransfersCreateInternalTransfer,
  kstsReadKsts,
  ledgersReadLedgers,
  usersReadUser,
} from "../client/services.gen";
import {
  InternalTransferCreate,
  Kst,
  Ledger,
  BasicUser,
  q_state,
} from "../client/types.gen";

import client from "../apiClientConfig"; //Do not remove

import ObjectEditor, {
  FieldConfig,
  FieldType,
} from "../components/ObjectEditor";
import { useLoaderData } from "react-router-dom";
import { Kst_title, Ledger_title } from "../components/Titles";

// Configure the client
client.setConfig({
  baseUrl: import.meta.env.VITE_API_BASE_URL,
  headers: {
    Origin: `localhost`,
  },
});

export function generateFieldConfigs(
  kst: Kst[],
  ledger: Ledger[],
  qmode: boolean,
): FieldConfig<InternalTransferCreate>[] {
  let qfields = [] as FieldConfig<InternalTransferCreate>[];
  if (qmode === true) {
    qfields = [
      {
        name: "textcomment",
        label: "text comment",
        type: FieldType.COMMENT,
        comment: "these fields are only for the quästor:",
      },
      { name: "creditor.qcomment", label: "qcomment", type: FieldType.STRING },
      { name: "creditor.name", label: "name", type: FieldType.STRING },
      { name: "creditor.creator_id", label: "creator", type: FieldType.STRING },
      { name: "creditor.ledger_id", label: "Aufwand (Creditor) ", type: FieldType.STRING ,
        items: ledger.map((l) => ({ label: Ledger_title(l), value: l.id ? l.id : "" })),
      },
      {
        name: "debitor.ledger_id",
        label: "Ertrag (Debitor) ",
        type: FieldType.STRING,
        items: ledger.map((l) => ({ label: Ledger_title(l), value: l.id ? l.id : "" })),
      },
      {
        name: "creditor.accounting_year",
        label: "accounting_year",
        type: FieldType.NUMERIC,
      },
      {
        name: "creditor.q_check",
        label: "q_check",
        type: FieldType.STRING,
        items: [
          { label: "open", value: q_state.OPEN },
          { label: "accepted", value: q_state.ACCEPTED },
          { label: "rejected", value: q_state.REJECTED },
        ],
      },
    ];
  }

  return [
    { name: "creditor.comment", label: "Description", type: FieldType.STRING },
    {
      name: "debitor.kst_id",
      label: "Destination Cost Center (KST)",
      type: FieldType.STRING,
      items: kst.map((k) => ({ label: Kst_title(k), value: k.id ? k.id : "" })),
    },
    {
      name: "creditor.kst_id",
      label: "Source Cost Center (KST)",
      type: FieldType.STRING,
      items: kst.map((k) => ({ label: Kst_title(k), value: k.id ? k.id : "" })),
    },
    { name: "creditor.amount", label: "Amount", type: FieldType.CURRENCY },
    { name: "creditor.currency", label: "Currency", type: FieldType.STRING },
    ...qfields,
  ];
}

export default function GenerateInternalTransfer() {
  const { kst, ledger, user } = useLoaderData() as {
    kst: Kst[];
    ledger: Ledger[];
    user: BasicUser;
  };
  const fieldConfig = generateFieldConfigs(kst, ledger);

  const initialElement: InternalTransferCreate = {
    creditor: {
      kst_id: "",
      ledger_id: ledger.find((l) => l.visibility == 2, 0)?.id || "",
      amount: 0,
      accounting_year: 2025,
      currency: "CHF",
      comment: "",
      qcomment: "",
      name: "",
      creator_id: user.id,
      
    },
    debitor: {
      kst_id: "",
      name: "IV",
      ledger_id: ledger.find((l) => l.visibility == 3, 0)?.id || "",
      amount: 0,
      mwst: "0",
      creator_id: user.id,
      comment: "",
      q_comment: "",
    },
    amount: 0,
  };

  const submitter = async (changes: InternalTransferCreate) => {
    changes.creditor.amount = Math.round(changes.creditor.amount * 100);
    changes.debitor.amount = changes.creditor.amount;
    changes.amount = changes.creditor.amount;
    changes.debitor.comment = changes.creditor.comment;
    changes.debitor.q_comment = changes.creditor.qcomment;

    const response = await internalTransfersCreateInternalTransfer({
      body: changes,
    });
    if (response.error) {
      changes.creditor.amount = Number((changes.creditor.amount / 100).toFixed(2));
      changes.debitor.amount = changes.creditor.amount;
      changes.amount = changes.creditor.amount;
      throw response.error;
    } else {
      //maybe navigate somewhere
      window.location.href = "/ownList";
      return;
    }
  };

  return (
    <Container>
      <p>
        This form can be used to transfer money between cost centers. DO NOT USE
        IT UNLESS YOU WERE EXPLICITLY INSTRUCTED TO DO SO.
      </p>
      <ObjectEditor
        fieldConfigs={fieldConfig}
        header="Add Internal Transfer"
        submitter={submitter}
        initial={initialElement}
      />
    </Container>
  );
}
