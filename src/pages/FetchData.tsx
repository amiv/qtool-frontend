import { useState, useEffect } from "react";
import {
  billsReadBills,
  creditPaymentsReadCreditPayments,
  reimbursementsReadReimbursements,
  ledgersReadLedgers,
  kstsReadKsts,
} from "../client/services.gen";
import {
  BillsList,
  BillPublic_Output,
  CreditPaymentPublic_Output,
  CreditPaymentsList,
  ReimbursementsList,
  ReimbursementPublic_Output,
  LedgerPublic,
  KstPublic,
} from "../client/types.gen";
import client from "../apiClientConfig";
//this line is needed , no idea why, it doens't change anything. using it once is enough.
client.getConfig();

export function useFetchBills() {
  const [bills, setBills] = useState<BillPublic_Output[]>([]);
  const [loading, setLoading] = useState<boolean>(true);
  const [error, setError] = useState<string | null>(null);

  useEffect(() => {
    const loadData = async () => {
      setLoading(true);
      setError(null);

      try {
        // Using the `billsReadBills` function which should be part of the generated client
        const response = await billsReadBills();

        if (typeof response.data !== "object" || !("items" in response.data)) {
          console.error("Unexpected response format:", response.data);
          setError("Received unexpected response format from the server.");
          return;
        }

        const billsList = response.data as BillsList;
        setBills(billsList.items);
      } catch (err) {
        console.error("Error fetching bills:", err);
        setError("Error fetching bills");
      } finally {
        setLoading(false);
      }
    };

    loadData();
  }, []);

  return { bills, loading, error };
}

export function useFetchCreditPayments() {
  const [creditPayments, setCreditPayments] = useState<
    CreditPaymentPublic_Output[]
  >([]);
  const [loading, setLoading] = useState<boolean>(true);
  const [error, setError] = useState<string | null>(null);

  useEffect(() => {
    const loadData = async () => {
      setLoading(true);
      setError(null);

      try {
        const response = await creditPaymentsReadCreditPayments();

        if (typeof response.data !== "object" || !("items" in response.data)) {
          console.error("Unexpected response format:", response.data);
          setError("Received unexpected response format from the server.");
          return;
        }

        const creditPaymentsList = response.data as CreditPaymentsList;
        setCreditPayments(creditPaymentsList.items);
      } catch (err) {
        console.error("Error fetching credit payments:", err);
        setError("Error fetching credit payments");
      } finally {
        setLoading(false);
      }
    };

    loadData();
  }, []);

  return { creditPayments, loading, error };
}

export function useFetchReimbursements() {
  const [reimbursements, setReimbursements] = useState<
    ReimbursementPublic_Output[]
  >([]);
  const [loading, setLoading] = useState<boolean>(true); // Default is true
  const [error, setError] = useState<string | null>(null);

  useEffect(() => {
    const loadData = async () => {
      try {
        const response = await reimbursementsReadReimbursements();
        if (typeof response.data !== "object" || !("items" in response.data)) {
          throw new Error("Unexpected response format");
        }
        const reimbursementsList = response.data as ReimbursementsList;
        setReimbursements(reimbursementsList.items);
      } catch (err) {
        setError("Error fetching reimbursements");
        console.error(err);
      } finally {
        setLoading(false); // Ensure loading is set to false
      }
    };

    loadData();
  }, []);

  return { reimbursements, loading, error };
}

export function useFetchLedgerItems() {
  const [ledgerItems, setLedgerItems] = useState<LedgerPublic[]>([]);
  const [loading, setLoading] = useState<boolean>(true);
  const [error, setError] = useState<string | null>(null);

  useEffect(() => {
    const loadData = async () => {
      setLoading(true);
      setError(null);

      try {
        const response = await ledgersReadLedgers();

        if (typeof response.data !== "object" || !("items" in response.data)) {
          console.error("Unexpected response format:", response.data);
          setError("Received unexpected response format from the server.");
          return;
        }

        const ledgerList = response.data as { items: LedgerPublic[] };
        setLedgerItems(ledgerList.items);
      } catch (err) {
        console.error("Error fetching ledger items:", err);
        setError("Error fetching ledger items");
      } finally {
        setLoading(false);
      }
    };

    loadData();
  }, []);

  return { ledgerItems, loading, error };
}

export function useFetchKstItems() {
  const [kstItems, setKstItems] = useState<KstPublic[]>([]);
  const [loading, setLoading] = useState<boolean>(true);
  const [error, setError] = useState<string | null>(null);

  useEffect(() => {
    const loadData = async () => {
      setLoading(true);
      setError(null);

      try {
        const response = await kstsReadKsts();

        if (typeof response.data !== "object" || !("items" in response.data)) {
          console.error("Unexpected response format:", response.data);
          setError("Received unexpected response format from the server.");
          return;
        }

        const kstList = response.data as { items: KstPublic[] };
        setKstItems(kstList.items);
      } catch (err) {
        console.error("Error fetching KST items:", err);
        setError("Error fetching KST items");
      } finally {
        setLoading(false);
      }
    };

    loadData();
  }, []);

  return { kstItems, loading, error };
}
