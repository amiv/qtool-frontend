import { Container } from "@mui/material";
import { billsCreateBill } from "../client/services.gen";
import {
  BillCreate,
  Card,
  Kst,
  Ledger,
  BasicUser,
  q_state,
} from "../client/types.gen";

import client from "../apiClientConfig"; //Do not remove

import ObjectEditor, {
  FieldConfig,
  FieldType,
} from "../components/ObjectEditor";
import { useLoaderData } from "react-router-dom";
import { Kst_title, Ledger_title } from "../components/Titles";
import receiptHandler from "../components/ReceiptHandler";

export function generateFieldConfigs(
  kst: Kst[],
  ledger: Ledger[],
  q_mode: boolean = false,
): FieldConfig<BillCreate>[] {
  let qfields = [] as FieldConfig<BillCreate>[];
  if (q_mode === true) {
    qfields = [
      {
        name: "textcomment",
        label: "text comment",
        type: FieldType.COMMENT,
        comment: "these fields are only for the quästor:",
      },
      { name: "creditor.qcomment", label: "qcomment", type: FieldType.STRING },
      { name: "creditor.name", label: "name", type: FieldType.STRING },
      { name: "creditor.creator_id", label: "creator", type: FieldType.STRING },
      {
        name: "creditor.accounting_year",
        label: "accounting_year",
        type: FieldType.NUMERIC,
      },
      {
        name: "creditor.q_check",
        label: "q_check",
        type: FieldType.STRING,
        items: [
          { label: "open", value: q_state.OPEN },
          { label: "accepted", value: q_state.ACCEPTED },
          { label: "rejected", value: q_state.REJECTED },
        ],
      },
    ];
  }

  return [
    { name: "creditor.comment", label: "Description", type: FieldType.STRING },
    { name: "creditor.amount", label: "Amount", type: FieldType.CURRENCY },
    { name: "creditor.currency", label: "Currency", type: FieldType.STRING },
    {
      name: "creditor.kst_id",
      label: "KST | Cost Center",
      type: FieldType.STRING,
      items: kst.map((k) => ({ label: Kst_title(k), value: k.id ? k.id : "" })),
    },
    {
      name: "creditor.ledger_id",
      label: "Ledger",
      type: FieldType.STRING,
      items: ledger.map((l) => ({
        label: Ledger_title(l),
        value: l.id ? l.id : "",
      })),
    },
    { name: "address.name", label: "Company Name", type: FieldType.STRING },
    {
      name: "address.address1",
      label: "Address Line 1",
      type: FieldType.STRING,
    },
    {
      name: "address.address2",
      label: "Address Line 2 (optional)",
      type: FieldType.STRING,
    },
    {
      name: "address.address3",
      label: "Address Line 3 (optional)",
      type: FieldType.STRING,
    },
    { name: "address.plz", label: "ZIP Code", type: FieldType.STRING },
    { name: "address.city", label: "City", type: FieldType.STRING },
    {
      name: "address.country",
      label: "Country",
      type: FieldType.STRING,
    },
    {
      name: "textcomment",
      label: "text comment",
      type: FieldType.COMMENT,
      comment:
        "These fields are optional for now but make sure that the qr code in the receipt can be correctly read by a camera:",
    },
    {
      name: "reference",
      label: "(QR-) Reference",
      type: FieldType.STRING,
    },
    { name: "iban", label: "Iban", type: FieldType.STRING },
    { name: "receipt", label: "receipt (mandatory)", type: FieldType.FILE },
    ...qfields,
  ];
}

export default function GenerateBill() {
  const { kst, ledger, user, limitedLedger } = useLoaderData() as {
    kst: Kst[];
    ledger: Ledger[];
    user: BasicUser;
    limitedLedger: Ledger[];
  };
  console.log("limitedLedger", limitedLedger);
  const fieldConfig = generateFieldConfigs(kst, limitedLedger);

  const initialElement: BillCreate = {
    creditor: {
      kst_id: "",
      ledger_id: "",
      amount: 0,
      accounting_year: 2025,
      currency: "CHF", // Default value, adjust as needed
      comment: "",
      qcomment: "",
      name: "",
      creator_id: user.id ? user.id : "",
    },
    address: {
      name: "",
      address1: "",
      address2: "",
      address3: "",
      plz: "0",
      city: "",
      country: "",
    },
    reference: "",
    iban: "",
    receipt: "",
    comment: "",
  };

  const submitter = async (changes: BillCreate) => {
    
    changes.receipt = await receiptHandler(changes.receipt);
    changes.creditor.amount = Math.round(changes.creditor.amount * 100); // Convert amount to cents before submitting
    const response = await billsCreateBill({ body: changes });
    if (response.error) {
      changes.creditor.amount = Number((changes.creditor.amount / 100).toFixed(2));
      throw response.error;
    } else {
      window.location.href = "/ownList";
      return;
    }
  };

  return (
    <Container>
      <ObjectEditor
        fieldConfigs={fieldConfig}
        header="Enter Bill"
        description="If you have a large bill for an upcoming event, use this form. AMIV will then pay the bill as soon as possible.
          Enter all required information below. Please note that a seperate request must be submitted for each receipt. Grouping receipts is not permitted."
        submitter={submitter}
        initial={initialElement}
      />
    </Container>
  );
}

/*     body: {
      creditor: {
        kst_id: "",
        ledger_id: "",
        amount: 0,
        accounting_year: 2025,
        currency: "CHF", // Default value, adjust as needed
        comment: "",
        qcomment: "",
      },
      address: {
        name: "",
        address1: "",
        address2: "",
        address3: "",
        plz: 0,
        city: "",
        country: "",
      },
      reference: 0,
      iban: "",
      receipt: "",
      comment: "",
    },
  });
 */
