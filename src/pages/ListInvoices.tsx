import React, { useState } from "react";
import { Button, Dialog, DialogContent, DialogTitle } from "@mui/material";

import { invoicesReadInvoices } from "../client"; // Assumed endpoint for reading invoices.
import GenericDataTable from "../components/GenericDataTable";
import EditInvoice from "./EditInvoice";
import GenerateInvoice from "./Invoices";

/**
 * A higher-order component that wraps an editing/creation component.
 *
 * @param EditFunction - The component function to wrap.
 * @returns A component that renders the EditFunction with a Close button.
 */
const withWrapper =
  (
    EditFunction: (props: { propIdString: string; onClose: () => void }) => JSX.Element
  ) =>
  ({
    propIdString,
    onClose,
  }: {
    propIdString: string;
    onClose: () => void;
  }) => {
    const content = EditFunction({ propIdString, onClose });
    return (
      <div>
        {content}
        <Button onClick={onClose}>Close</Button>
      </div>
    );
  };

const EditInvoiceWrapper = withWrapper(EditInvoice);
const CreateInvoiceWrapper = withWrapper(GenerateInvoice);

/**
 * ListInvoice displays a paginated table of invoices.
 * It supports creating a new invoice or editing an existing one using dialogs.
 */
const ListInvoice: React.FC = () => {
  const [open, setOpen] = useState<boolean>(false);
  const [content, setContent] = useState<React.ReactNode | null>(null);
  const [refresh, setRefresh] = useState<boolean>(false);

  // Define columns for the data table.
  const columns = [
    { name: "invoice_date", label: "Invoice Date" },
    { name: "our_reference", label: "Our Reference" },
    { name: "your_reference", label: "Your Reference" },
    { name: "debitor_name", label: "Debitor Name" },
    { name: "edit", label: "Edit" },
  ];

  /**
   * Fetches invoices from the server using server-side pagination.
   *
   * @param params - The parameters for search, sorting, filtering, page and limit.
   * @returns An object with the transformed invoice data and the total record count.
   */
  const fetchInvoices = async ({
    search,
    sort,
    filters,
    page,
    limit,
  }: {
    search: string;
    sort: { column: string; direction: "asc" | "desc" } | null;
    filters: Record<string, any>;
    page: number;
    limit: number;
  }) => {
    const query: any = {
      search: search || null,
      sort: sort ? `${sort.column}:${sort.direction}` : null,
      page,
      limit,
      ...filters,
    };

    setRefresh(false);
    try {
      const response = await invoicesReadInvoices({ query });
      const results = response.data?.items || [];
      const total = response.data?.total || results.length;

      // Transform each invoice for display.
      const transformedData = results.map((invoice: any) => ({
        id: invoice?.id || "",
        invoice_date: invoice?.invoice_date
          ? new Date(invoice.invoice_date).toLocaleDateString()
          : "",
        our_reference: invoice?.our_reference || "",
        your_reference: invoice?.your_reference || "",
        debitor_name: invoice?.debitor?.name || "",
        // Render an Edit button in the "edit" column.
        edit: (
          <Button
            variant="contained"
            color="primary"
            onClick={() => handleEditAction(invoice?.id)}
          >
            Edit
          </Button>
        ),
      }));

      return { data: transformedData, total };
    } catch (error) {
      console.error("Error fetching invoices", error);
      return { data: [], total: 0 };
    }
  };

  /**
   * Opens the edit dialog for the given invoice ID.
   *
   * @param id - The ID of the invoice to edit.
   */
  const handleEditAction = (id: string) => {
    setOpen(true);
    setContent(<EditInvoiceWrapper propIdString={id} onClose={handleClose} />);
  };

  /**
   * Opens the create dialog.
   */
  const handleCreate = () => {
    setOpen(true);
    setContent(<CreateInvoiceWrapper propIdString="" onClose={handleClose} />);
  };

  /**
   * Closes the dialog.
   */
  const handleClose = () => {
    setOpen(false);
    setContent(null);
  };

  return (
    <>
      <div>
        <Button variant="contained" color="primary" onClick={handleCreate}>
          Generate New Invoice
        </Button>

        <GenericDataTable
          title="Invoice List"
          columns={columns}
          fetchData={fetchInvoices}
          refresh={!open}
        />
      </div>
      <Dialog open={open} onClose={handleClose} maxWidth="md" fullWidth>
        <DialogTitle>
          {content && React.isValidElement(content) ? "Details" : "Edit Invoice"}
        </DialogTitle>
        <DialogContent>{content}</DialogContent>
      </Dialog>
    </>
  );
};

export default ListInvoice;
