import React from "react";
import { useLoaderData } from "react-router-dom";
import {
  combinedReadResponsibleCombinedPayments,
  kstsReadKstsResponsible,
} from "../client/services.gen";
import GenericEditableTable from "../components/GenericEditableTable";
import { CombinedCreditor } from "../client/types.gen";
import { Container } from "@mui/material";

const ResponsibleList: React.FC = () => {
  const { kst, ledger, user } = useLoaderData() as {
    kst: any[];
    ledger: any[];
    user: any;
  };

  /**
   * fetchCombinedPayments now supports server-side pagination.
   * It accepts search, sort, filters, page, and limit, and returns an object with:
   *   - data: the current page's transformed data,
   *   - total: the total number of records.
   */
  const fetchCombinedPayments = async ({
    search,
    sort,
    filters,
    page,
    limit,
  }: {
    search: string;
    sort: { column: string; direction: "asc" | "desc" } | null;
    filters: Record<string, any>;
    page: number;
    limit: number;
  }) => {
    const ownkstresponse = await kstsReadKstsResponsible();
    if (ownkstresponse.error) {
      console.error("Error fetching responsible KSTs:", ownkstresponse.error);
      return { data: [], total: 0 };
    }

    const response = await combinedReadResponsibleCombinedPayments({
      query: {
        search,
        sort: sort ? `${sort.column}:${sort.direction}` : null,
        page,
        limit,
        ...filters,
      },
    });
    if (response.error) {
      console.error("Error fetching combined payments:", response.error);
      return { data: [], total: 0 };
    }
    const results = response.data.items;
    const total = response.data.total || results.length;

    const data = results.map((item: CombinedCreditor) => ({
      name: item.creditor.name,
      creditor__amount: (item.creditor?.amount) / 100.0,
      card: item.card,
      creditor__kst__kst_number:
        kst.find((k) => k.id === item.creditor?.kst_id)?.kst_number ||
        "Unknown",
      creditor__kst__name_de:
        kst.find((k) => k.id === item.creditor?.kst_id)?.name_de || "Unknown",
      creditor__ledger__name_de:
        ledger.find((l) => l.id === item.creditor?.ledger_id)?.name_de ||
        "Unknown",
      creditor__currency: item.creditor?.currency,
      q_check: item.creditor?.q_check,
      creator: item.creator,
      type: item.type,
      id: item.id,
      reference: item.reference,
      iban: item.iban,
      comment: item.creditor.comment,
      reimbursement__recipient: item.reimbursement__recipient,
    }));

    return { data, total };
  };

  return (
    <Container>
      <p>
        If you are a member of the AMIV board or a commission board, you can
        view all submitted requests related to the cost center you are
        responsible for.
      </p>
      <GenericEditableTable
        title="All Requests Regarding Your Cost Center(s)"
        fetchFunction={fetchCombinedPayments}
        kst={kst}
        ledger={ledger}
        editable={false}
      />
    </Container>
  );
};

export default ResponsibleList;
