import { CircularProgress, Container } from "@mui/material";

import client from "../apiClientConfig"; // Do not remove

import ObjectEditor, {
  FieldConfig,
  FieldType,
} from "../../components/ObjectEditor";
import { useLoaderData, useParams } from "react-router-dom";
import { Kst_title, Ledger_title } from "../components/Titles";
import { useEffect, useState } from "react";
import { generateFieldConfigs } from "./GenerateLedger";
import {
  LedgerPublic,
  ledgersDeleteLedger,
  ledgersReadLedger,
  ledgersUpdateLedger,
} from "../../client";

interface EditLedgerProps {
  propIdString: string;
  onClose?: () => void;
  qmode?: boolean;
}

/**
 * EditLedger component fetches ledger data,
 * displays the ObjectEditor for editing,
 * and calls onClose after a successful update or deletion.
 */
export default function EditLedger({
  propIdString,
  onClose = () => {},
  qmode = false,
}: EditLedgerProps) {
  const { idstring: urlidstring } = useParams<{ idstring: string }>();
  const idstring = urlidstring || propIdString;

  const [loading, setLoading] = useState<boolean>(true);
  const [initialElement, setInitialElement] = useState<LedgerPublic | null>(
    null
  );

  useEffect(() => {
    const fetchData = async () => {
      try {
        // Fetch existing ledger data by ID
        console.log("Fetching ledger data for ID:", idstring);
        const database = await ledgersReadLedger({
          path: { id: idstring },
        });
        const data = database.data;
        setInitialElement({
          id: data?.id || "",
          accountnumber: data?.accountnumber || "",
          visibility: data?.visibility || 0,
          name_de: data?.name_de || "",
          name_en: data?.name_en || "",
        });
      } catch (error) {
        console.error("Failed to load ledger data:", error);
      } finally {
        setLoading(false);
      }
    };

    fetchData();
  }, [idstring]);

  const submitter = async (changes: LedgerPublic) => {
    const response = await ledgersUpdateLedger({
      body: changes,
      path: { id: idstring },
    });
    if (response.error) {
      throw response.error;
    } else {
      // Close the dialog after a successful update.
      onClose();
      return;
    }
  };

  const deleter = async () => {
    const response = await ledgersDeleteLedger({
      path: { id: idstring },
    });
    if (response.error) {
      throw response.error;
    } else {
      // Close the dialog after deletion.
      onClose();
      return;
    }
  };

  const fieldConfig = generateFieldConfigs();

  if (loading || !initialElement) {
    return (
      <Container>
        <CircularProgress />
      </Container>
    );
  }

  return (
    <Container>
      <ObjectEditor
        fieldConfigs={fieldConfig}
        header="Edit Ledger"
        submitter={submitter}
        deleter={deleter}
        initial={initialElement}
        onClose={onClose}
      />
    </Container>
  );
}
