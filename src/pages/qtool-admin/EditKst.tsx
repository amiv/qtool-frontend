import { CircularProgress, Container } from "@mui/material";

import client from "../apiClientConfig"; // Do not remove

import ObjectEditor, {
  FieldConfig,
  FieldType,
} from "../../components/ObjectEditor";
import { useLoaderData, useParams } from "react-router-dom";
import { Kst_title, Ledger_title } from "../components/Titles";
import { useEffect, useState } from "react";
import { generateFieldConfigs } from "./GenerateKst";
import receiptHandler from "../../components/ReceiptHandler";
import {
  KstPublic,
  kstsDeleteKst,
  kstsReadKst,
  kstsUpdateKst,
} from "../../client";

interface EditKstProps {
  propIdString: string;
  onClose?: () => void;
  qmode?: boolean;
}

/**
 * EditKst component fetches Kst data,
 * displays the ObjectEditor for editing,
 * and calls onClose after a successful update or deletion.
 */
export default function EditKst({
  propIdString,
  onClose = () => {},
  qmode = false,
}: EditKstProps) {
  const { idstring: urlidstring } = useParams<{ idstring: string }>();
  const idstring = urlidstring || propIdString;

  const [loading, setLoading] = useState<boolean>(true);
  const [initialElement, setInitialElement] = useState<KstPublic | null>(null);

  useEffect(() => {
    const fetchData = async () => {
      try {
        // Fetch existing Kst data by ID
        console.log("Fetching Kst data for ID:", idstring);
        const database = await kstsReadKst({
          path: { id: idstring },
        });
        const data = database.data;
        setInitialElement({
          id: data?.id || "",
          kst_number: data?.kst_number || "",
          name_de: data?.name_de || "",
          name_en: data?.name_en || "",
          owner: data?.owner || "",
          active: data?.active || true,
          budget_plus: (data?.budget_plus || 0) / 100.0,
          budget_minus: (data?.budget_minus || 0) / 100.0,
        });
      } catch (error) {
        console.error("Failed to load Kst data:", error);
      } finally {
        setLoading(false);
      }
    };

    fetchData();
  }, [idstring]);

  const submitter = async (changes: KstPublic) => {
    // Convert amounts to cents before submitting
    changes.budget_plus = changes.budget_plus * 100;
    changes.budget_minus = changes.budget_minus * 100;
    const response = await kstsUpdateKst({
      body: changes,
      path: { id: idstring },
    });
    if (response.error) {
      changes.budget_plus = changes.budget_plus / 100;
      changes.budget_minus = changes.budget_minus / 100;
      throw response.error;
    } else {
      // Close the dialog after a successful update.
      onClose();
      return;
    }
  };

  const deleter = async () => {
    const response = await kstsDeleteKst({
      path: { id: idstring },
    });
    if (response.error) {
      throw response.error;
    } else {
      // Close the dialog after deletion.
      onClose();
      return;
    }
  };

  const fieldConfig = generateFieldConfigs();

  if (loading || !initialElement) {
    return (
      <Container>
        <CircularProgress />
      </Container>
    );
  }

  return (
    <Container>
      <ObjectEditor
        fieldConfigs={fieldConfig}
        header="Edit Kst"
        submitter={submitter}
        deleter={deleter}
        initial={initialElement}
        onClose={onClose}
      />
    </Container>
  );
}
