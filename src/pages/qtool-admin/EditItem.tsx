import { CircularProgress, Container } from "@mui/material";
import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";

import client from "../apiClientConfig"; // Do not remove
import ObjectEditor, { FieldConfig, FieldType } from "../../components/ObjectEditor";
import {
  itemsReadItem,
  itemsUpdateItem,
  itemsDeleteItem,
} from "../../client";
import { ItemBase } from "../../client/types.gen";

/**
 * An extended interface for editing items which includes an `id` field.
 */
interface ItemPublic extends ItemBase {
  id: string;
}

interface EditItemProps {
  propIdString: string;
  onClose?: () => void;
  qmode?: boolean;
}

/**
 * EditItem component fetches Item data,
 * displays the ObjectEditor for editing,
 * and calls onClose after a successful update or deletion.
 *
 * The price is assumed to be stored as cents on the backend.
 * Therefore, we divide by 100 for display and multiply by 100 before submitting.
 */
export default function EditItem({
  propIdString,
  onClose = () => {},
  qmode = false,
}: EditItemProps) {
  // Get the id from the URL params, falling back to the passed propIdString.
  const { idstring: urlidstring } = useParams<{ idstring: string }>();
  const idstring = urlidstring || propIdString;

  const [loading, setLoading] = useState<boolean>(true);
  const [initialElement, setInitialElement] = useState<ItemPublic | null>(null);

  useEffect(() => {
    const fetchData = async () => {
      try {
        console.log("Fetching Item data for ID:", idstring);
        const response = await itemsReadItem({
          path: { id: idstring },
        });
        const data = response.data;
        setInitialElement({
          id: data?.id || "",
          title_de: data?.title_de || "",
          title_en: data?.title_en || "",
          description_de: data?.description_de || "",
          description_en: data?.description_en || "",
          // Convert price from cents to a decimal representation.
          price: (data?.price || 0) / 100.0,
          unit: data?.unit || "",
          mwst: data?.mwst || 0,
          mwst_type: data?.mwst_type || "",
          mwst_published: data?.mwst_published || 0,
          active: data?.active ?? true,
        });
      } catch (error) {
        console.error("Failed to load Item data:", error);
      } finally {
        setLoading(false);
      }
    };

    fetchData();
  }, [idstring]);

  /**
   * Prepares and submits the updated Item data.
   *
   * @param changes The updated Item data from the ObjectEditor.
   */
  const submitter = async (changes: ItemPublic) => {
    // Convert price back to cents before submitting.
    changes.price = changes.price * 100;
    // If needed, perform similar adjustments for mwst here.
    const response = await itemsUpdateItem({
      body: changes,
      path: { id: idstring },
    });
    if (response.error) {
        changes.price = changes.price /100;
      throw response.error;
    } else {
      // Close after a successful update.
      onClose();
      return;
    }
  };

  /**
   * Deletes the current Item.
   */
  const deleter = async () => {
    const response = await itemsDeleteItem({
      path: { id: idstring },
    });
    if (response.error) {
        
      throw response.error;
    } else {
      // Close after deletion.
      onClose();
      return;
    }
  };

  // Field configuration for editing an Item.
  const fieldConfig: FieldConfig<ItemBase>[] = [
    { name: "title_de", label: "Titel Deutsch", type: FieldType.STRING },
    { name: "title_en", label: "Titel Englisch", type: FieldType.STRING },
    { name: "description_de", label: "Beschreibung Deutsch", type: FieldType.STRING },
    { name: "description_en", label: "Beschreibung Englisch", type: FieldType.STRING },
    { name: "price", label: "Preis (e.g. 123.45 CHF)", type: FieldType.NUMERIC },
    { name: "unit", label: "Einheit", type: FieldType.STRING },
    { name: "mwst", label: "MWST Intern", type: FieldType.NUMERIC },
    { name: "mwst_type", label: "MWST Type", type: FieldType.STRING },
    { name: "mwst_published", label: "MWST auf Rechnung", type: FieldType.NUMERIC },
    { name: "active", label: "Active", type: FieldType.BOOLEAN },
  ];

  if (loading || !initialElement) {
    return (
      <Container>
        <CircularProgress />
      </Container>
    );
  }

  return (
    <Container>
      <ObjectEditor
        fieldConfigs={fieldConfig}
        header="Edit Invoice Item"
        submitter={submitter}
        deleter={deleter}
        initial={initialElement}
        onClose={onClose}
      />
    </Container>
  );
}
