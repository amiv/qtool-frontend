import React, { useState } from "react";
import { Button, Dialog, DialogContent, DialogTitle } from "@mui/material";

import { itemsReadItems } from "../../client"; // Assumed endpoint for reading list of items.
import GenericDataTable from "../../components/GenericDataTable";
import { handleDownload } from "../components/DownloadHandler";
import EditItem from "./EditItem";
import GenerateItem from "./GenerateItem";

/**
 * A higher-order component that wraps an editing component.
 *
 * @param EditFunction - The edit function component to wrap.
 * @returns A component that renders the EditFunction with a Close button.
 */
const withWrapper =
  (EditFunction: (props: { propIdString: string; onClose: () => void }) => JSX.Element) =>
  ({
    propIdString,
    onClose,
  }: {
    propIdString: string;
    onClose: () => void;
  }) => {
    const content = EditFunction({ propIdString, onClose });
    return (
      <div>
        {content}
        <Button onClick={onClose}>Close</Button>
      </div>
    );
  };

const EditItemWrapper = withWrapper(EditItem);
const CreateItemWrapper = withWrapper(GenerateItem);

/**
 * ListItem component displays a paginated table of items.
 * It supports creating and editing items through dialogs.
 */
const ListItem: React.FC = () => {
  const [open, setOpen] = useState<boolean>(false);
  const [content, setContent] = useState<React.ReactNode | null>(null);
  const [refresh, setRefresh] = useState<boolean>(false);

  // Define columns for the data table.
  const columns = [
    { name: "title_de", label: "Title (DE)" },
    { name: "title_en", label: "Title (EN)" },
    { name: "description_de", label: "Description (DE)" },
    { name: "description_en", label: "Description (EN)" },
    { name: "price", label: "Price" },
    { name: "unit", label: "Unit" },
    { name: "active", label: "Active" },
    { name: "edit", label: "Edit" },
  ];

  /**
   * fetchItems supports server-side pagination.
   * It accepts search, sort, filters, page, and limit,
   * and returns an object with:
   *   - data: the transformed list of items for the current page
   *   - total: the total number of records.
   */
  const fetchItems = async ({
    search,
    sort,
    filters,
    page,
    limit,
  }: {
    search: string;
    sort: { column: string; direction: "asc" | "desc" } | null;
    filters: Record<string, any>;
    page: number;
    limit: number;
  }) => {
    const body: any = {
      search: search || null,
      sort: sort ? `${sort.column}:${sort.direction}` : null,
      page,
      limit,
      ...filters,
    };

    setRefresh(false);
    try {
      const response = await itemsReadItems({ query: body });
      const results = response.data?.items || [];
      const total = response.data?.total || results.length;

      const transformedData = results.map((item: any) => ({
        id: item?.id || "",
        title_de: item?.title_de || "",
        title_en: item?.title_en || "",
        description_de: item?.description_de || "",
        description_en: item?.description_en || "",
        // Convert price from cents to decimal representation.
        price: (item?.price || 0) / 100.0,
        unit: item?.unit || "",
        active: item?.active ?? true,
        // Render an Edit button in the "edit" column.
        edit: (
          <Button
            variant="contained"
            color="primary"
            onClick={() => handleEditAction(item?.id)}
          >
            Edit
          </Button>
        ),
      }));

      return { data: transformedData, total };
    } catch (error) {
      console.error("Error fetching items", error);
      return { data: [], total: 0 };
    }
  };

  /**
   * Opens the edit dialog for a given item ID.
   *
   * @param id The ID of the item to edit.
   */
  const handleEditAction = (id: string) => {
    setOpen(true);
    setContent(<EditItemWrapper propIdString={id} onClose={handleClose} />);
  };

  /**
   * Opens the create dialog.
   */
  const handleCreate = () => {
    setOpen(true);
    setContent(<CreateItemWrapper propIdString="" onClose={handleClose} />);
  };

  /**
   * Closes the dialog.
   */
  const handleClose = () => {
    setOpen(false);
    setContent(null);
  };

  const handleEdit = (e: React.MouseEvent<HTMLButtonElement>, id: string) => {
    e.stopPropagation();
    handleEditAction(id);
  };

  return (
    <>
      <div>
        <Button variant="contained" color="primary" onClick={handleCreate}>
          Generate New Item
        </Button>

        <GenericDataTable
          title="Item List"
          columns={columns}
          fetchData={fetchItems}
          refresh={!open}
        />
      </div>
      <Dialog open={open} onClose={handleClose} maxWidth="md" fullWidth>
        <DialogTitle>
          {content && React.isValidElement(content) ? "Details" : "Edit Item"}
        </DialogTitle>
        <DialogContent>{content}</DialogContent>
      </Dialog>
    </>
  );
};

export default ListItem;
