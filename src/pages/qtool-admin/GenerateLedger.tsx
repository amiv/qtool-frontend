import { Container } from "@mui/material";
import { ledgersCreateLedger } from "../../client/services.gen";
import {
  LedgerCreate,
  Card,
  Kst,
  Ledger,
  BasicUser,
} from "../../client/types.gen";

import client from "../../apiClientConfig"; //Do not remove

import ObjectEditor, {
  FieldConfig,
  FieldType,
} from "../../components/ObjectEditor";
import { useLoaderData } from "react-router-dom";
import { Kst_title, Ledger_title } from "../../components/Titles";

export function generateFieldConfigs(): FieldConfig<LedgerCreate>[] {
  return [
    { name: "accountnumber", label: "accountnumber", type: FieldType.STRING },
    { name: "name_de", label: "name_de", type: FieldType.STRING },
    { name: "name_en", label: "name_en", type: FieldType.STRING },
    {
      name: "visibility",
      label: "Visibility, 0 all 1 q only",
      type: FieldType.NUMERIC,
    },
  ];
}

export default function GenerateLedger() {
  const fieldConfig = generateFieldConfigs();

  const initialElement: LedgerCreate = {
    accountnumber: 0,
    name_de: "",
    name_en: "",
    visibility: 0,
  };

  const submitter = async (changes: LedgerCreate) => {
    const response = await ledgersCreateLedger({ body: changes });
    if (response.error) {
      throw response.error;
    } else {
      //maybe navigate somewhere
      return;
    }
  };

  return (
    <Container>
      <ObjectEditor
        fieldConfigs={fieldConfig}
        header="Add Ledger"
        submitter={submitter}
        initial={initialElement}
      />
    </Container>
  );
}
