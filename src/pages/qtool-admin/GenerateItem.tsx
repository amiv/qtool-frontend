import React from "react";
import {
  Container,
} from "@mui/material";
import { itemsCreateItem } from "../../client/services.gen";
import { ItemBase } from "../../client/types.gen";

import ObjectEditor, {
  FieldConfig,
  FieldType,
} from "../../components/ObjectEditor";

const fieldConfig: FieldConfig<ItemBase>[] = [
  { name: "title_de", label: "Titel Deutsch", type: FieldType.STRING },
  { name: "title_en", label: "Titel Englisch", type: FieldType.STRING },
  { name: "description_de", label: "Beschreibung Deutsch", type: FieldType.STRING },
  { name: "description_en", label: "Beschreibung Englisch", type: FieldType.STRING },
  { name: "price", label: "Preis (e.g. 123.45 CHF)", type: FieldType.NUMERIC },
  { name: "unit", label: "Einheit", type: FieldType.STRING },
  { name: "mwst", label: "MWST Intern", type: FieldType.NUMERIC },
  { name: "mwst_type", label: "MWST Type", type: FieldType.STRING },
  { name: "mwst_published", label: "MWST auf Rechnung", type: FieldType.NUMERIC },
];

export default function GenerateItem() {
  const initialElement: ItemBase = {
    title_de: "",
    title_en: "",
    description_de: "",
    description_en: "",
    price: 0,
    unit: "",
    mwst: 0,
    mwst_type: "",
    mwst_published: 0,
    active: true,
  };

  const submitter = async (changes: ItemBase) => {
    // Adjust price to be sent as cents (or smallest currency unit)
    changes.price = changes.price * 100;
    // If needed, perform similar adjustments for mwst here
    const response = await itemsCreateItem({ body: changes });
    if (response.error) {
      changes.price = changes.price /100;
      throw response.error;
    }
    // maybe navigate somewhere after successful creation
  };

  return (
    <Container>
      <ObjectEditor
        fieldConfigs={fieldConfig}
        header="Add Invoice Item"
        submitter={submitter}
        initial={initialElement}
      />
    </Container>
  );
}
