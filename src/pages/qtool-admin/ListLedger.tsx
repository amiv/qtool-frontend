import React, { useState } from "react";
import { Button, Dialog, DialogContent, DialogTitle } from "@mui/material";
import { ledgersReadLedgers } from "../../client";
import GenericDataTable from "../../components/GenericDataTable";
import { handleDownload } from "../components/DownloadHandler";
import EditLedger from "./EditLedger";
import GenerateLedger from "./GenerateLedger";

const withWrapper =
  (EditFunction: (id: string) => JSX.Element) =>
  ({
    propIdString,
    onClose,
  }: {
    propIdString: string;
    onClose: () => void;
  }) => {
    const content = EditFunction({propIdString, onClose});
    return (
      <div>
        {content}
        <Button onClick={onClose}>Close</Button>
      </div>
    );
  };

const EditLedgerWrapper = withWrapper(EditLedger);
const CreateLedgerWrapper = withWrapper(GenerateLedger);

const ListLedger: React.FC = () => {
  const [open, setOpen] = useState(false);
  const [content, setContent] = useState<React.ReactNode | null>(null);
  const [refresh, setRefresh] = useState(false);

  const columns = [
    { name: "accountnumber", label: "accountnumber" },
    { name: "name_de", label: "name_de" },
    { name: "name_en", label: "name_en" },
    { name: "visibility", label: "Visibility, 0 all 1 q only" },
    { name: "edit", label: "edit" },
  ];

  /**
   * fetchEzags now supports server-side pagination.
   * It accepts search, sort, filters, page, and limit,
   * and returns an object with:
   *   - data: the transformed list of ledger items for the current page
   *   - total: the total number of records.
   */
  const fetchEzags = async ({
    search,
    sort,
    filters,
    page,
    limit,
  }: {
    search: string;
    sort: { column: string; direction: "asc" | "desc" } | null;
    filters: Record<string, any>;
    page: number;
    limit: number;
  }) => {
    const body: any = {
      search: search || null,
      sort: sort ? `${sort.column}:${sort.direction}` : null,
      page,
      limit,
      ...filters,
    };

    setRefresh(false);
    try {
      const response = await ledgersReadLedgers({ query: body });
      const results = response.data?.items || [];
      const total = response.data?.total || results.length;
      console.log(results);

      const transformedData = results.map((item: any) => ({
        id: item?.id || "",
        accountnumber: item?.accountnumber || "",
        visibility: item?.visibility || 0,
        name_de: item?.name_de || "",
        name_en: item?.name_en || "",
        // Render an Edit button in the "edit" column.
        edit: (
          <Button
            variant="contained"
            color="primary"
            onClick={() => handleEditAction(item?.id)}
            download
          >
            Edit
          </Button>
        ),
      }));

      return { data: transformedData, total };
    } catch (error) {
      console.error("Error fetching Ledgers", error);
      return { data: [], total: 0 };
    }
  };

  const handleEditAction = (id: string) => {
    setOpen(true);
    setContent(<EditLedgerWrapper propIdString={id} onClose={handleClose} />);
  };

  const handleCreate = () => {
    setOpen(true);
    setContent(<CreateLedgerWrapper propIdString="" onClose={handleClose} />);
  };

  const handleClose = () => {
    setOpen(false);
    setContent(null);
  };

  const handleEdit = (e: React.MouseEvent<HTMLButtonElement>, id: string) => {
    e.stopPropagation();
    handleEditAction(id);
  };

  return (
    <>
      <div>
        <Button
          variant="contained"
          color="primary"
          onClick={handleCreate}
        >
          generate new Ledger
        </Button>

        <GenericDataTable
          title="Ledger List"
          columns={columns}
          fetchData={fetchEzags}
          refresh={!open}
        />
      </div>
      <Dialog open={open} onClose={handleClose} maxWidth="md" fullWidth>
        <DialogTitle>
          {content && React.isValidElement(content) ? "Details" : "Edit Item"}
        </DialogTitle>
        <DialogContent>{content}</DialogContent>
      </Dialog>
    </>
  );
};

export default ListLedger;
