import { Container } from "@mui/material";
import { kstsCreateKst } from "../../client/services.gen";
import { KstBase, Card, Kst, BasicUser } from "../../client/types.gen";

import client from "../../apiClientConfig"; //Do not remove

import ObjectEditor, {
  FieldConfig,
  FieldType,
} from "../../components/ObjectEditor";
import { useLoaderData } from "react-router-dom";
import { Kst_title } from "../../components/Titles";

export function generateFieldConfigs(): FieldConfig<KstBase>[] {
  return [
    { name: "kst_number", label: "kst_number", type: FieldType.STRING },
    { name: "name_de", label: "name_de", type: FieldType.STRING },
    { name: "name_en", label: "name_en", type: FieldType.STRING },
    { name: "owner", label: "owner", type: FieldType.STRING },
    { name: "active", label: "active", type: FieldType.BOOLEAN },
    { name: "budget_plus", label: "budget_plus (eg. 1500.0)", type: FieldType.NUMERIC },
    { name: "budget_minus", label: "budget_minus (eg. 1000.0)", type: FieldType.NUMERIC },
  ];
}

export default function GenerateKst(propIdString = "") {
  const fieldConfig = generateFieldConfigs();

  const initialElement: KstBase = {
    kst_number: "",
    name_de: "",
    name_en: "",
    owner: "",
    active: true,
    budget_plus: 0,
    budget_minus: 0,
  };

  const submitter = async (changes: KstBase) => {
    changes.budget_plus = changes.budget_plus * 100;
    changes.budget_minus = changes.budget_minus * 100;
    const response = await kstsCreateKst({ body: changes });
    if (response.error) {
      changes.budget_plus = changes.budget_plus / 100;
      changes.budget_minus = changes.budget_minus / 100;
      throw response.error;
    } else {
      //maybe navigate somewhere
      return;
    }
  };

  return (
    <Container>
      <ObjectEditor
        fieldConfigs={fieldConfig}
        header="Add Cost Center"
        submitter={submitter}
        initial={initialElement}
      />
    </Container>
  );
}
