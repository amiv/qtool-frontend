import React, { useState } from "react";
import { Button, Dialog, DialogContent, DialogTitle } from "@mui/material";
import { kstsReadKsts } from "../../client";
import GenericDataTable from "../../components/GenericDataTable";
import { handleDownload } from "../components/DownloadHandler";
import ObjectViewer from "../../components/ObjectViewer";
import EditKst from "./EditKst";
import GenerateKst from "./GenerateKst";

const withWrapper =
  (EditFunction: (id: string) => JSX.Element) =>
  ({
    propIdString,
    onClose,
  }: {
    propIdString: string;
    onClose: () => void;
  }) => {
    const content = EditFunction({propIdString, onClose});

    return (
      <div>
        {content}
        <Button onClick={onClose}>Close</Button>
      </div>
    );
  };

const EditKstWrapper = withWrapper(EditKst);
const CreateKstWrapper = withWrapper(GenerateKst);

const ListKst: React.FC = () => {
  const [open, setOpen] = useState(false);
  const [content, setContent] = useState<React.ReactNode | null>(null);
  const [refresh, setRefresh] = useState(false);

  const columns = [
    { name: "kst_number", label: "kst_number" },
    { name: "name_de", label: "name_de" },
    { name: "name_en", label: "name_en" },
    { name: "owner", label: "owner" },
    { name: "active", label: "activ" },
    { name: "budget_plus", label: "budget_plus" },
    { name: "budget_minus", label: "budget_minus" },
    { name: "edit", label: "edit" },
  ];

  /**
   * fetchEzags now supports server-side pagination.
   * It accepts search, sort, filters, page, and limit,
   * and returns an object with:
   *   - data: the transformed list of Kst items for the current page
   *   - total: the total number of records.
   */
  const fetchKsts = async ({
    search,
    sort,
    filters,
    page,
    limit,
  }: {
    search: string;
    sort: { column: string; direction: "asc" | "desc" } | null;
    filters: Record<string, any>;
    page: number;
    limit: number;
  }) => {
    const body: any = {
      search: search || null,
      sort: sort ? `${sort.column}:${sort.direction}` : null,
      page,
      limit,
      ...filters,
    };

    setRefresh(false);
    try {
      const response = await kstsReadKsts({ query: body });
      const results = response.data?.items || [];
      const total = response.data?.total || results.length;

      console.log(results);

      const transformedData = results.map((item: any) => ({
        id: item?.id || "",
        kst_number: item?.kst_number || "",
        name_de: item?.name_de || "",
        name_en: item?.name_en || "",
        owner: item?.owner || "",
        active: item?.active ?? true,
        budget_plus: (item?.budget_plus || 0) / 100.0,
        budget_minus: (item?.budget_minus || 0) / 100.0,
        // Render an Edit button in the "edit" column.
        edit: (
          <Button
            variant="contained"
            color="primary"
            onClick={() => handleEditAction(item?.id)}
            download
          >
            Edit
          </Button>
        ),
      }));

      return { data: transformedData, total };
    } catch (error) {
      console.error("Error fetching Ksts", error);
      return { data: [], total: 0 };
    }
  };

  const handleEditAction = (id: string) => {
    setOpen(true);
    setContent(<EditKstWrapper propIdString={id} onClose={handleClose} />);
  };

  const handleCreate = () => {
    setOpen(true);
    setContent(<CreateKstWrapper propIdString="" onClose={handleClose} />);
  };

  const handleClose = () => {
    setOpen(false);
    setContent(null);
  };

  const handleEdit = (e: React.MouseEvent<HTMLButtonElement>, id: string) => {
    e.stopPropagation();
    handleEditAction(id);
  };

  return (
    <>
      <div>
        <Button
          variant="contained"
          color="primary"
          onClick={handleCreate}
        >
          generate new Kst
        </Button>

        <GenericDataTable
          title="Kst List"
          columns={columns}
          fetchData={fetchKsts}
          refresh={!open}
        />
      </div>
      <Dialog open={open} onClose={handleClose} maxWidth="md" fullWidth>
        <DialogTitle>
          {content && React.isValidElement(content) ? "Details" : "Edit Item"}
        </DialogTitle>
        <DialogContent>{content}</DialogContent>
      </Dialog>
    </>
  );
};

export default ListKst;
