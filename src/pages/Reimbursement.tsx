import { Container } from "@mui/material";
import {
  filesUploadFile,
  reimbursementsCreateReimbursement,
} from "../client/services.gen";
import {
  ReimbursementCreate,
  Card,
  Kst,
  Ledger,
  BasicUser,
  FilesUploadFileData,
  q_state,
} from "../client/types.gen";

import client from "../apiClientConfig"; //Do not remove

import ObjectEditor, {
  FieldConfig,
  FieldType,
} from "../components/ObjectEditor";
import { useLoaderData } from "react-router-dom";
import { Kst_title, Ledger_title } from "../components/Titles";
import receiptHandler from "../components/ReceiptHandler";
import { q_stateSchema } from "../client";

export function generateFieldConfigs(
  kst: Kst[],
  ledger: Ledger[],
  qmode: boolean = false,
): FieldConfig<ReimbursementCreate>[] {
  let qfields = [] as FieldConfig<ReimbursementCreate>[];
  if (qmode === true) {
    qfields = [
      {
        name: "textcomment",
        label: "text comment",
        type: FieldType.COMMENT,
        comment: "these fields are only for the quästor:",
      },
      { name: "creditor.name", label: "name", type: FieldType.STRING },
      { name: "creditor.creator_id", label: "creator", type: FieldType.STRING },
      { name: "recipient", label: "recipient", type: FieldType.STRING },
      {
        name: "creditor.accounting_year",
        label: "accounting_year",
        type: FieldType.NUMERIC,
      },
      {
        name: "creditor.q_check",
        label: "q_check",
        type: FieldType.STRING,
        items: [
          { label: "open", value: q_state.OPEN },
          { label: "accepted", value: q_state.ACCEPTED },
          { label: "rejected", value: q_state.REJECTED },
        ],
      },
    ];
  }

  return [
    { name: "creditor.comment", label: "Description (What, which shop, and which event?)", type: FieldType.STRING },
    { name: "creditor.amount", label: "Amount (e.g. 123.45)", type: FieldType.CURRENCY },
    { name: "creditor.currency", label: "Currency", type: FieldType.STRING },
    {
      name: "creditor.kst_id",
      label: "KST | Cost Center",
      type: FieldType.STRING,
      items: kst.map((k) => ({ label: Kst_title(k), value: k.id ? k.id : "" })),
    },
    {
      name: "creditor.ledger_id",
      label: "Type of Expense",
      type: FieldType.STRING,
      items: ledger.map((l) => ({
        label: Ledger_title(l),
        value: l.id ? l.id : "",
      })),
    },
    { name: "receipt", label: "Please upload your receipt here:", type: FieldType.FILE },

    ...qfields,
  ];
}

export default function GenerateReimbursement() {
  const { kst, ledger, user, limitedLedger } = useLoaderData() as {
    kst: Kst[];
    ledger: Ledger[];
    user: BasicUser;
    limitedLedger: Ledger[];
  };
  const fieldConfig = generateFieldConfigs(kst, limitedLedger, false);

  const initialElement: ReimbursementCreate = {
    creditor: {
      kst_id: "",
      ledger_id: "",
      amount: 0,
      accounting_year: 2025,
      currency: "CHF",
      comment: "",
      q_check: q_state.OPEN,
      qcomment: "",
      name: "",
      creator_id: user.id ? user.id : "",
    },
    receipt: "",
    creator: user.id,
    recipient: user.id,
  };

  const submitter = async (changes: ReimbursementCreate) => {
    // Convert amount to cents before submitting
    

    changes.receipt = await receiptHandler(changes.receipt);
    
    changes.creditor.amount = Math.round(changes.creditor.amount * 100);
    const response = await reimbursementsCreateReimbursement({
      body: changes,
    });
    
    if (response.error) {
      changes.creditor.amount = Number((changes.creditor.amount / 100).toFixed(2));
      throw response.error;
    } else {
      window.location.href = "/ownList";

      return;
    }
  };

  return (
    <Container>
      <ObjectEditor
        fieldConfigs={fieldConfig}
        header="Request Reimbursement"
        description="Enter all required information below. Please note that a separate request must be
          submitted for each receipt. Grouping receipts is not permitted."
        submitter={submitter}
        initial={initialElement}
      />
    </Container>
  );
}
