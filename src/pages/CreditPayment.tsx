import { Container } from "@mui/material";
import { creditPaymentsCreateCreditPayment } from "../client/services.gen";
import {
  CreditPaymentCreate,
  Card,
  Kst,
  Ledger,
  BasicUser,
  q_state,
} from "../client/types.gen";

import client from "../apiClientConfig"; //Do not remove

import ObjectEditor, {
  FieldConfig,
  FieldType,
} from "../components/ObjectEditor";
import { useLoaderData } from "react-router-dom";
import { Kst_title, Ledger_title } from "../components/Titles";
import receiptHandler from "../components/ReceiptHandler";

export function generateFieldConfigs(
  kst: Kst[],
  ledger: Ledger[],
  qmode: boolean,
): FieldConfig<CreditPaymentCreate>[] {
  let qfields = [] as FieldConfig<CreditPaymentCreate>[];
  if (qmode === true) {
    qfields = [
      {
        name: "textcomment",
        label: "text comment",
        type: FieldType.COMMENT,
        comment: "these fields are only for the quästor:",
      },
      { name: "creditor.qcomment", label: "qcomment", type: FieldType.STRING },
      { name: "creditor.name", label: "name", type: FieldType.STRING },
      { name: "creditor.creator_id", label: "creator", type: FieldType.STRING },
      { name: "recipient", label: "recipient", type: FieldType.STRING },
      {
        name: "creditor.accounting_year",
        label: "accounting_year",
        type: FieldType.NUMERIC,
      },
      {
        name: "creditor.q_check",
        label: "q_check",
        type: FieldType.STRING,
        items: [
          { label: "open", value: q_state.OPEN },
          { label: "accepted", value: q_state.ACCEPTED },
          { label: "rejected", value: q_state.REJECTED },
        ],
      },
    ];
  }
  return [
    { name: "creditor.comment", label: "Description", type: FieldType.STRING },
    { name: "creditor.amount", label: "Amount", type: FieldType.CURRENCY },
    { name: "creditor.currency", label: "Currency", type: FieldType.STRING },
    {
      name: "creditor.kst_id",
      label: "KST | Cost Center",
      type: FieldType.STRING,
      items: kst.map((k) => ({ label: Kst_title(k), value: k.id ? k.id : "" })),
    },
    {
      name: "creditor.ledger_id",
      label: "Ledger",
      type: FieldType.STRING,
      items: ledger.map((l) => ({
        label: Ledger_title(l),
        value: l.id ? l.id : "",
      })),
    },
    {
      name: "textcomment",
      label: "text comment",
      type: FieldType.COMMENT,
      comment:
        "Choose the credit card you used. Each credit card is labeled at the back.",
    },
    {
      name: "card",
      label: "Creditcard",
      type: FieldType.STRING,
      items: [
        { label: "President", value: Card.PRESIDENT },
        { label: "Events", value: Card.EVENTS },
        { label: "Quaestor", value: Card.QUAESTOR },
      ],
    },
    { name: "receipt", label: "receipt", type: FieldType.FILE },
    ...qfields,
  ];
}

export default function GenerateCreditPayment() {
  const { kst, ledger, user, limitedLedger } = useLoaderData() as {
    kst: Kst[];
    ledger: Ledger[];
    user: BasicUser;
    limitedLedger: Ledger[];
  };
  const fieldConfig = generateFieldConfigs(kst, limitedLedger, false);

  const initialElement: CreditPaymentCreate = {
    creditor: {
      kst_id: "",
      ledger_id: "",
      amount: 0,
      accounting_year: 2025,
      currency: "CHF",
      comment: "",
      qcomment: "",
      name: "",
      creator_id: user.id,
    },
    receipt: "",
    card: "President",
  };

  const submitter = async (changes: CreditPaymentCreate) => {
    
    changes.receipt = await receiptHandler(changes.receipt);
    changes.creditor.amount = Math.round(changes.creditor.amount * 100);  // Convert amount to cents before submitting
    const response = await creditPaymentsCreateCreditPayment({ body: changes });
    if (response.error) {
      changes.creditor.amount = Number((changes.creditor.amount / 100).toFixed(2));
      throw response.error;
    } else {
      //maybe navigate somewhere
      window.location.href = "/ownList";
      return;
    }
  };

  return (
    <Container>
      <ObjectEditor
        fieldConfigs={fieldConfig}
        header="Add Credit Card Receipt"
        description="Enter all required information below. Please note that a separate
          request must be submitted for each receipt. Grouping receipts is not permitted."
        submitter={submitter}
        initial={initialElement}
      />
    </Container>
  );
}
