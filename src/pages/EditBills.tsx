import { CircularProgress, Container } from "@mui/material";
import {
  billsReadBill,
  billsUpdateBill,
  billsDeleteBill,
} from "../client/services.gen";
import {
  BillCreate,
  Kst,
  Ledger,
  BasicUser,
  BillPublic_Input,
  q_state,
} from "../client/types.gen";

import client from "../apiClientConfig"; // Do not remove

import ObjectEditor, {
  FieldConfig,
  FieldType,
} from "../components/ObjectEditor";
import { useLoaderData, useParams } from "react-router-dom";
import { Kst_title, Ledger_title } from "../components/Titles";
import { useEffect, useState } from "react";
import { generateFieldConfigs } from "./Bills";
import receiptHandler from "../components/ReceiptHandler";

interface EditBillsProps {
  propIdString: string;
  onClose: () => void;
  qmode?: boolean;
}

/**
 * EditBills component fetches bill payment data,
 * displays the ObjectEditor for editing, and calls onClose after a successful update.
 */
export default function EditBills({
  propIdString,
  onClose,
  qmode = false,
}: EditBillsProps) {
  const { idstring: urlidstring } = useParams<{ idstring: string }>();
  const idstring = urlidstring || propIdString;
  const { kst, ledger, user, limitedLedger } = useLoaderData() as {
    kst: Kst[];
    ledger: Ledger[];
    user: BasicUser;
    limitedLedger: Ledger[];
  };
  const [loading, setLoading] = useState<boolean>(true);
  const [initialElement, setInitialElement] = useState<BillPublic_Input | null>(
    null,
  );

  useEffect(() => {
    const fetchData = async () => {
      try {
        // Fetch existing bill payment data by ID
        console.log("Fetching bill payment data for ID:", idstring);
        const database = await billsReadBill({
          path: { id: idstring },
        });
        const data = database.data;
        setInitialElement({
          id: data?.id || "",
          creditor_id: data?.creditor_id || "",
          creditor: {
            id: data?.creditor?.id || "",
            kst_id: data?.creditor?.kst_id || "",
            ledger_id: data?.creditor?.ledger_id || "",
            amount: (data?.creditor?.amount || 0) / 100,
            accounting_year: data?.creditor?.accounting_year || 2025,
            currency: data?.creditor?.currency || "CHF",
            comment: data?.creditor?.comment || "",
            qcomment: data?.creditor?.qcomment || "",
            name: data?.creditor?.name || "",
            creator_id: data?.creditor?.creator_id || "",
            q_check: data?.creditor.q_check || q_state.OPEN,
            time_create: data?.creditor.time_create || Date.now(),
            time_modified: data?.creditor.time_modified || Date.now(),
          },
          receipt: data?.receipt || "",
          reference: data?.reference || "",
          iban: data?.iban || "",
          address_id: data?.address?.id || "",
          address: {
            id: data?.address?.id || "",
            name: data?.address?.name || "",
            address1: data?.address?.address1 || "",
            address2: data?.address?.address2 || "",
            address3: data?.address?.address3 || "",
            plz: data?.address?.plz || 0,
            city: data?.address?.city || "",
            country: data?.address?.country || "",
          },
        });
      } catch (error) {
        console.error("Failed to load bill payment data:", error);
      } finally {
        setLoading(false);
      }
    };

    fetchData();
  }, [idstring, user.id]);

  const submitter = async (changes: BillPublic_Input) => {
    // Convert amount to cents before submitting
    
    changes.receipt = await receiptHandler(changes.receipt);
    changes.creditor.amount = Math.round(changes.creditor.amount * 100);
    const response = await billsUpdateBill({
      body: changes,
      path: { id: idstring },
    });
    if (response.error) {
      changes.creditor.amount = Number((changes.creditor.amount / 100).toFixed(2));
      throw response.error;
    } else {
      // After a successful update, close the dialog.
      onClose();
      return;
    }
  };

  const deleter = async () => {
    const response = await billsDeleteBill({
      path: { id: idstring },
    });
    if (response.error) {
      throw response.error;
    } else {
      // Optionally, close the dialog after deletion.
      onClose();
      return;
    }
  };

  const fieldConfig = generateFieldConfigs(
    kst,
    limitedLedger,
    user.role === "quaestor",
  );

  if (loading || !initialElement) {
    return (
      <Container>
        <CircularProgress />
      </Container>
    );
  }

  return (
    <Container>
      <ObjectEditor
        fieldConfigs={fieldConfig}
        header="Edit Bill Payment"
        submitter={submitter}
        deleter={deleter}
        initial={initialElement}
        onClose={onClose}
      />
    </Container>
  );
}
