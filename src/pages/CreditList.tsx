import React from "react";
import { useLoaderData } from "react-router-dom";
import {
  creditPaymentsReadCreditPayments,
  kstsReadKsts,
  ledgersReadLedgers,
  authGetBasicUserInfo,
} from "../client/services.gen";
import { BasicUser, Kst, Ledger } from "../client/types.gen";
import GenericDataTable from "../components/GenericDataTable";
import { Button } from "@mui/material";

const CreditList: React.FC = () => {
  const { kst, ledger, user } = useLoaderData() as {
    kst: Kst[];
    ledger: Ledger[];
    user: BasicUser;
  };

  const columns = [
    { name: "name", label: "Name" },
    { name: "creditor__amount", label: "Amount" },
    {
      name: "card",
      label: "Card",
      options: {
        filterType: "checkbox",
        filterOptions: ["Event", "President", "Quaestor"],
      },
    },
    { name: "creditor__kst__kst_number", label: "KST Number" },
    { name: "creditor__kst__name_de", label: "KST Name" },
    { name: "creditor__ledger__name_de", label: "Ledger Name" },
    { name: "creditor__currency", label: "Currency" },
    { name: "receipt", label: "Receipt" },
    { name: "creator", label: "Creator" },
    { name: "edit", label: "edit", options: { filter: false, sort: false } },
  ];

  // Define the data fetching function for Credit Payments
  const fetchCreditPayments = async ({
    search,
    sort,
    filters,
  }: {
    search: string;
    sort: { column: string; direction: "asc" | "desc" } | null;
    filters: Record<string, any>;
  }) => {
    const body: any = {
      search: search || null,
      sort: sort ? `${sort.column}:${sort.direction}` : null,
      ...filters,
    };

    console.log("Fetching Credit Payments with:", body);

    try {
      const response = await creditPaymentsReadCreditPayments({ query: body });
      const results = response.data?.items || [];

      // Transform the fetched data to match the table's columns
      const transformedData = results.map((item: any) => ({
        name: item.name,
        creditor__amount: item.creditor?.amount,
        card: item.card,
        creditor__kst__kst_number:
          kst.find((k) => k.id === item.creditor?.kst_id)?.kst_number ||
          "Unknown",
        creditor__kst__name_de:
          kst.find((k) => k.id === item.creditor?.kst_id)?.name_de || "Unknown",
        creditor__ledger__name_de:
          ledger.find((l) => l.id === item.creditor?.ledger_id)?.name_de ||
          "Unknown",
        creditor__currency: item.creditor?.currency,
        receipt: item.receipt,
        creator: user.nethz,
        edit: (
          <Button
            variant="contained"
            color="primary"
            href={`/creditpayment/${item.id}`}
          >
            Edit
          </Button>
        ),
      }));

      return transformedData;
    } catch (error) {
      console.error("Error fetching Credit Payments:", error);
      return [];
    }
  };

  return (
    <GenericDataTable
      title="Credit Payments List"
      columns={columns}
      fetchData={fetchCreditPayments}
    />
  );
};

export default CreditList;
