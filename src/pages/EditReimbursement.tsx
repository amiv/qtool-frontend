import { CircularProgress, Container } from "@mui/material";
import {
  reimbursementsUpdateReimbursement,
  reimbursementsReadReimbursement,
  reimbursementsDeleteReimbursement,
} from "../client/services.gen";
import {
  ReimbursementPublic_Input,
  Kst,
  Ledger,
  BasicUser,
  q_state,
} from "../client/types.gen";

import client from "../apiClientConfig"; // Do not remove

import ObjectEditor, {
  FieldConfig,
  FieldType,
} from "../components/ObjectEditor";
import { useLoaderData, useParams } from "react-router-dom";
import { Kst_title, Ledger_title } from "../components/Titles";
import { useEffect, useState } from "react";
import { generateFieldConfigs } from "./Reimbursement";
import receiptHandler from "../components/ReceiptHandler";

interface EditReimbursementProps {
  propIdString: string;
  onClose: () => void;
}

/**
 * EditReimbursement component fetches reimbursement data,
 * displays the ObjectEditor for editing, and calls onClose after a successful update.
 */
export default function EditReimbursement({
  propIdString,
  onClose,
}: EditReimbursementProps) {
  console.log("EditReimbursement", propIdString);
  
  const { idstring: urlidstring } = useParams<{ idstring: string }>();
  const idstring = urlidstring || propIdString;
  const { kst, ledger, user, limitedLedger } = useLoaderData() as {
    kst: Kst[];
    ledger: Ledger[];
    user: BasicUser;
    limitedLedger: Ledger[];
  };
  const [loading, setLoading] = useState<boolean>(true);
  const [initialElement, setInitialElement] =
    useState<ReimbursementPublic_Input | null>(null);

  useEffect(() => {
    const fetchData = async () => {
      try {
        // Fetch existing reimbursement data by ID
        console.log("Fetching reimbursement data for ID:", idstring);
        const database = await reimbursementsReadReimbursement({
          path: { id: idstring },
        });
        const data = database.data;
        setInitialElement({
          id: data?.id || "",
          creditor_id: data?.creditor_id || "",
          creditor: {
            id: data?.creditor?.id || "",
            kst_id: data?.creditor?.kst_id || "",
            ledger_id: data?.creditor?.ledger_id || "",
            amount: (data?.creditor?.amount || 0) / 100,
            accounting_year: data?.creditor?.accounting_year || 2025,
            currency: data?.creditor?.currency || "CHF",
            comment: data?.creditor?.comment || "",
            qcomment: data?.creditor?.qcomment || "",
            name: data?.creditor?.name || "",
            creator_id: data?.creditor?.creator_id || "",
            q_check: data?.creditor.q_check || q_state.OPEN,
            time_create: data?.creditor.time_create || Date.now(),
            time_modified: data?.creditor.time_modified || Date.now(),
          },
          receipt: data?.receipt || "",
          recipient: data?.recipient || "",
          ezag_id: data?.ezag_id || null,
          ezag_timestamp: data?.ezag_timestamp || null,
        });
      } catch (error) {
        console.error("Failed to load Reimbursement data:", error);
      } finally {
        setLoading(false);
      }
    };

    fetchData();
  }, [idstring, user.id]);

  // Modify the submitter function to call onClose after a successful update.
  const submitter = async (changes: ReimbursementPublic_Input) => {
    
    changes.receipt = await receiptHandler(changes.receipt);
    changes.creditor.amount = Math.round(changes.creditor.amount * 100);
    const response = await reimbursementsUpdateReimbursement({
      body: changes,
      path: { id: idstring },
    });

    if (response.error) {
      changes.creditor.amount = Number((changes.creditor.amount / 100).toFixed(2));
      throw response.error;
    } else {
      // Call onClose to close the edit (and preview) dialog after a successful update.
      onClose();
      return;
    }
  };

  const deleter = async () => {
    const response = await reimbursementsDeleteReimbursement({
      path: { id: idstring },
    });
    if (response.error) {
      throw response.error;
    } else {
      // Optionally, call onClose here as well if you want to close the dialog after deletion.
      onClose();
      return;
    }
  };

  const fieldConfig = generateFieldConfigs(
    kst,
    limitedLedger,
    user.role === "quaestor"
  );

  if (loading || !initialElement) {
    return (
      <Container>
        <CircularProgress />
      </Container>
    );
  }

  return (
    <Container>
      <ObjectEditor
        fieldConfigs={fieldConfig}
        header="Edit reimbursement"
        submitter={submitter}
        deleter={deleter}
        initial={initialElement}
      />
    </Container>
  );
}
