import { CircularProgress, Container } from "@mui/material";
import {
  internalTransfersUpdateInternalTransfer,
  internalTransfersReadInternalTransfer,
  internalTransfersDeleteInternalTransfer,
} from "../client/services.gen";
import {
  InternalTransferCreate,
  Card,
  Kst,
  Ledger,
  BasicUser,
  InternalTransferPublic_Input,
  q_state,
} from "../client/types.gen";

import client from "../apiClientConfig"; // Do not remove

import ObjectEditor, {
  FieldConfig,
  FieldType,
} from "../components/ObjectEditor";
import { useLoaderData, useParams } from "react-router-dom";
import { Kst_title, Ledger_title } from "../components/Titles";
import { useEffect, useState } from "react";
import { generateFieldConfigs } from "./InternalTransfer";

interface EditInternalTransfersProps {
  propIdString: string;
  onClose: () => void;
  qmode?: boolean;
}

/**
 * EditInternalTransfers component fetches internal transfer data,
 * displays the ObjectEditor for editing, and calls onClose after a successful update or deletion.
 */
export default function EditInternalTransfers({
  propIdString,
  onClose,
  qmode = false,
}: EditInternalTransfersProps) {
  const { idstring: urlidstring } = useParams<{ idstring: string }>();
  const idstring = urlidstring || propIdString;
  const { kst, ledger, user, limitedLedger } = useLoaderData() as {
    kst: Kst[];
    ledger: Ledger[];
    user: BasicUser;
    limitedLedger: Ledger[];
  };
  const [loading, setLoading] = useState<boolean>(true);
  const [initialElement, setInitialElement] =
    useState<InternalTransferPublic_Input | null>(null);

  useEffect(() => {
    const fetchData = async () => {
      try {
        // Fetch existing internal transfer data by ID
        console.log("Fetching internal transfer data for ID:", idstring);
        const database = await internalTransfersReadInternalTransfer({
          path: { id: idstring },
        });
        const data = database.data;
        setInitialElement({
          id: data?.id,
          creditor_id: data?.creditor_id,
          debitor_id: data?.debitor_id,
          creditor: {
            id: data?.creditor?.id || "",
            kst_id: data?.creditor?.kst_id || "",
            ledger_id: data?.creditor?.ledger_id || "",
            amount: (data?.creditor?.amount || 0) / 100,
            accounting_year: data?.creditor?.accounting_year || 2025,
            currency: data?.creditor?.currency || "CHF",
            comment: data?.creditor?.comment || "",
            qcomment: data?.creditor?.qcomment || "",
            name: data?.creditor?.name || "",
            creator_id: data?.creditor?.creator_id || "",
            q_check: data?.creditor.q_check || q_state.OPEN,
            time_create: data?.creditor.time_create || Date.now(),
            time_modified: data?.creditor.time_modified || Date.now(),
          },
          debitor: {
            id: data?.debitor?.id || "",
            kst_id: data?.debitor?.kst_id || "",
            ledger_id: data?.debitor?.ledger_id || "",
            amount: (data?.debitor?.amount || 0) / 100,
            comment: data?.debitor?.comment || "0",
            q_comment: data?.debitor?.q_comment || "",
            name: data?.debitor?.name || "",
            creator_id: data?.debitor?.creator_id || "",
            q_check: data?.debitor?.q_check || q_state.OPEN,
            time_create: data?.creditor.time_create || Date.now(),
            time_modified: data?.creditor.time_modified || Date.now(),
            
          },
          amount: data?.amount || 0,
        });
      } catch (error) {
        console.error("Failed to load internal transfer data:", error);
      } finally {
        setLoading(false);
      }
    };

    fetchData();
  }, [idstring, user.id]);

  const submitter = async (changes: InternalTransferPublic_Input) => {
    // Convert amounts to cents before submitting
    changes.creditor.amount = Math.round(changes.creditor.amount * 100);
    changes.debitor.amount = changes.creditor.amount;
    changes.amount = changes.creditor.amount;
    changes.debitor.comment = changes.creditor.comment;
    changes.debitor.q_comment = changes.creditor.qcomment;
    const response = await internalTransfersUpdateInternalTransfer({
      body: changes,
      path: { id: idstring },
    });
    if (response.error) {
      changes.creditor.amount = Number((changes.creditor.amount / 100).toFixed(2));
      changes.debitor.amount = changes.creditor.amount;
      changes.amount = changes.creditor.amount;
      throw response.error;
    } else {
      // Close the dialog after a successful update.
      onClose();
      return;
    }
  };

  const deleter = async () => {
    const response = await internalTransfersDeleteInternalTransfer({
      path: { id: idstring },
    });
    if (response.error) {
      
      throw response.error;
    } else {
      // Close the dialog after deletion.
      onClose();
      return;
    }
  };

  const fieldConfig = generateFieldConfigs(
    kst,
    limitedLedger,
    user.role === "quaestor"
  );

  if (loading || !initialElement) {
    return (
      <Container>
        <CircularProgress />
      </Container>
    );
  }

  return (
    <Container>
      <ObjectEditor
        fieldConfigs={fieldConfig}
        header="Edit Internal Transfer"
        submitter={submitter}
        deleter={deleter}
        initial={initialElement}
        onClose={onClose}
      />
    </Container>
  );
}
