import React, { useState } from "react";
import { useLoaderData } from "react-router-dom";
import {
  combinedReadUncheckedCombinedPayments,
  usersReadUser,
  reimbursementsUpdateReimbursement,
  creditPaymentsUpdateCreditPayment,
  internalTransfersUpdateInternalTransfer,
  reimbursementsUpdateReimbursement as updateReimbursement, // example re-export if needed
} from "../client/services.gen";
import GenericEditableTable, {
  fetchDataForView,
} from "../components/GenericEditableTable";
import { CombinedCreditor, q_state } from "../client/types.gen";
import { useSnackbar } from "./SnackbarProvider"; // Import Snackbar hook
import receiptHandler from "../components/ReceiptHandler";
import { PreviewHeader } from "../components/PreviewHeader";

const CombinedList: React.FC = () => {
  const { kst, ledger, user } = useLoaderData() as {
    kst: any[];
    ledger: any[];
    user: any;
  };
  const { showSnackbar } = useSnackbar(); // Use Snackbar hook
  const [qcomment, setQcomment] = useState("");

  /**
   * fetchCombinedPayments now supports server-side pagination.
   * It accepts search, sort, filters, page, and rowsPerPage, and returns an object with:
   *   - data: the transformed list for the current page
   *   - total: the total number of records.
   */
  const fetchCombinedPayments = async ({
    search,
    sort,
    filters,
    page,
    rowsPerPage,
  }: {
    search: string;
    sort: { column: string; direction: "asc" | "desc" } | null;
    filters: Record<string, any>;
    page: number;
    rowsPerPage: number;
  }) => {
    try {
      const response = await combinedReadUncheckedCombinedPayments({
        query: {
          search,
          sort: sort ? `${sort.column}:${sort.direction}` : null,
          page,
          rowsPerPage,
          ...filters,
        },
      });
      if (response.error) {
        throw new Error(
          "Error fetching combined payments: " + response.error.detail
        );
      }
      const results = response.data.items;
      const total = response.data.total || results.length;
      const transformedData = results.map((item: CombinedCreditor) => ({
        name: item.creditor.name,
        creditor__amount: (item.creditor?.amount) / 100.0,
        card: item.card,
        creditor__kst__kst_number:
          kst.find((k) => k.id === item.creditor?.kst_id)?.kst_number ||
          "Unknown",
        creditor__kst__name_de:
          kst.find((k) => k.id === item.creditor?.kst_id)?.name_de || "Unknown",
        creditor__ledger__name_de:
          ledger.find((l) => l.id === item.creditor?.ledger_id)?.name_de ||
          "Unknown",
        creditor__currency: item.creditor?.currency,
        creditor__qcomment: item.creditor?.qcomment,
        q_check: item.creditor?.q_check,
        creator: item.creator,
        type: item.type,
        id: item.id,
        reference: item.reference,
        iban: item.iban,
        comment: item.creditor.comment,
        reimbursement__recipient: item.reimbursement__recipient,
      }));
      return { data: transformedData, total };
    } catch (error: any) {
      showSnackbar(
        error.message || "Failed to fetch combined payments",
        "error"
      );
      return { data: [], total: 0 };
    }
  };

  function previewHeader(type: string, id: string, data: any) {
    return <PreviewHeader type={type} id={id} data={data} />;
  }

  return (
    <GenericEditableTable
      title="Combined Payments List"
      additionalColumns={[{ name: "check", label: "Check" }]}
      fetchFunction={fetchCombinedPayments}
      kst={kst}
      ledger={ledger}
      previewHeader={previewHeader}
    />
  );
};

export default CombinedList;
