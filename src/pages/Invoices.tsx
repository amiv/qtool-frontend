import React, { useEffect, useState } from "react";
import { Container } from "@mui/material";
import {
  InternalTransferCreate,
  Kst,
  Ledger,
  BasicUser,
  q_state,
  InvoiceCreate,
  MwstType,
  Item,
  ItemstoInvoiceCreate,
} from "../client/types.gen";

import client from "../apiClientConfig"; // Do not remove

import ObjectEditor, {
  FieldConfig,
  FieldType,
} from "../components/ObjectEditor";
import { useLoaderData } from "react-router-dom";
import { Kst_title, Ledger_title } from "../components/Titles";
import {
  invoicesCreateInvoice,
  itemsReadItems,
  kstsCreateKst,
} from "../client";

/**
 * Generates field configurations for the InvoiceCreate object.
 *
 * @param kst - Array of cost centers.
 * @param ledger - Array of ledgers.
 * @param items_list - Array of invoice items.
 * @param qmode - Flag indicating if quästor-specific fields should be added.
 * @returns Array of field configurations for the invoice object.
 */
export function generateFieldConfigs(
  kst: Kst[],
  ledger: Ledger[],
  items_list: Item[],
  qmode: boolean
): FieldConfig<InvoiceCreate>[] {
  // Define quästor-specific fields if qmode is active.
  let qfields: FieldConfig<InvoiceCreate>[] = [];
  if (qmode === true) {
    qfields = [
      {
        name: "debitor.q_comment",
        label: "q comment",
        type: FieldType.STRING,
        comment: "These fields are only for the quästor:",
      },
      {
        name: "debitor.q_check",
        label: "q check",
        type: FieldType.STRING,
        items: [
          { label: "open", value: q_state.OPEN },
          { label: "accepted", value: q_state.ACCEPTED },
          { label: "rejected", value: q_state.REJECTED },
        ],
      },
    ];
  }

  return [
    // Main invoice fields
    { name: "invoice_date", label: "Invoice Date", type: FieldType.DATE },
    { name: "payinterval", label: "Pay Interval (Days)", type: FieldType.NUMERIC },
    { name: "text_comment", label: "Text Comment", type: FieldType.COMMENT },
    { name: "mwst_type", label: "Mwst Type", type: FieldType.STRING },
    { name: "our_reference", label: "Our Reference", type: FieldType.STRING },
    { name: "your_reference", label: "Your Reference", type: FieldType.STRING },
    {
      name: "items",
      label: "Items",
      type: FieldType.MULTI_ITEM,
      comment: "List of invoice items",
      items: items_list.map((i) => ({
        label: i.description_de ? i.description_de : "",
        value: i.id ? i.id : "",
      })),
    },
    // Debitor fields (object properties of debitor)
    { name: "debitor.comment", label: "Debitor Comment", type: FieldType.STRING },
    { name: "debitor.name", label: "Debitor Name", type: FieldType.STRING },
    {
      name: "debitor.kst_id",
      label: "Debitor Cost Center (KST)",
      type: FieldType.STRING,
      items: kst.map((k) => ({
        label: Kst_title(k),
        value: k.id ? k.id : "",
      })),
    },
    {
      name: "debitor.ledger_id",
      label: "Debitor Ledger",
      type: FieldType.STRING,
      items: ledger.map((l) => ({
        label: Ledger_title(l),
        value: l.id ? l.id : "",
      })),
    },
    { name: "debitor.creator_id", label: "Creator ID", type: FieldType.STRING },
    {
      name: "debitor.accounting_year",
      label: "Accounting Year",
      type: FieldType.NUMERIC,
    },
    // InvoiceAddress fields (object properties of address)
    { name: "address.name", label: "Recipient Name", type: FieldType.STRING },
    { name: "address.address1", label: "Address Line 1", type: FieldType.STRING },
    { name: "address.address2", label: "Address Line 2", type: FieldType.STRING },
    { name: "address.address3", label: "Address Line 3", type: FieldType.STRING },
    { name: "address.plz", label: "Postal Code", type: FieldType.STRING },
    { name: "address.city", label: "City", type: FieldType.STRING },
    { name: "address.country", label: "Country", type: FieldType.STRING },
    // Append quästor-specific fields, if any.
    ...qfields,
  ];
}

/**
 * Component for generating an Invoice form.
 *
 * Fetches necessary data asynchronously using hooks and renders an ObjectEditor.
 */
export default function GenerateInvoice(propIdString = "") {
  const { kst, ledger, user } = useLoaderData() as {
    kst: Kst[];
    ledger: Ledger[];
    user: BasicUser;
  };

  const [items, setItems] = useState<Item[]>([]);
  const [loading, setLoading] = useState<boolean>(true);

  useEffect(() => {
    /**
     * Fetches invoice items from the server.
     */
    const fetchItems = async () => {
      try {
        const fetchedItems = await itemsReadItems();
        setItems(fetchedItems.data?.items || []);
      } catch (error) {
        console.error("Error fetching items:", error);
      } finally {
        setLoading(false);
      }
    };

    fetchItems();
  }, [kst]);

  // Show a loading indicator until the items are fetched.
  if (loading) {
    return <Container>Loading...</Container>;
  }
  console.log("items", items);
  const fieldConfig = generateFieldConfigs(
    kst,
    ledger,
    items,
    user.role === "quastor"
  );

  const initialElement: InvoiceCreate = {
    debitor_id: "",
    address_id: "",
    invoice_date: new Date(),
    payinterval: 0,
    text_comment: "",
    mwst_type: MwstType.EXKL,
    our_reference: "",
    your_reference: "",
    items: [],
    debitor: {
      comment: "",
      name: "",
      kst_id: "",
      ledger_id: "",
      creator_id: user.id,
      accounting_year: 2025,
      amount: 10,
    },
    address: {
      name: "",
      address1: "",
      address2: "",
      address3: "",
      plz: "",
      city: "",
      country: "",
    },
  };

  /**
   * Handles form submission for creating a new invoice.
   *
   * @param changes - The updated InvoiceCreate object from the form.
   */
  const submitter = async (changes: InvoiceCreate) => {
    console.log("Before conversion:", changes);
  
    // Convert debitor.amount to smallest unit (e.g., cents)
    changes.debitor.amount = changes.debitor.amount * 100;
  
    // Convert multi-item entries (an object) into an array of ItemstoInvoiceCreate.
    // The multi-item field in changes.items is an object mapping item IDs to the entered value.
    const itemsMapping = changes.items;
    const itemstoinvoice: ItemstoInvoiceCreate[] = Object.entries(itemsMapping).map(
      ([itemId, value]) => {
        // Attempt to parse the entered value as a number.
        // If parsing fails, we default the count to 1.
        const parsedCount = Number(value);
        return {
          item_id: itemId,
          count: isNaN(parsedCount) ? 1 : parsedCount,
          amount: 10, // Default amount value
          comment: "", // Default comment value
        };
      }
    );
  
    // Replace the original items with the converted array.
    changes.items = itemstoinvoice;
  
    console.log("After conversion:", changes);
  
    const response = await invoicesCreateInvoice({ body: changes });
    if (response.error) {
      changes.debitor.amount = changes.debitor.amount/100;
      throw response.error;
    } else {
      // Optionally navigate somewhere after a successful submission.
      return;
    }
  };

  return (
    <Container>
      <ObjectEditor
        fieldConfigs={fieldConfig}
        header="Add Invoice"
        submitter={submitter}
        initial={initialElement}
      />
    </Container>
  );
}
