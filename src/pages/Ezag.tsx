import React, { useState } from "react";
import { Button } from "@mui/material";
import { ezagsGenerateEzag, ezagsReadEzags } from "../client";
import GenericDataTable from "../components/GenericDataTable";
import { handleDownload } from "../components/DownloadHandler";
import { useSnackbar } from "./SnackbarProvider"; // Import Snackbar hook

const EzagPage: React.FC = () => {
  const columns = [
    { name: "name", label: "Name" },
    { name: "id", label: "ID" },
    { name: "size", label: "Size" },
    { name: "time_create", label: "Created At" },
    { name: "link", label: "Download" },
  ];
  const [refresh, setRefresh] = useState(false);
  const { showSnackbar } = useSnackbar(); // Use Snackbar hook

  /**
   * fetchEzags now supports server‑side pagination.
   * It accepts search, sort, filters, page, and rowsPerPage, and returns an object with:
   *   - data: the transformed list of ezag items for the current page
   *   - total: the total number of records.
   */
  const fetchEzags = async ({
    search,
    sort,
    filters,
    page,
    rowsPerPage,
  }: {
    search: string;
    sort: { column: string; direction: "asc" | "desc" } | null;
    filters: Record<string, any>;
    page: number;
    rowsPerPage: number;
  }) => {
    const body: any = {
      search: search || null,
      sort: sort ? `${sort.column}:${sort.direction}` : null,
      page,
      rowsPerPage,
      ...filters,
    };
    setRefresh(false);
    try {
      const response = await ezagsReadEzags({ query: body });
      const results = response.data?.items || [];
      const total = response.data?.total || results.length;
      const transformedData = results.map((item: any) => ({
        name: item.name,
        id: item.id,
        time_create: String(item.time_create),
        size: item.size,
        link: (
          <Button
            variant="contained"
            color="primary"
            onClick={() => handleDownload(item.link)}
            download
          >
            Download
          </Button>
        ),
      }));
      return { data: transformedData, total };
    } catch (error: any) {
      console.error("Error fetching ezags", error);
      showSnackbar(
        error.message || "An error occurred while fetching ezags",
        "error"
      );
      return { data: [], total: 0 };
    }
  };

  const generateEzag = async () => {
    try {
      const response = await ezagsGenerateEzag();
      if (response.error) {
        throw new Error("Unable to generate ezag: " + response.error.detail);
      }
      const result = response.data;
      setRefresh(true);
      showSnackbar("Ezag generated successfully", "success");
      console.log(result);
    } catch (error: any) {
      console.error("Error generating ezag", error);
      showSnackbar(
        error.message || "An error occurred while generating ezag",
        "error"
      );
    }
  };

  return (
    <div>
      <Button variant="contained" color="primary" onClick={generateEzag}>
        Generate New Ezag
      </Button>

      <GenericDataTable
        title="Ezag List"
        columns={columns}
        fetchData={fetchEzags}
        refresh={refresh}
      />
    </div>
  );
};

export default EzagPage;
