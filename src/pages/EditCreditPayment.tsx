import { useState, useEffect } from "react";
import { Container, CircularProgress } from "@mui/material";
import {
  creditPaymentsReadCreditPayment,
  creditPaymentsUpdateCreditPayment,
  creditPaymentsDeleteCreditPayment,
} from "../client/services.gen";
import {
  Card,
  Kst,
  Ledger,
  BasicUser,
  CreditPaymentPublic_Input,
  q_state,
} from "../client/types.gen";

import client from "../apiClientConfig"; // Do not remove

import ObjectEditor, {
  FieldConfig,
  FieldType,
} from "../components/ObjectEditor";
import { useLoaderData, useParams } from "react-router-dom";
import { Kst_title, Ledger_title } from "../components/Titles";
import { generateFieldConfigs } from "./CreditPayment";
import receiptHandler from "../components/ReceiptHandler";

interface EditCreditPaymentProps {
  propIdString?: string;
  onClose: () => void;
  qmode?: boolean;
}

/**
 * EditCreditPayment component fetches credit payment data,
 * displays the ObjectEditor for editing, and calls onClose after a successful update or deletion.
 */
export default function EditCreditPayment({
  propIdString = "",
  onClose,
  qmode = false,
}: EditCreditPaymentProps) {
  const { idstring: urlidstring } = useParams<{ idstring: string }>();
  const idstring = urlidstring || propIdString;
  const { kst, ledger, user, limitedLedger } = useLoaderData() as {
    kst: Kst[];
    ledger: Ledger[];
    user: BasicUser;
    limitedLedger: Ledger[];
  };

  const [loading, setLoading] = useState<boolean>(true);
  const [initialElement, setInitialElement] =
    useState<CreditPaymentPublic_Input | null>(null);

  useEffect(() => {
    const fetchData = async () => {
      try {
        // Fetch existing credit payment data by ID
        console.log("Fetching credit payment data for ID:", idstring);
        const database = await creditPaymentsReadCreditPayment({
          path: { id: idstring },
        });
        const data = database.data;
        setInitialElement({
          id: data?.id || "",
          creditor_id: data?.creditor_id || "",
          creditor: {
            id: data?.creditor?.id || "",
            kst_id: data?.creditor?.kst_id || "",
            ledger_id: data?.creditor?.ledger_id || "",
            amount: (data?.creditor?.amount || 0) / 100,
            accounting_year: data?.creditor?.accounting_year || 2025,
            currency: data?.creditor?.currency || "CHF",
            comment: data?.creditor?.comment || "",
            qcomment: data?.creditor?.qcomment || "",
            name: data?.creditor?.name || "",
            creator_id: data?.creditor?.creator_id || "",
            q_check: data?.creditor.q_check || q_state.OPEN,
            time_create: data?.creditor.time_create || Date.now(),
            time_modified: data?.creditor.time_modified || Date.now(),
          },
          receipt: data?.receipt || "",
          card: data?.card || "President",
        });
      } catch (error) {
        console.error("Failed to load credit payment data:", error);
      } finally {
        setLoading(false);
      }
    };

    fetchData();
  }, [idstring, user.id]);

  const submitter = async (changes: CreditPaymentPublic_Input) => {
    // Convert amount to cents before submitting
    
    changes.receipt = await receiptHandler(changes.receipt);
    changes.creditor.amount = Math.round(changes.creditor.amount * 100);
    const response = await creditPaymentsUpdateCreditPayment({
      body: changes,
      path: { id: idstring },
    });
    if (response.error) {
      changes.creditor.amount = Number((changes.creditor.amount / 100).toFixed(2));
      throw response.error;
    } else {
      // Close the dialog after a successful update.
      onClose();
      return;
    }
  };

  const deleter = async () => {
    const response = await creditPaymentsDeleteCreditPayment({
      path: { id: idstring },
    });
    if (response.error) {
      throw response.error;
    } else {
      // Close the dialog after deletion.
      onClose();
      return;
    }
  };

  const fieldConfig = generateFieldConfigs(
    kst,
    limitedLedger,
    user.role === "quaestor",
  );

  if (loading || !initialElement) {
    return (
      <Container>
        <CircularProgress />
      </Container>
    );
  }

  return (
    <Container>
      <ObjectEditor
        fieldConfigs={fieldConfig}
        header="Edit Credit Payment"
        submitter={submitter}
        deleter={deleter}
        initial={initialElement}
        onClose={onClose}
      />
    </Container>
  );
}
