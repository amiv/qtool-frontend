import React from "react";
import { useLoaderData } from "react-router-dom";
import { combinedReadOwnCombinedPayments } from "../client/services.gen";
import GenericEditableTable from "../components/GenericEditableTable";
import { BasicUser, CombinedCreditor, Kst, Ledger } from "../client/types.gen";
import { Container, Typography } from "@mui/material";

const OwnList: React.FC = () => {
  const { kst, ledger, user, limitedLedger } = useLoaderData() as {
    kst: Kst[];
    ledger: Ledger[];
    user: BasicUser;
    limitedLedger?: any;
  };

  /**
   * fetchCombinedPayments now supports server‑side pagination.
   * It accepts search, sort, filters, page, and limit, and returns an object with:
   *   - data: the current page's data (transformed for the table)
   *   - total: the total number of records.
   */
  const fetchCombinedPayments = async ({
    search,
    sort,
    filters,
    page,
    limit,
  }: {
    search: string;
    sort: { column: string; direction: "asc" | "desc" } | null;
    filters: Record<string, any>;
    page: number;
    limit: number;
  }) => {
    const response = await combinedReadOwnCombinedPayments({
      query: {
        search,
        sort: sort ? `${sort.column}:${sort.direction}` : null,
        page,
        limit,
        ...filters,
      },
    });
    if (response.error) {
      console.error("Error fetching combined payments:", response.error);
      return { data: [], total: 0 };
    }
    const results = response.data.items;
    const total = response.data.total || results.length;
    const data = results.map((item: CombinedCreditor) => ({
      name: item.creditor.name,
      creditor__amount: item.creditor?.amount / 100.0,
      card: item.card,
      creditor__kst__kst_number:
        kst.find((k) => k.id === item.creditor?.kst_id)?.kst_number ||
        "Unknown",
      creditor__kst__name_de:
        kst.find((k) => k.id === item.creditor?.kst_id)?.name_de || "Unknown",
      creditor__ledger__name_de:
        ledger.find((l) => l.id === item.creditor?.ledger_id)?.name_de ||
        "Unknown",
      creditor__currency: item.creditor?.currency,
      q_check: item.creditor?.q_check,
      type: item.type,
      id: item.id,
      reference: item.reference,
      comment: item.creditor.comment,
    }));
    return { data, total };
  };

  return (
    <Container>
      <Typography variant="h4" component="h1" gutterBottom>
        My Requests and Payments
      </Typography>
      In this list, you can find all the requests you have submitted, including
      reimbursements, credit card receipts, bills, and internal transfers. You
      can filter the list, edit requests that have not yet been approved, export
      the requests, and download your receipts. Additionally, the status of each
      request is displayed.
      <GenericEditableTable
        title="My Requests"
        fetchFunction={fetchCombinedPayments}
        kst={kst}
        ledger={ledger}
      />
    </Container>
  );
};

export default OwnList;
