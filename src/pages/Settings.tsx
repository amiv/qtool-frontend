import React, { useEffect, useState } from "react";
import { Container, CircularProgress } from "@mui/material";
import {
  authGetBasicUserInfo,
  usersReadUser,
  usersUpdateUser,
} from "../client/services.gen";
import {
  DbUserCreate,
  BasicUser,
  DbUserPublic,
  Kst,
  Ledger,
} from "../client/types.gen";
import { DbUserCreateSchema } from "../client";

import ObjectEditor, {
  FieldConfig,
  FieldType,
} from "../components/ObjectEditor";
import { useLoaderData } from "react-router-dom";

const fieldConfig: FieldConfig<DbUserCreate>[] = [
  {
    name: "textcomment",
    label: "text comment",
    type: FieldType.COMMENT,
    comment:
      "Please enter your bank details. Any reimbursements will be sent to this account.",
  },
  { name: "iban", label: "IBAN", type: FieldType.STRING },

  { name: "address.name", label: "Name", type: FieldType.STRING },
  {
    name: "address.address1",
    label: "Optional Addr field",
    type: FieldType.STRING,
  },
  { name: "address.address2", label: "street and Nr", type: FieldType.STRING },
  { name: "address.address3", label: "Optional ", type: FieldType.STRING },
  { name: "address.plz", label: "PLZ", type: FieldType.STRING },
  { name: "address.city", label: "City", type: FieldType.STRING },
  { name: "address.country", label: "Country", type: FieldType.STRING },
];
async function fetchBasicUserInfo(): Promise<BasicUser> {
  const response = await authGetBasicUserInfo();
  if (response.error) {
    throw response.error;
  }
  return response.data;
}

export default function SettingsPage() {
  const [basicUser, setBasicUser] = useState<BasicUser | null>(null);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState<string | null>(null);
  const [initialElement, setInitialElement] = useState<DbUserPublic | null>(
    null,
  );

  const { kst, ledger, user } = useLoaderData() as {
    kst: Kst[];
    ledger: Ledger[];
    user: BasicUser;
  };
  useEffect(() => {
    const loadUser = async () => {
      try {
        const userInfo = await fetchBasicUserInfo();
        setBasicUser(userInfo);
        if (!userInfo) {
          throw new Error("Failed to fetch user info");
        }
        // Fetch existing credit payment data by ID
        console.log("Fetching user data for ID:", userInfo.id);

        const database = await usersReadUser({
          path: { user_id: userInfo.id },
        });
        const data = database.data;
        if (!data) {
          throw new Error("Failed to load user data");
        }
        setInitialElement({
          id: userInfo.id,
          nethz: userInfo.nethz,
          iban: data?.iban || "",
          address_id: data?.address_id || "",
          address: {
            id: data?.address?.id || "",
            name: data?.address?.name || "",
            address1: data?.address?.address1 || "",
            address2: data?.address?.address2 || "",
            address3: data?.address?.address3 || "",
            plz: data?.address?.plz || "0",
            city: data?.address?.city || "",
            country: data?.address?.country || "",
          },
        });
      } catch (err: any) {
        setError(err.message || "Failed to fetch user info");
      } finally {
        setLoading(false);
      }
    };

    loadUser();
  }, [user]);
  if (!basicUser) {
    return <Container>No user data available</Container>;
  }

  const submitter = async (changes: DbUserPublic) => {
    const response = await usersUpdateUser({
      body: changes,
      path: { user_id: basicUser.id },
    });
    if (response.error) {
      throw response.error;
    } else {
      //navigate to /
      window.location.href = "/";

      return;
    }
  };

  if (loading || !initialElement) {
    return (
      <Container>
        <CircularProgress />
      </Container>
    );
  }

  return (
    <Container>
      <ObjectEditor
        fieldConfigs={fieldConfig}
        header="Settings"
        submitter={submitter}
        initial={initialElement}
      />
    </Container>
  );
}
