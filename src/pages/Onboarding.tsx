import React, { useEffect, useState } from "react";
import {
  Container,
  Typography,
  Stack,
  Button,
  TextField,
  Alert,
  CircularProgress,
} from "@mui/material";
import { usersCreateUser, authGetBasicUserInfo } from "../client/services.gen";
import { DbUserCreate, BasicUser } from "../client/types.gen";
import { DbUserCreateSchema } from "../client";

import ObjectEditor, {
  FieldConfig,
  FieldType,
} from "../components/ObjectEditor";

const fieldConfig: FieldConfig<DbUserCreate>[] = [
  {
    name: "textcomment",
    label: "text comment",
    type: FieldType.COMMENT,
    comment:
      "Please enter your bank details. Any reimbursements will be sent to this account.",
  },
  { name: "iban", label: "IBAN", type: FieldType.STRING },

  { name: "address.name", label: "Name", type: FieldType.STRING },
  {
    name: "address.address1",
    label: "Optional Addr field",
    type: FieldType.STRING,
  },
  { name: "address.address2", label: "street and Nr", type: FieldType.STRING },
  { name: "address.address3", label: "Optional ", type: FieldType.STRING },
  { name: "address.plz", label: "PLZ", type: FieldType.STRING },
  { name: "address.city", label: "City", type: FieldType.STRING },
  { name: "address.country", label: "Country", type: FieldType.STRING },
];
async function fetchBasicUserInfo(): Promise<BasicUser> {
  const response = await authGetBasicUserInfo();
  if (response.error) {
    throw response.error;
  }
  return response.data;
}

export default function OnboardingPage() {
  const [basicUser, setBasicUser] = useState<BasicUser | null>(null);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState<string | null>(null);

  useEffect(() => {
    const loadUser = async () => {
      try {
        const userInfo = await fetchBasicUserInfo();
        setBasicUser(userInfo);
      } catch (err: any) {
        setError(err.message || "Failed to fetch user info");
      } finally {
        setLoading(false);
      }
    };

    loadUser();
  }, []);
  if (!basicUser) {
    return <Container>No user data available</Container>;
  }

  const initialElement: DbUserCreate = {
    id: basicUser.id,
    iban: "",
    address: {
      name: "",
      address1: "",
      address2: "",
      address3: "",
      plz: 0,
      city: "",
      country: "",
    },
  };
  const submitter = async (changes: DbUserCreate) => {
    const response = await usersCreateUser({ body: changes });
    if (response.error) {
      throw response.error;
    } else {
      //navigate to /
      window.location.href = "/";

      return;
    }
  };

  return (
    <Container>
      <ObjectEditor
        fieldConfigs={fieldConfig}
        header="Onboarding"
        submitter={submitter}
        initial={initialElement}
      />
    </Container>
  );
}
