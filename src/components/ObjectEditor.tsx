import React, { useState, useEffect } from "react";
import {
  Paper,
  Stack,
  Typography,
  Box,
  Button,
  TextField,
  FormControl,
  InputLabel,
  Select,
  MenuItem,
  ButtonGroup,
  Dialog,
  DialogTitle,
  DialogContentText,
  DialogContent,
  DialogActions,
  Autocomplete,
  IconButton,
  Grid,
  Chip,
  FormControlLabel,
} from "@mui/material";
import DeleteIcon from "@mui/icons-material/Delete";
import { DateTimePicker } from "@mui/x-date-pickers";
import dayjs from "dayjs";
import { useSnackbar } from "../pages/SnackbarProvider";
import { Camera, CloudUpload, PhotoCamera, UploadFile } from "@mui/icons-material";

/**
 * Enum representing the different types of fields.
 */
export enum FieldType {
  STRING,
  NUMERIC,
  CURRENCY,
  BOOLEAN,
  DATETIME,
  COMMENT,
  FILE, // Added FILE type
  MULTI_ITEM, // MULTI_ITEM for a multi-item selector with inline editing
}

/**
 * Interface for select menu items.
 *
 * An optional `id` property can be provided. When present, it will be used
 * as the unique key for a row rather than a randomly generated id.
 */
export interface SelectMenuItem {
  id?: string | number;
  label: string;
  value: string | number;
}

/**
 * Interface for configuring a field.
 */
export interface FieldConfig<T> {
  name: string; // Supports nested paths
  label: string;
  type: FieldType;
  items?: SelectMenuItem[];
  comment?: string;
}

/**
 * Interface for the ObjectEditor component props.
 */
interface ObjectEditorInterface<ItemT> {
  fieldConfigs: FieldConfig<ItemT>[];
  header: string;
  description?: string;
  initial: ItemT;
  submitter: (changes: ItemT) => Promise<void>;
  deleter?: () => Promise<void>;
  secondary?: boolean; // if the editor is not the main editor on the page
  /** Optional callback that is called after a successful submission */
  onClose?: () => void;
}

/**
 * Helper function to get a nested value given a path.
 */
const getNestedValue = (obj: any, path: string): any =>
  path.split(".").reduce((acc, part) => acc && acc[part], obj);

/**
 * Helper function to set a nested value given a path.
 */
const setNestedValue = (obj: any, path: string, value: any): void => {
  const parts = path.split(".");
  const last = parts.pop()!;
  const target = parts.reduce((acc, part) => {
    if (!acc[part]) acc[part] = {};
    return acc[part];
  }, obj);
  target[last] = value;
};

/**
 * Data structure for a row in the multi-item editor.
 */
interface MultiItemRow {
  id: string;
  item: SelectMenuItem | null;
  value: string;
}

/**
 * Props for the MultiItemEditor component.
 *
 * This component renders a dynamic table where each row allows the user
 * to select an item (via a searchable dropdown) and enter an associated value.
 */
interface MultiItemEditorProps {
  items: SelectMenuItem[];
  initialRows: MultiItemRow[];
  onRowsChange: (rows: MultiItemRow[]) => void;
}

/**
 * MultiItemEditor renders a table of rows with an item selector, an input field,
 * and a remove button. It automatically adds an empty row at the bottom.
 */
const MultiItemEditor: React.FC<MultiItemEditorProps> = ({
  items,
  initialRows,
  onRowsChange,
}) => {
  const [rows, setRows] = useState<MultiItemRow[]>(initialRows);

  // Helper to generate a unique id for each row.
  const generateId = (): string =>
    Date.now().toString() + Math.random().toString(36).substr(2, 5);

  // Update parent whenever rows change.
  useEffect(() => {
    onRowsChange(rows);
  }, [rows, onRowsChange]);

  // Always ensure an empty row exists at the bottom.
  useEffect(() => {
    if (rows.length === 0 || rows[rows.length - 1].item !== null) {
      setRows((prev) => [
        ...prev,
        { id: generateId(), item: null, value: "" },
      ]);
    }
  }, [rows]);

  // Handler for when a row's selected item changes.
  const handleItemChange = (
    id: string,
    newItem: SelectMenuItem | null
  ): void => {
    setRows((prevRows) =>
      prevRows.map((row) => {
        if (row.id !== id) return row;
        const newRow = { ...row, item: newItem };
        // Use the provided item id if available.
        if (newItem && newItem.id !== undefined) {
          newRow.id = newItem.id.toString();
        }
        return newRow;
      })
    );
  };

  // Handler for when a row's value (amount/comment) changes.
  const handleValueChange = (id: string, newValue: string): void => {
    setRows((prevRows) =>
      prevRows.map((row) => (row.id === id ? { ...row, value: newValue } : row))
    );
  };

  // Remove a row by id.
  const handleRemoveRow = (id: string): void => {
    setRows((prevRows) => prevRows.filter((row) => row.id !== id));
  };

  // Check if an option is already selected in another row.
  const isOptionDisabled = (
    option: SelectMenuItem,
    currentRowId: string
  ): boolean =>
    rows.some(
      (row) =>
        row.item &&
        row.id !== currentRowId &&
        (
          option.id !== undefined
            ? option.id.toString() === (row.item.id ?? "").toString()
            : option.value.toString() === row.item.value.toString()
        )
    );

  return (
    <Stack spacing={2}>
      {rows.map((row) => (
        <Grid container spacing={1} alignItems="center" key={row.id}>
          {row.item ? (
            <>
              <Grid item xs={4}>
                <Autocomplete
                  options={items}
                  getOptionLabel={(option) => option.label}
                  value={row.item}
                  onChange={(_, newValue) =>
                    handleItemChange(row.id, newValue)
                  }
                  getOptionDisabled={(option) =>
                    isOptionDisabled(option, row.id)
                  }
                  renderInput={(params) => (
                    <TextField {...params} label="Item" variant="outlined" />
                  )}
                />
              </Grid>
              <Grid item xs={6}>
                <TextField
                  fullWidth
                  label="Value"
                  variant="outlined"
                  value={row.value}
                  onChange={(e) => handleValueChange(row.id, e.target.value)}
                />
              </Grid>
              <Grid item xs={2}>
                <IconButton
                  onClick={() => handleRemoveRow(row.id)}
                  aria-label="remove"
                >
                  <DeleteIcon />
                </IconButton>
              </Grid>
            </>
          ) : (
            <Grid item xs={12}>
              <Autocomplete
                options={items}
                getOptionLabel={(option) => option.label}
                value={row.item}
                onChange={(_, newValue) => handleItemChange(row.id, newValue)}
                getOptionDisabled={(option) =>
                  isOptionDisabled(option, row.id)
                }
                renderInput={(params) => (
                  <TextField
                    {...params}
                    label="Select an item"
                    variant="outlined"
                  />
                )}
              />
            </Grid>
          )}
        </Grid>
      ))}
    </Stack>
  );
};

/**
 * Props for the MultiItemDialog component.
 */
interface MultiItemDialogProps {
  open: boolean;
  onClose: () => void;
  onSave: (selectedItems: { [key: string]: string }) => void;
  items: SelectMenuItem[];
  /**
   * initialValue is a mapping of item id (if available, otherwise item value) to its associated comment/amount.
   */
  initialValue: { [key: string]: string };
  label: string;
}

/**
 * A dialog component that allows selecting multiple items via a dynamic table.
 * Each row lets the user choose an item (using a searchable dropdown), enter a value,
 * and remove the row if needed. It also loads pre‑existing data for editing.
 */
const MultiItemDialog: React.FC<MultiItemDialogProps> = ({
  open,
  onClose,
  onSave,
  items,
  initialValue,
  label,
}) => {
  // Ensure initialValue is always an object.
  const safeInitialValue = initialValue || {};

  // Compute initial rows using the item id (if available) for matching.
  const computeInitialRows = (): MultiItemRow[] => {
    const rowsFromValues = Object.entries(safeInitialValue).map(
      ([key, val]) => {
        const found = items.find((it) =>
          it.id !== undefined
            ? String(it.id) === key
            : String(it.value) === key
        );
        return {
          id: key,
          item: found || null,
          value: val,
        };
      }
    );
    return rowsFromValues.length > 0
      ? rowsFromValues
      : [{ id: Date.now().toString(), item: null, value: "" }];
  };

  const [rows, setRows] = useState<MultiItemRow[]>(computeInitialRows());

  // Reset rows when the dialog opens.
  useEffect(() => {
    if (open) {
      setRows(computeInitialRows());
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [open, safeInitialValue, items]);

  // When saving, convert the rows into a mapping of item id (if available) to the entered value.
  const handleSave = (): void => {
    const result: { [key: string]: string } = {};
    rows.forEach((row) => {
      if (row.item) {
        const key =
          row.item.id !== undefined
            ? String(row.item.id)
            : String(row.item.value);
        result[key] = row.value;
      }
    });
    console.log("rows", result);
    onSave(result);
  };

  return (
    <Dialog open={open} onClose={onClose} fullWidth maxWidth="md">
      <DialogTitle>Edit {label}</DialogTitle>
      <DialogContent>
        <MultiItemEditor
          items={items}
          initialRows={rows}
          onRowsChange={setRows}
        />
      </DialogContent>
      <DialogActions>
        <Button onClick={onClose}>Cancel</Button>
        <Button onClick={handleSave} variant="contained">
          Save
        </Button>
      </DialogActions>
    </Dialog>
  );
};

/**
 * The main ObjectEditor component.
 */
export default function ObjectEditor<ItemT>({
  fieldConfigs,
  header,
  description,
  initial,
  submitter,
  deleter,
  secondary,
  onClose,
}: ObjectEditorInterface<ItemT>) {
  const [item, setItem] = useState<ItemT>(initial);
  const [changedFields, setChangedFields] = useState<Set<string>>(new Set());
  const [fieldErrors, setFieldErrors] = useState<Map<string, string>>(
    new Map()
  );
  const [deleteDialogOpen, setDeleteDialogOpen] = useState(false);
  // Maintain open state for each multi-item dialog by field name.
  const [multiItemDialogOpen, setMultiItemDialogOpen] = useState<{
    [key: string]: boolean;
  }>({});

  const { showSnackbar } = useSnackbar();

  const handleChange = (name: string, value: any) => {
    const updatedItem = { ...item };
    setNestedValue(updatedItem, name, value);
    setItem(updatedItem);
    setChangedFields((prev) => new Set(prev).add(name));
    setFieldErrors((prev) => {
      const newErrors = new Map(prev);
      newErrors.delete(name);
      return newErrors;
    });
  };

  const handleFileChange = (name: string, file: File | null) => {
    handleChange(name, file);
  };

  const handleSubmit = async () => {
    setFieldErrors(new Map());
    let parsingError = false;
    const updatedItem = { ...initial };

    fieldConfigs.forEach((fc) => {
      if (changedFields.has(fc.name)) {
        const rawValue = getNestedValue(item, fc.name);
        if (fc.type === FieldType.NUMERIC || fc.type === FieldType.CURRENCY) {
          const numericValue = Number(rawValue);
          if (isNaN(numericValue)) {
            parsingError = true;
            setFieldErrors((prev) =>
              new Map(prev).set(fc.name, "Please enter a number")
            );
          } else {
            setNestedValue(updatedItem, fc.name, numericValue);
          }
        } else if (fc.type === FieldType.BOOLEAN) {
          setNestedValue(updatedItem, fc.name, rawValue === "true");
        } else if (fc.type === FieldType.FILE) {
          setNestedValue(updatedItem, fc.name, rawValue);
        } else {
          setNestedValue(updatedItem, fc.name, rawValue);
        }
      }
    });

    if (!parsingError) {
      try {
        await submitter(updatedItem);
        showSnackbar("Update successful", "success");
        setChangedFields(new Set());
        setFieldErrors(new Map());
        if (onClose) {
          onClose();
        }
      } catch (err: any) {
        if (err.detail) {
          const newFieldErrors = new Map<string, string>();
          console.log(err.detail);
          if (typeof err.detail === "string") {
            showSnackbar(err.detail, "error");
          } else {
            err.detail.forEach((error: any) => {
              const fieldPath = error.loc.slice(1).join(".");
              const errorMessage = error.msg;
              newFieldErrors.set(fieldPath, errorMessage);
            });
            setFieldErrors(newFieldErrors);
            showSnackbar(
              "There are errors in the form, see highlighted fields.",
              "error"
            );
          }
        } else {
          showSnackbar(
            err.message || "An unexpected error occurred.",
            "error"
          );
        }
      }
    } else {
      showSnackbar(
        "There are errors in the form, please fix them and try again!",
        "error"
      );
    }
  };

  return (
    <Box>
      <Paper variant="outlined" sx={{ margin: 2, padding: 2 }}>
        <Stack direction="row" justifyContent="space-between" spacing={2}>
          <Typography variant="h4" component="h2">
            {header}
          </Typography>
          <ButtonGroup variant="contained">
            {deleter && (
              <Button onClick={() => setDeleteDialogOpen(true)}>Delete</Button>
            )}
          </ButtonGroup>
        </Stack>
        {description && (
          <Typography variant="subtitle1">{description}</Typography>
        )}
        <Stack spacing={1}>
          {fieldConfigs.map((fconf) => {
            const value = getNestedValue(item, fconf.name);
            const error = fieldErrors.get(fconf.name);

            if (fconf.items && fconf.type === FieldType.MULTI_ITEM) {
              // Ensure we always work with an object.
              const multiItemValue = value || {};
              return (
                <Box key={fconf.name} sx={{ marginY: 2 }}>
                  <Button
                    variant="outlined"
                    onClick={() =>
                      setMultiItemDialogOpen({
                        ...multiItemDialogOpen,
                        [fconf.name]: true,
                      })
                    }
                  >
                    {fconf.label}
                  </Button>
                  <MultiItemDialog
                    open={!!multiItemDialogOpen[fconf.name]}
                    onClose={() =>
                      setMultiItemDialogOpen({
                        ...multiItemDialogOpen,
                        [fconf.name]: false,
                      })
                    }
                    onSave={(newValue) => {
                      handleChange(fconf.name, newValue);
                      setMultiItemDialogOpen({
                        ...multiItemDialogOpen,
                        [fconf.name]: false,
                      });
                    }}
                    items={fconf.items || []}
                    initialValue={multiItemValue}
                    label={fconf.label}
                  />
                  <Box sx={{ marginTop: 1 }}>
                    {Object.keys(multiItemValue).length > 0 ? (
                      <Stack direction="row" spacing={1} flexWrap="wrap">
                        {Object.entries(multiItemValue).map(([key, val]) => {
                          const foundItem = (fconf.items || []).find((it) =>
                            it.id !== undefined
                              ? String(it.id) === key
                              : String(it.value) === key
                          );
                          const itemLabel = foundItem ? foundItem.label : key;
                          return (
                            <Chip
                              key={key}
                              label={`${itemLabel}: ${val}`}
                            />
                          );
                        })}
                      </Stack>
                    ) : (
                      <Typography variant="caption" color="textSecondary">
                        No items selected
                      </Typography>
                    )}
                  </Box>
                  {error && (
                    <Typography variant="caption" color="error">
                      {error}
                    </Typography>
                  )}
                </Box>
              );
            } else if (fconf.items) {
              return (
                <FormControl key={fconf.name} error={!!error}>
                  <InputLabel id={`${fconf.name}-select-label`}>
                    {fconf.label}
                  </InputLabel>
                  <Select
                    labelId={`${fconf.name}-select-label`}
                    name={fconf.name}
                    value={value || ""}
                    label={fconf.label}
                    onChange={(e) =>
                      handleChange(fconf.name, e.target.value)
                    }
                  >
                    {fconf.items.map((it) => (
                      <MenuItem key={it.value} value={it.value}>
                        {it.label}
                      </MenuItem>
                    ))}
                  </Select>
                </FormControl>
              );
            } else if (
              fconf.type === FieldType.STRING ||
              fconf.type === FieldType.NUMERIC ||
              fconf.type === FieldType.CURRENCY
            ) {
              return (
                <TextField
                  key={fconf.name}
                  name={fconf.name}
                  label={fconf.label}
                  value={value || ""}
                  onChange={(e) =>
                    handleChange(fconf.name, e.target.value)
                  }
                  error={!!error}
                  helperText={error}
                  fullWidth
                />
              );
            } else if (fconf.type === FieldType.BOOLEAN) {
              return (
                <Box key={fconf.name}>
                  <FormControl>
                    <FormControlLabel
                      control={
                        <input
                          type="checkbox"
                          name={fconf.name}
                          checked={value === true || value === "true"}
                          onChange={(e) =>
                            handleChange(
                              fconf.name,
                              e.target.checked.toString()
                            )
                          }
                        />
                      }
                      label={fconf.label}
                    />
                  </FormControl>
                </Box>
              );
            } else if (fconf.type === FieldType.DATETIME) {
              return (
                <DateTimePicker
                  key={fconf.name}
                  label={fconf.label}
                  value={value ? dayjs(value) : null}
                  onChange={(newValue) =>
                    handleChange(fconf.name, newValue?.toISOString())
                  }
                  renderInput={(params) => (
                    <TextField {...params} error={!!error} helperText={error} />
                  )}
                />
              );
            } else if (fconf.type === FieldType.COMMENT) {
              return (
                <Typography variant="subtitle1" key={fconf.name}>
                  {fconf.comment}
                </Typography>
              );
            } else if (fconf.type === FieldType.FILE) {
              return (
                <FormControl key={fconf.name} error={!!error} fullWidth>
                  <Typography variant="body2" sx={{ marginTop: 1 }}>
                    Receipt: 
                  </Typography>
                  <Stack direction="row" spacing={2} justifyContent="flex-start">
                    <Button
                      component="label"
                      variant="contained"
                      startIcon={<UploadFile />}
                    >
                      File
                      <input
                        accept="application/pdf,image/jpeg,image/png"
                        style={{ display: "none" }}
                        id={`file-input-${fconf.name}-pdf`}
                        type="file"
                        onChange={(e) =>
                          handleFileChange(
                            fconf.name,
                            e.target.files ? e.target.files[0] : null,
                          )
                        }
                        />
                    </Button>
                    <Button
                      component="label"
                      variant="contained"
                      startIcon={<PhotoCamera />}
                    >
                      Capture
                      <input
                        accept="image/jpeg,image/png"
                        capture="environment"
                        style={{ display: "none" }}
                        id={`file-input-${fconf.name}-photo`}
                        type="file"
                        onChange={(e) =>
                          handleFileChange(
                            fconf.name,
                            e.target.files ? e.target.files[0] : null,
                          )
                        }
                        />
                    </Button>
                  </Stack>
                  {value && typeof value !== "string" && (
                    <Typography variant="body2" sx={{ marginTop: 1 }}>
                      Selected File: {value.name}
                    </Typography>
                  )}
                  {error && (
                    <Typography variant="caption" color="error">
                      {error}
                    </Typography>
                  )}
                </FormControl>
              );
            } else {
              return (
                <Typography key={fconf.name}>
                  Unsupported field type
                </Typography>
              );
            }
          })}
        </Stack>
      </Paper>
      <Box sx={{ display: "flex", justifyContent: "center", margin: 2 }}>
        <Button
          variant="contained"
          onClick={handleSubmit}
          disabled={changedFields.size === 0}
          sx={{
            padding: 2,
            fontSize: "1.2rem",
            width: "100%",
            maxWidth: "400px",
          }}
        >
          Submit
        </Button>
      </Box>
      {deleter && (
        <Dialog
          open={deleteDialogOpen}
          onClose={() => setDeleteDialogOpen(false)}
        >
          <DialogTitle>Confirm Deletion</DialogTitle>
          <DialogContent>
            <DialogContentText>
              Are you sure you want to delete this item? This action cannot be
              undone.
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={() => setDeleteDialogOpen(false)}>
              Cancel
            </Button>
            <Button
              onClick={async () => {
                try {
                  await deleter();
                  setDeleteDialogOpen(false);
                  if (onClose) {
                    onClose();
                  }
                } catch (err: any) {
                  showSnackbar(
                    err.message || "An unexpected error occurred.",
                    "error"
                  );
                }
              }}
              color="error"
            >
              Delete
            </Button>
          </DialogActions>
        </Dialog>
      )}
    </Box>
  );
}
