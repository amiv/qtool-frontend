// FilePreview.tsx
import React, { useEffect, useState } from "react";
import {
  Typography,
  Box,
  CircularProgress,
  Modal,
  Button,
  IconButton,
} from "@mui/material";
import CloseIcon from "@mui/icons-material/Close";

interface FilePreviewProps {
  fileUrl: string;
}

const contentTypeCache: { [url: string]: string } = {};
const blobUrlCache: { [url: string]: string } = {};

const FilePreview: React.FC<FilePreviewProps> = ({ fileUrl }) => {
  const [contentType, setContentType] = useState<string | null>(null);
  const [error, setError] = useState<string | null>(null);
  const [loading, setLoading] = useState<boolean>(true);
  const [blobUrl, setBlobUrl] = useState<string | null>(null);
  const [isModalOpen, setIsModalOpen] = useState<boolean>(false);

  useEffect(() => {
    let isMounted = true;
    const controller = new AbortController();
    const signal = controller.signal;

    const fetchFile = async () => {
      if (contentTypeCache[fileUrl]) {
        if (isMounted) {
          setContentType(contentTypeCache[fileUrl]);
          setBlobUrl(blobUrlCache[fileUrl]);
          setLoading(false);
        }
        return;
      }

      try {
        const response = await fetch(fileUrl, {
          method: "GET",
          signal,
        });

        if (!response.ok) {
          throw new Error(`HTTP error! status: ${response.status}`);
        }

        const type = response.headers.get("Content-Type");
        if (type) {
          contentTypeCache[fileUrl] = type;
          if (isMounted) {
            setContentType(type);
          }
        } else {
          throw new Error("Content-Type not found");
        }

        const blob = await response.blob();
        const url = URL.createObjectURL(blob);
        blobUrlCache[fileUrl] = url;
        if (isMounted) {
          setBlobUrl(url);
        }
      } catch (err: any) {
        if (isMounted) {
          if (err.name === "AbortError") {
            setError("Request timed out.");
          } else {
            console.error("Error fetching file:", err);
            setError("Unable to load file.");
          }
        }
      } finally {
        if (isMounted) {
          setLoading(false);
        }
      }
    };

    const timeoutId = setTimeout(() => {
      controller.abort();
    }, 10000);

    fetchFile();

    return () => {
      isMounted = false;
      clearTimeout(timeoutId);
      controller.abort();
      if (blobUrlCache[fileUrl]) {
        URL.revokeObjectURL(blobUrlCache[fileUrl]);
        delete blobUrlCache[fileUrl];
      }
    };
  }, [fileUrl]);

  const handleModalClose = () => {
    setIsModalOpen(false);
  };

  const renderModalContent = () => {
    if (contentType?.startsWith("image/")) {
      return (
        <img
          src={blobUrl!}
          alt="Preview"
          style={{ maxWidth: "100%", maxHeight: "80vh" }}
        />
      );
    }

    if (contentType === "application/pdf") {
      return (
        <iframe
          src={blobUrl!}
          title="PDF Preview"
          style={{
            width: "90vw",
            height: "90vh",
            border: "none",
          }}
        />
      );
    }

    return null;
  };

  if (loading) {
    return <CircularProgress size={20} />;
  }

  if (error) {
    return (
      <Typography variant="body2" color="error">
        {error}{" "}
        <a href={fileUrl} target="_blank" rel="noopener noreferrer">
          Download
        </a>
      </Typography>
    );
  }

  if (!contentType || !blobUrl) {
    return (
      <Typography variant="body2">
        <a href={fileUrl} target="_blank" rel="noopener noreferrer">
          Download File
        </a>
      </Typography>
    );
  }

  return (
    <Box flex={1}>
      {contentType.startsWith("image/") ? (
        <Box
          component="img"
          src={blobUrl}
          alt="Image Preview"
          sx={{
            maxWidth: 600,
            maxHeight: 600,
            objectFit: "contain",
            cursor: "pointer",
          }}
          onClick={() => setIsModalOpen(true)}
        />
      ) : contentType === "application/pdf" ? (
        <Box
          component="iframe"
          src={blobUrl}
          title="PDF Preview"
          sx={{ width: "100%", height: "100%", border: "none", cursor: "pointer" }}
          onClick={() => setIsModalOpen(true)}
        />
      ) : (
        <Typography variant="body2">
          <a href={fileUrl} target="_blank" rel="noopener noreferrer">
            Download File
          </a>
        </Typography>
      )}

      <Modal
        open={isModalOpen}
        onClose={handleModalClose}
        sx={{
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <Box
          sx={{
            position: "relative",
            bgcolor: "background.paper",
            p: 2,
            boxShadow: 24,
            borderRadius: 1,
            maxWidth: "90%",
            maxHeight: "90%",
            outline: "none",
          }}
        >
          <IconButton
            onClick={handleModalClose}
            sx={{ position: "absolute", top: 8, right: 8 }}
          >
            <CloseIcon />
          </IconButton>
          {renderModalContent()}
          <Box mt={2} textAlign="center">
            <Button
              variant="contained"
              color="primary"
              href={fileUrl}
              target="_blank"
              rel="noopener noreferrer"
              download
            >
              Download
            </Button>
          </Box>
        </Box>
      </Modal>
    </Box>
  );
};

export default FilePreview;
