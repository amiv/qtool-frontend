import React, { useState } from "react";
import { TextField, Button } from "@mui/material";
import { q_state } from "../client/types.gen";
import receiptHandler from "./ReceiptHandler";
import {
  billsUpdateBill,
  creditPaymentsUpdateCreditPayment,
  internalTransfersUpdateInternalTransfer,
  reimbursementsUpdateReimbursement,
} from "../client/services.gen";
import { useSnackbar } from "../pages/SnackbarProvider";

interface PreviewHeaderProps {
  type: string;
  id: string;
  data: any;
}

async function handleCheck(
  type: string,
  id: string,
  data: any,
  state: q_state,
  showSnackbar: (message: string, severity: "success" | "error") => void,
) {
  try {
    const item = data;
    item.creditor.q_check = state; // Toggle q_check

    if (type === "Bill") {
      item.receipt = await receiptHandler(item.receipt);
      const resp = await billsUpdateBill({ body: item, path: { id } });
      if (resp.error) {
        throw new Error("Failed to update bill: " + resp.error.detail);
      }
      showSnackbar("Bill approval status updated", "success");
    } else if (type === "CreditPayment") {
      item.receipt = await receiptHandler(item.receipt);
      const resp = await creditPaymentsUpdateCreditPayment({
        body: item,
        path: { id },
      });
      if (resp.error) {
        throw new Error(
          "Failed to update credit payment: " + resp.error.detail,
        );
      }
      showSnackbar("Credit Payment approval status updated", "success");
    } else if (type === "Reimbursement") {
      item.receipt = await receiptHandler(item.receipt);
      const resp = await reimbursementsUpdateReimbursement({
        body: item,
        path: { id },
      });
      if (resp.error) {
        throw new Error("Failed to update reimbursement: " + resp.error.detail);
      }
      showSnackbar("Reimbursement approval status updated", "success");
    } else if (type === "InternalTransfer") {
      const resp = await internalTransfersUpdateInternalTransfer({
        body: item,
        path: { id },
      });
      if (resp.error) {
        throw new Error(
          "Failed to update internal transfer: " + resp.error.detail,
        );
      }
      showSnackbar("Internal Transfer approval status updated", "success");
    } else {
      throw new Error("Unknown type provided");
    }

    return true;
  } catch (error: any) {
    console.error("Failed to toggle check:", error);
    showSnackbar(error.message || "Failed to toggle check", "error");
    return false;
  }
}

export const PreviewHeader: React.FC<PreviewHeaderProps> = ({
  type,
  id,
  data,
}) => {
  const [qcomment, setQcomment] = useState(data.creditor.qcomment || "");
  const { showSnackbar } = useSnackbar(); // Use Snackbar hook

  const qCommentField = (
    e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>,
  ) => {
    setQcomment(e.target.value);
  };

  const handleAccept = async () => {
    try {
      data.creditor.qcomment = qcomment;
      setQcomment("");
      await handleCheck(type, id, data, q_state.ACCEPTED, showSnackbar);
    } catch (error) {
      console.error("Error handling accept:", error);
      showSnackbar("Failed to accept: " + error.message, "error");
    }
  };

  const handleReject = async () => {
    try {
      data.creditor.qcomment = qcomment;
      setQcomment("");
      await handleCheck(type, id, data, q_state.REJECTED, showSnackbar);
    } catch (error) {
      console.error("Error handling reject:", error);
      showSnackbar("Failed to reject: " + error.message, "error");
    }
  };

  return (
    <div>
      <TextField
        name="q_comment"
        label="q_comment"
        value={qcomment}
        onChange={qCommentField}
        fullWidth
      />
      <Button variant="contained" color="success" onClick={handleAccept}>
        {data.creditor.q_check === q_state.OPEN ? "Accept" : "Uncheck"}
      </Button>
      <Button variant="contained" color="error" onClick={handleReject}>
        {data.creditor.q_check === q_state.OPEN ? "Reject" : "Uncheck"}
      </Button>
    </div>
  );
};
