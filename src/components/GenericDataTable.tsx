import React, { useEffect, useState } from "react";
import MUIDataTable, { MUIDataTableOptions } from "mui-datatables";
import { CircularProgress } from "@mui/material";

export interface GenericDataTableProps {
  title: string;
  columns: any[];
  /**
   * The fetchData function should return an object with the data array and the total count.
   */
  fetchData: (params: FetchParams) => Promise<{ data: any[]; total: number }>;
  onRowClick?: (rowData: string[], rowMeta: { dataIndex: number }) => void;
  refresh?: boolean;
}

export interface FetchParams {
  search: string;
  sort: { column: string; direction: "asc" | "desc" } | null;
  filters: Record<string, any>;
  page: number;
  limit: number;
}

const GenericDataTable: React.FC<GenericDataTableProps> = ({
  title,
  columns,
  fetchData,
  onRowClick = () => {},
  refresh = false,
}) => {
  // Data and loading states.
  const [data, setData] = useState<any[]>([]);
  const [isInitialLoading, setIsInitialLoading] = useState(true);
  const [isLoading, setIsLoading] = useState(false);

  // States for search, filters, sort.
  const [filters, setFilters] = useState<Record<string, any>>({});
  const [search, setSearch] = useState<string>("");
  const [sort, setSort] = useState<{ column: string; direction: "asc" | "desc" } | null>(null);

  // Pagination states.
  const [page, setPage] = useState<number>(0);
  const [limit, setLimit] = useState<number>(50);
  const [count, setCount] = useState<number>(0);

  /**
   * Loads the data based on the current state.
   */
  const loadData = async () => {
    // Show full-screen loading if we have no data yet.
    if (data.length > 0) {
      setIsLoading(true);
    } else {
      setIsInitialLoading(true);
    }

    const params: FetchParams = {
      search,
      sort,
      filters,
      page,
      limit,
    };

    try {
      const result = await fetchData(params);
      setData(result.data);
      setCount(result.total);
    } catch (error) {
      console.error("Error fetching data:", error);
    } finally {
      setIsInitialLoading(false);
      setIsLoading(false);
    }
  };

  // Reset to page 0 when search, sort, or filters change.
  useEffect(() => {
    setPage(0);
  }, [search, sort, filters]);

  // Re-fetch data whenever any of these change.
  useEffect(() => {
    loadData();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [filters, search, sort, page, refresh, limit]);

  const options: MUIDataTableOptions = {
    serverSide: true,
    search: true,
    searchText: search,
    download: true,
    print: true,
    viewColumns: true,
    filter: true,
    pagination: true,
    rowsPerPage: limit,
    count: count,
    rowsPerPageOptions: [10,50,100],
    page: page,
    draggableColumns: { enabled: true },
    filterType: "textField",
    responsive: "vertical",
    selectableRows: "none",
    onRowClick: (rowData: string[], rowMeta: { dataIndex: number }) => {
      onRowClick(rowData, rowMeta);
    },
    onSearchChange: (newSearch: string | null) => {
      setSearch(newSearch || "");
    },
    onColumnSortChange: (changedColumn: string, direction: "asc" | "desc") => {
      setSort({ column: changedColumn, direction });
    },
    onFilterChange: (column: string, filterList: any) => {
      const newFilters: Record<string, any> = {};
      columns.forEach((col: any, index: number) => {
        if (filterList[index]?.length > 0) {
          newFilters[col.name as string] = filterList[index];
        }
      });
      setFilters(newFilters);
    },
    onTableChange: (action: string, tableState: any) => {
      console.log(action, tableState);
      switch (action) {
        case "changePage":
          setPage(tableState.page);
          break;
        case "sort":
          setSort({
            column: tableState.sortOrder.name,
            direction: tableState.sortOrder.direction,
          });
          break;
        case "changeRowsPerPage":
          setLimit(tableState.rowsPerPage);
          break;
        default:
          break;
      }
    },
  };

  return (
    <div style={{ position: "relative" }}>
      {isInitialLoading && data.length === 0 ? (
        <div style={{ position: "absolute", top: "50%", left: "50%" }}>
          <CircularProgress />
        </div>
      ) : (
        <>
          {/* Overlay loading spinner */}
          {isLoading && (
            <div
              style={{
                position: "absolute",
                top: 0,
                left: 0,
                width: "100%",
                height: "100%",
                background: "rgba(0, 0, 0, 0.3)",
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
                zIndex: 10,
              }}
            >
              <CircularProgress />
            </div>
          )}
          <MUIDataTable
            title={title}
            data={data}
            columns={columns}
            options={options}
          />
        </>
      )}
    </div>
  );
};

export default GenericDataTable;
