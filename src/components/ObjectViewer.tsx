import React, { useEffect, useState } from "react";
import {
  Typography,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableRow,
  Paper,
  IconButton,
  Box,
  TableHead,
  Button,
  Stack,
} from "@mui/material";
import { ExpandLess, ExpandMore, Height } from "@mui/icons-material";
import FilePreview from "./FilePreview";

export enum FieldType {
  STRING,
  NUMERIC,
  CURRENCY,
  BOOLEAN,
  DATETIME,
  COMMENT,
  FILE,
}

export interface SelectMenuItem {
  label: string;
  value: string | number;
}

export interface FieldConfig<T> {
  name: string;
  label: string;
  type: FieldType;
  items?: SelectMenuItem[];
  comment?: string;
}

const getNestedValue = (obj: any, path: string): any =>
  path.split(".").reduce((acc, part) => (acc ? acc[part] : undefined), obj);

interface ObjectViewerProps {
  data: Record<string, any>;
  fieldConfigs: FieldConfig<any>[];
  nestingVisibility?: Record<string, boolean>;
  depth?: number;
  isRoot?: boolean;
  previewHeader?: React.ReactNode; // Prop for custom header above the preview
}

const ObjectViewer: React.FC<ObjectViewerProps> = ({
  data,
  fieldConfigs,
  nestingVisibility = {},
  depth = 0,
  isRoot = true,
  previewHeader,
}) => {
  const [expandedKeys, setExpandedKeys] = useState<Record<string, boolean>>({});

  if (!data || typeof data !== "object") {
    return (
      <Typography variant="body2" color="error">
        Invalid data (not an object)
      </Typography>
    );
  }

  const handleToggle = (key: string) => {
    setExpandedKeys((prev) => ({
      ...prev,
      [key]: !prev[key],
    }));
  };

  const renderValue = (fieldConfig: FieldConfig<any>, rawValue: any) => {
    const { type, items } = fieldConfig;

    if (items && items.length) {
      const matched = items.find((it: SelectMenuItem) => it.value === rawValue);
      return (
        <Typography variant="body2" component="span">
          {matched?.label || String(rawValue) || "—"}
        </Typography>
      );
    }

    if (type === FieldType.FILE) {
      if (typeof rawValue !== "string") {
        return (
          <Typography variant="body2" color="error">
            Invalid file URL
          </Typography>
        );
      }
      return (
        <Button
          variant="contained"
          color="primary"
          href={rawValue}
          target="_blank"
          rel="noopener noreferrer"
          download
        >
          Download
        </Button>
      );
    }

    switch (type) {
      case FieldType.STRING:
      case FieldType.NUMERIC:
      case FieldType.DATETIME:
        return (
          <Typography variant="body2">
            {rawValue !== undefined ? String(rawValue) : "—"}
          </Typography>
        );
      case FieldType.CURRENCY:
        return (
          <Typography variant="body2">
            {rawValue !== undefined ? String(Number((rawValue / 100).toFixed(2))) : "—"}
          </Typography>
        );
      case FieldType.BOOLEAN:
        return (
          <Typography variant="body2">{rawValue ? "True" : "False"}</Typography>
        );
      case FieldType.COMMENT:
        return (
          <Typography variant="body2" color="text.secondary">
            {String(rawValue)}
          </Typography>
        );
      default:
        return (
          <Typography variant="body2" color="text.secondary">
            {String(rawValue)}
          </Typography>
        );
    }
  };

  const findFilePreview = (): string | null => {
    for (const fc of fieldConfigs) {
      if (fc.type === FieldType.FILE) {
        const value = getNestedValue(data, fc.name);
        if (typeof value === "string" && value.trim() !== "") {
          return value;
        }
      }
    }
    return null;
  };

  const filePreviewUrl = isRoot ? findFilePreview() : null;

  const renderRow = (fieldConfig: FieldConfig<any>) => {
    const { name, label } = fieldConfig;
    const value = getNestedValue(data, name);

    const isNestedObject =
      typeof value === "object" &&
      value !== null &&
      !Array.isArray(value) &&
      fieldConfig.type !== FieldType.FILE;

    return (
      <React.Fragment key={name}>
        <TableRow>
          <TableCell>
            {isNestedObject && nestingVisibility[name] !== false && (
              <IconButton
                size="small"
                onClick={() => handleToggle(name)}
                aria-label={`Toggle ${label}`}
              >
                {expandedKeys[name] ? <ExpandLess /> : <ExpandMore />}
              </IconButton>
            )}
          </TableCell>
          <TableCell>
            <Typography variant="body2" sx={{ paddingLeft: depth * 2 }}>
              {label}
            </Typography>
          </TableCell>
          <TableCell>
            {isNestedObject
              ? expandedKeys[name]
                ? "—"
                : "Object"
              : renderValue(fieldConfig, value)}
          </TableCell>
        </TableRow>
        {isNestedObject &&
          expandedKeys[name] &&
          nestingVisibility[name] !== false && (
            <TableRow>
              <TableCell colSpan={3} sx={{ pl: 4 }}>
                <ObjectViewer
                  data={value}
                  fieldConfigs={fieldConfigs}
                  nestingVisibility={nestingVisibility}
                  depth={depth + 1}
                  isRoot={false}
                />
              </TableCell>
            </TableRow>
          )}
      </React.Fragment>
    );
  };

  const renderTable = () => (
    <TableContainer component={Paper} sx={{ mt: 2, flex: 1, minWidth: 400 }}>
      <Table size="small">
        {isRoot && (
          <TableHead>
            <TableRow>
              <TableCell />
              <TableCell>
                <strong>Field</strong>
              </TableCell>
              <TableCell>
                <strong>Value</strong>
              </TableCell>
            </TableRow>
          </TableHead>
        )}
        <TableBody>{fieldConfigs.map((fc) => renderRow(fc))}</TableBody>
      </Table>
    </TableContainer>
  );

  const renderPreview = () => (
    <Box
      sx={{
        flex: 3,
        ml: 2,
        mt: isRoot ? 2 : 0,
      }}
    >
      <Stack height={'100%'}>
        {previewHeader && <Box mb={2}>{previewHeader}</Box>}
        {filePreviewUrl && <FilePreview fileUrl={filePreviewUrl} />}
      </Stack>
    </Box>
  );

  if (isRoot) {
    return (
      <Stack direction={{ xs: 'column', md: 'row'}} height={'80vh'}>
        {renderTable()}
        {(previewHeader || filePreviewUrl) && renderPreview()}
      </Stack>
    );
  } else {
    return renderTable();
  }
};

export default ObjectViewer;
