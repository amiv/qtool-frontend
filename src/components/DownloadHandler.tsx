import React from "react";
import Button from "@material-ui/core/Button";

export async function handleDownload(url) {
  try {
    // Fetch the file data as a Blob
    const response = await fetch(url);
    if (!response.ok) {
      throw new Error(`HTTP error! status: ${response.status}`);
    }
    const blob = await response.blob();

    // Create a temporary URL for the Blob
    const blobUrl = window.URL.createObjectURL(blob);

    // Create a temporary anchor element to trigger the download
    const link = document.createElement("a");
    link.href = blobUrl;

    // Optionally, extract filename from URL or headers. Here we use a default name.
    const defaultFilename = "Ezag.xml";
    link.download = defaultFilename;

    // Append the link to the body (required for Firefox)
    document.body.appendChild(link);

    // Programmatically click the link to trigger download
    link.click();

    // Cleanup: remove link and revoke the object URL
    document.body.removeChild(link);
    window.URL.revokeObjectURL(blobUrl);
  } catch (error) {
    console.error("Download failed:", error);
  }
}
