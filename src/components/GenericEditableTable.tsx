import React, { useState } from "react";
import { AppBar, Button, Dialog, DialogActions, DialogContent, DialogTitle, IconButton, Toolbar, Typography } from "@mui/material";
import GenericDataTable from "../components/GenericDataTable";
import ObjectViewer from "../components/ObjectViewer";
import EditReimbursement from "../pages/EditReimbursement";
import EditBills from "../pages/EditBills";
import EditCreditPayment from "../pages/EditCreditPayment";
import EditInternalTransfer from "../pages/EditInternalTransfer";
import { generateFieldConfigs as ReimbursementFieldConfig } from "../pages/Reimbursement";
import { generateFieldConfigs as BillFieldConfig } from "../pages/Bills";
import { generateFieldConfigs as CreditPaymentFieldConfig } from "../pages/CreditPayment";
import { generateFieldConfigs as InternalTransferFieldConfig } from "../pages/InternalTransfer";
import {
  billsReadBill,
  creditPaymentsReadCreditPayment,
  internalTransfersReadInternalTransfer,
  reimbursementsReadReimbursement,
  usersReadUser,
} from "../client/services.gen";
import { Kst, Ledger } from "../client/types.gen";
import { Close } from "@mui/icons-material";

const withWrapper =
  (EditFunction: (id: string, onClose: () => void) => JSX.Element) =>
  ({
    propIdString,
    onClose,
  }: {
    propIdString: string;
    onClose: () => void;
  }) => {
    const content = EditFunction({propIdString: propIdString, onClose: onClose});
    return (
      <div>
        {content}
        <Button onClick={onClose}>Close</Button>
      </div>
    );
  };

const EditReimbursementWrapper = withWrapper(EditReimbursement);
const EditBillsWrapper = withWrapper(EditBills);
const EditCreditPaymentWrapper = withWrapper(EditCreditPayment);
const EditInternalTransferWrapper = withWrapper(EditInternalTransfer);

function getEditComponent(
  type: string,
  id: string,
  handleClose: () => void,
): React.ReactNode {
  switch (type) {
    case "Reimbursement":
      return (
        <EditReimbursementWrapper propIdString={id} onClose={handleClose} />
      );
    case "Bill":
      return <EditBillsWrapper propIdString={id} onClose={handleClose} />;
    case "CreditPayment":
      return (
        <EditCreditPaymentWrapper propIdString={id} onClose={handleClose} />
      );
    case "InternalTransfer":
      return (
        <EditInternalTransferWrapper propIdString={id} onClose={handleClose} />
      );
    default:
      return <div>Unknown Type</div>;
  }
}

export async function fetchDataForView(type: string, id: string) {
  switch (type) {
    case "Reimbursement":
      return (await reimbursementsReadReimbursement({ path: { id } })).data;
    case "Bill":
      return (await billsReadBill({ path: { id } })).data;
    case "CreditPayment":
      return (await creditPaymentsReadCreditPayment({ path: { id } })).data;
    case "InternalTransfer":
      return (
        await internalTransfersReadInternalTransfer({ path: { id } })
      ).data;
    default:
      throw new Error("Unknown Type");
  }
}

function getFieldConfig(
  type: string,
  kst: Kst[],
  ledger: Ledger[],
  full: boolean = true,
): any[] {
  switch (type) {
    case "Reimbursement":
      return ReimbursementFieldConfig(kst, ledger, full);
    case "Bill":
      return BillFieldConfig(kst, ledger, full);
    case "CreditPayment":
      return CreditPaymentFieldConfig(kst, ledger, full);
    case "InternalTransfer":
      return InternalTransferFieldConfig(kst, ledger, full);
    default:
      return [];
  }
}

interface EditableTableProps {
  title: string;
  /**
   * The fetchFunction should accept search, sort, filters, page and limit.
   * It must return a Promise resolving to an object containing:
   *   - data: the current page's data
   *   - total: the total record count
   */
  fetchFunction: (params: {
    search: string;
    sort: { column: string; direction: "asc" | "desc" } | null;
    filters: Record<string, any>;
    page: number;
    limit: number;
  }) => Promise<{ data: any[]; total: number }>;
  kst: any[];
  ledger: any[];
  additionalColumns?: any[];
  previewHeader?: (type: string, id: string, data: any) => React.ReactNode;
  editable?: boolean;
}

const GenericEditableTable: React.FC<EditableTableProps> = ({
  title,
  fetchFunction,
  kst,
  ledger,
  additionalColumns = [],
  previewHeader = (type, id, data) => "",
  editable = true,
}) => {
  const [open, setOpen] = useState(false);
  const [content, setContent] = useState<React.ReactNode | null>(null);

  const handleClose = () => {
    setOpen(false);
    setContent(null);
  };

  const handleViewAction = async (type: string, id: string) => {
    setOpen(true);
    try {
      const data = await fetchDataForView(type, id);
      const modifiedData = data || [];
      if (data && data?.creditor && data?.creditor.creator_id ){
        const user = await usersReadUser({
          path: { id : data.creditor.creator_id}
        })
        //modifiedData.creditor.creator_id = user.data?.nethz || "not found"
      }

      const fieldConfig = getFieldConfig(type, kst, ledger, true);
      setContent(
        <ObjectViewer
          data={data}
          fieldConfigs={fieldConfig}
          previewHeader={
            <div
              style={{
                minWidth: "150px",
                display: "flex",
                flexDirection: "column",
                gap: "1rem",
              }}
            >
              <Button
                variant="contained"
                color="primary"
                onClick={() => handleEditAction(type, id)}
              >
                Edit
              </Button>
              {previewHeader(type, id, data)}
            </div>
          }
        />
      );
    } catch (error) {
      console.error(`Error fetching ${type} data:`, error);
      setContent(
        <div>
          <p>Error loading data.</p>
          <Button onClick={handleClose}>Close</Button>
        </div>,
      );
    }
  };

  const handleEditAction = (type: string, id: string) => {
    setOpen(true);
    setContent(getEditComponent(type, id, handleClose));
  };

  const onRowClick = (rowData: string[]) => {
    const [type, id] = rowData;
    handleViewAction(type, id);
  };

  const handleEdit = (
    e: React.MouseEvent<HTMLButtonElement>,
    type: string,
    id: string,
  ) => {
    e.stopPropagation();
    handleEditAction(type, id);
  };

  const defaultColumns = [
    { name: "type", label: "Type", options: { display: "true" } },
    { name: "id", label: "ID", options: { display: "false" } },
    { name: "comment", label: "Description", options: { display: "true" } },
    { name: "q_check", label: "Status", options: { display: "true" } },
    { name: "name", label: "Name", options: { display: "false" } },
    { name: "creditor__qcomment", label: "Quästor comment", options: { display: "false" } },
    { name: "creditor__amount", label: "Amount", options: { display: "true" } },
    { name: "creditor__currency", label: "Currency", options: { display: "true" } },
    { name: "creator", label: "Creator", options: { display: "true" } },
    { name: "card", label: "Card", options: { display: "false" } },
    { name: "creditor__kst__kst_number", label: "KST Number", options: { display: "true" } },
    { name: "creditor__kst__name_de", label: "KST Name", options: { display: "true" } },
    { name: "creditor__ledger__name_de", label: "Ledger Name", options: { display: "true" } },
    { name: "receipt", label: "Receipt", options: { display: "false" } },
    { name: "reference", label: "Reference", options: { display: "false" } },
    { name: "iban", label: "IBAN", options: { display: "false" } },
    { name: "reimbursement__recipient", label: "Recipient", options: { display: "false" }  },
    {
      name: "edit",
      label: "Edit",
      options: {
        filter: false,
        sort: false,
        display: editable ? "true" : "false",
        customBodyRender: (_: any, tableMeta: any) => {
          const itemType = tableMeta.rowData[0];
          const itemId = tableMeta.rowData[1];
          if (tableMeta.rowData[3] === "open") {
            return (
              <Button
                variant="contained"
                color="primary"
                onClick={(e) => handleEdit(e, itemType, itemId)}
              >
                Edit
              </Button>
            );
          }
          return <></>;
        },
      },
    },
  ];

  const columns = [...defaultColumns, ...additionalColumns];

  return (
    <>
      <GenericDataTable
        title={title}
        columns={columns}
        fetchData={fetchFunction}
        onRowClick={onRowClick}
        refresh={!open}
      />
      <Dialog open={open} onClose={handleClose} maxWidth="lg" fullWidth>
        <DialogTitle>
          {content && React.isValidElement(content) ? "Details" : "Edit Item"}
        </DialogTitle>
        <DialogContent>
          {content}
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose}>Close</Button>
        </DialogActions>
      </Dialog>
    </>
  );
};

export default GenericEditableTable;
