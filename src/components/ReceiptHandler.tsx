import { filesUploadFile } from "../client/services.gen";

/**
 * Compress an image file (JPEG/PNG) by resizing it to the specified maximum dimensions.
 *
 * @param file - The original image file.
 * @param maxWidth - Maximum width in pixels (default is 1024).
 * @param maxHeight - Maximum height in pixels (default is 1024).
 * @returns A promise that resolves to the compressed image file.
 */
async function compressImage(
  file: File,
  maxWidth: number = 1024,
  maxHeight: number = 1024
): Promise<File> {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      const img = new Image();
      img.src = reader.result as string;
      img.onload = () => {
        let { width, height } = img;

        // Calculate new dimensions if needed
        if (width > maxWidth || height > maxHeight) {
          const ratio = Math.min(maxWidth / width, maxHeight / height);
          width = Math.floor(width * ratio);
          height = Math.floor(height * ratio);
        }

        // Draw the resized image on a canvas
        const canvas = document.createElement("canvas");
        canvas.width = width;
        canvas.height = height;
        const ctx = canvas.getContext("2d");
        if (!ctx) {
          return reject(new Error("Failed to get canvas context"));
        }
        ctx.drawImage(img, 0, 0, width, height);

        // Convert the canvas back to a Blob
        canvas.toBlob(
          (blob) => {
            if (blob) {
              // Create a new file from the Blob, preserving the original file name and type
              const compressedFile = new File([blob], file.name, { type: file.type });
              resolve(compressedFile);
            } else {
              reject(new Error("Image compression failed"));
            }
          },
          file.type,
          0.8 // quality parameter (0.8 is a typical value for JPEG images)
        );
      };
      img.onerror = (error) => {
        reject(error);
      };
    };
    reader.onerror = (error) => {
      reject(error);
    };
  });
}

/**
 * Stub function for PDF compression.
 * Compressing PDFs on the client side is non-trivial and may require a dedicated library
 * or server-side processing. This function currently returns the original file.
 *
 * @param file - The original PDF file.
 * @returns A promise that resolves to the (uncompressed) PDF file.
 */
async function compressPDF(file: File): Promise<File> {
  console.warn("PDF compression is not implemented. Uploading original PDF.");
  return file;
}

/**
 * Handles receipt file uploads.
 * If the receipt is a file, it first compresses the file (if it is an image or PDF)
 * before uploading it using filesUploadFile.
 * If the receipt is a string, it extracts and returns the file name.
 *
 * @param receipt - Either a URL string or a File object.
 * @returns A promise that resolves to the file id or file name.
 */
export default async function receiptHandler(receipt: string | File): Promise<string> {
  let receiptOut = "";

  if (typeof receipt !== "string") {
    let fileToUpload = receipt;

    // Check the file type and compress accordingly
    if (receipt.type.startsWith("image/")) {
      fileToUpload = await compressImage(receipt);
    } else if (receipt.type === "application/pdf") {
      fileToUpload = await compressPDF(receipt);
    }

    console.log("Uploading file...");
    // Upload the (potentially compressed) file and get the file id back
    const response = await filesUploadFile({ body: { file: fileToUpload } });
    if (response.error) {
      throw response.error;
    } else if (response.data) {
      console.log("File uploaded:", response.data);
      receiptOut = response.data.file_id || "";
    } else {
      throw new Error("No data returned from file upload");
    }
  } else {
    // If the receipt is a string (URL), extract the file name from the URL
    const t1 = receipt.split("?")[0];
    const t2 = t1.split("/").pop();
    receiptOut = t2 || "";
  }
  
  return receiptOut;
}
