import { Kst, Ledger } from "../client";

export function Kst_title(kst: Kst): string {
  return String(kst.kst_number) + " | " + kst.name_de;
}

export function Ledger_title(ledger: Ledger): string {
  return ledger.name_de;
}
