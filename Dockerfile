From node:23-alpine

# Create app directory
RUN mkdir -p /app
WORKDIR /app

COPY package.json /app
COPY yarn.lock /app

RUN yarn install

COPY . /app
RUN chmod +x /app/entrypoint.sh
EXPOSE 4173
CMD ["/app/entrypoint.sh"]

